package com.training.homework10.task1;

import com.training.homework10.task1.atm.MoneyConsumer;
import com.training.homework10.task1.atm.MoneyProducer;
import com.training.homework10.task1.card.Card;
import com.training.homework10.task1.card.CreditCard;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {
    private static final int CONSUMERS = 5;
    private static final int PRODUCERS = 3;

    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        Card card = new CreditCard("Holder1", 500);
        ExecutorService exec = Executors.newFixedThreadPool(8);

        for (int i = 0; i < PRODUCERS; i++) {
            exec.execute(new MoneyProducer(card));
        }
        for (int i = 0; i < CONSUMERS; i++) {
            exec.execute(new MoneyConsumer(card));
        }
        exec.shutdown();
    }
}

