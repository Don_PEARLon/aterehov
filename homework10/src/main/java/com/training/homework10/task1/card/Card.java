package com.training.homework10.task1.card;

/**
 * Class represents a bank card.
 *
 * @author Alexandr_Terehov
 */
public abstract class Card {
    protected String holderName;
    protected double accountBalance;

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public Card(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card. 
     * @throws IllegalArgumentException if accountBalance < 0.  
     */
    public Card(String holderName, double accountBalance) {
        if (accountBalance < 0) {
            throw new IllegalArgumentException("Account balance should be more than 0.");
        }
        this.holderName = holderName;
        this.accountBalance = accountBalance;
    }

    /**
     * Method used to add cash amount to card.
     *
     * @param amount
     *            cash amount 
     */
    public void deposit(double amount) {
        this.accountBalance += amount;
    }
    
    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount
     */
    public void withdraw(double amount) {
        accountBalance -= amount;
    }
    
    /**
     * @return card holder name.
     */
    public String getHolderName() {
        return holderName;
    }

    /**
     * 
     * @param holderName
     *            card holder name to set.
     */
    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }
               
    /**
     * @return card cash balance.
     */
    public double getBalance() {
        return accountBalance;
    }

    /**
     * 
     * @param accountBalance
     *            card cash balance to set.
     * @throws IllegalArgumentException if accountBalance < 0.
     */
    public void setAccountBalance(double accountBalance) {
        if (accountBalance < 0) {
            throw new IllegalArgumentException("Account balance should be more than 0.");
        }
        this.accountBalance = accountBalance;
    }
    
    /**
     * @return String representation of the card.
     */
    @Override
    public String toString() {
        String strOutput = "[Holder: " + this.holderName + ", "
                + "Balance: " + this.accountBalance + "]";
        return strOutput;
    }
}
