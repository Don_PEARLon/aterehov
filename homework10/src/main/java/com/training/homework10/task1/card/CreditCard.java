package com.training.homework10.task1.card;

/**
 * Class represents a credit card.
 *
 * @author Alexandr_Terehov
 */
public class CreditCard extends Card {
    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public CreditCard(String holderName) {
       super(holderName);
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card
     */
    public CreditCard(String holderName, double accountBalance) {
        super(holderName, accountBalance);
    }
    
    /**
     * @return String representation of the card.
     */
    @Override
    public String toString() {
        return "CreditCard " + super.toString();
    }
}