package com.training.homework10.task1.card;

import com.training.homework10.task1.card.exception.NegativeBalanceException;

/**
 * Class represents a debit card.
 *
 * @author Alexandr_Terehov
 */
public class DebitCard extends Card {
    
    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public DebitCard(String holderName) {
       super(holderName);
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card 
     */
    public DebitCard(String holderName, double accountBalance) {
        super(holderName, accountBalance);
    }

    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount.
     * @throws NegativeBalanceException if amount > accountBalance.
     */
    @Override
    public void withdraw(double amount) {
        if (amount > accountBalance) {
            throw new NegativeBalanceException("Cant withdraw more than Card balance");
        }
        super.withdraw(amount);
    }
    
    /**
     * @return String representation of the card.
     */
    @Override
    public String toString() {
        return "DebitCard " + super.toString();
    }
}
