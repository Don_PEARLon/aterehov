package com.training.homework10.task1.atm;

import com.training.homework10.task1.card.Card;

import java.util.Random;

/**
 * Class represents an ATM with option of withdraw in thread.
 *
 * @author Alexandr_Terehov
 */
public class MoneyConsumer extends Atm implements Runnable {
    public static final int MAX_AMOUNT = 10;
    public static final int MIN_AMOUNT = 5;
                                      
    private Card card;
    private Random random;
    
    /**
     * Constructor.
     *
     * @param card
     *            instance of the Card class.
     * @throws NullPointerException if card is null            
     */
    public MoneyConsumer(Card card) {
        if (card == null) {
            throw new NullPointerException("Card instance is null");
        }
        this.card = card;
        random = new Random();
    }
    
    /**
     * Method used to withdraw 5-10$ from card cash balance.
     */
    @Override
    public void run() {
        boolean run = true;
        int amount;
        while (run) {
            synchronized (card) {
                amount = random.nextInt(MAX_AMOUNT - MIN_AMOUNT + 1) + MIN_AMOUNT;
                if (!this.cardIsValid(card)) {
                    break;
                }
                this.withdraw(card, amount);
            }

            try {
                Thread.sleep((long) (Math.random() * 300));
            } catch (InterruptedException exc) {
                System.out.println(exc.getMessage());
            }

        }
        System.out.println("Finished.");
    }
}
