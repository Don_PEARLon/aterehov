package com.training.homework10.task1.atm;

import com.training.homework10.task1.card.Card;

/**
 * Class represents an ATM.
 *
 * @author Alexandr_Terehov
 */
public class Atm {
   
    /**
     * Method used to add cash amount to card.
     *
     * @param card
     *            bank card.
     * @param amount
     *            cash amount.
     * @throws NullPointerException if card is null              
     */
    public void deposit(Card card, double amount) {
        if (card == null) {
            throw new NullPointerException("Card instance is null");
        }
        card.deposit(amount);
        System.out.println("Deposit of " + amount + " has been processed");
        System.out.println(card);
        
    }

    /**
     * Method used to withdraw funds from the card.
     *
     * @param card
     *            bank card.
     * @param amount
     *            cash amount.
     * @throws NullPointerException if card is null  
     */
    public void withdraw(Card card, double amount) {
        if (card == null) {
            throw new NullPointerException("Card instance is null");
        }
        card.withdraw(amount);
        System.out.println("Withdrawal of " + amount + " has been processed");
        System.out.println(card);
    }

    /**
     * Method used to return card cash balance.
     *
     * @param card
     *            bank card.
     * @return card cash balance.
     * @throws NullPointerException if card is null  
     */
    public double getAtmCardBalance(Card card) {
        if (card == null) {
            throw new NullPointerException("Card instance is null");
        }
        return card.getBalance();
    }

    /**
     * Method provides printing card cash balance to the console.
     * 
     * @param card
     *            bank card.
     * @throws NullPointerException if card is null  
     */
    public void displayAtmCardBalance(Card card) {
        if (card == null) {
            throw new NullPointerException("Card instance is null");
        }
        System.out.println(card.getBalance());
    }
    
    /**
     * Checks if card cash balance conforms following conditions: balance must be
     * more then 0 and must not be bigger than 1000.
     * 
     * @param card
     *            bank card.
     * @return boolean result of the checking.  
     * @throws NullPointerException if card is null            
     */
    public boolean cardIsValid(Card card) {
        if (card == null) {
            throw new NullPointerException("Card instance is null");
        }
        if (card.getBalance() <= 0) {
            System.out.println("Card balance is <= 0. " + card);
            return false;
        }
        if (card.getBalance() >= 1000) {
            System.out.println("Card balance is more than 1000. " + card);
            return false;
        }
        return true;
    }
}
