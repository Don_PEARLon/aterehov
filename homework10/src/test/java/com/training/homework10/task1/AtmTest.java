package com.training.homework10.task1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import com.training.homework10.task1.atm.Atm;
import com.training.homework10.task1.card.Card;
import com.training.homework10.task1.card.CreditCard;
import com.training.homework10.task1.card.DebitCard;
import com.training.homework10.task1.card.exception.NegativeBalanceException;


public class AtmTest {
	private static Atm atm;
	private static Card creditCard;
	private static Card debitCard;

	@Before
	public void initObjects() {
		creditCard = new CreditCard("Holder1", 1000);
		debitCard = new DebitCard("Holder1", 1100);
		atm = new Atm();
	}
	
	@Test
	public void testAtmDepositCredit() {
		atm.deposit(creditCard, 250.254);
		assertEquals(1250.254, atm.getAtmCardBalance(creditCard), 0.0);
	}

	@Test
	public void testAtmDepositDebit() {
		atm.deposit(debitCard, 350.25);
		assertEquals(1450.25, atm.getAtmCardBalance(debitCard), 0.0);
	}
	
	@Test (expected = NullPointerException.class)
	public void testAtmDepositNull() {
		atm.deposit(null, 350.25);
	}
	
	@Test
	public void testAtmWitdrawCredit() {
		atm.withdraw(creditCard, 1100);
		assertEquals(-100, atm.getAtmCardBalance(creditCard), 0.0);
	}

	@Test
	public void testAtmWithdrawDebit() {
		atm.withdraw(debitCard, 550);
		assertEquals(550, atm.getAtmCardBalance(debitCard), 0.0);
	}
	
	@Test (expected = NegativeBalanceException.class)
	public void testAtmWithdrawDebitNegative() {
		atm.withdraw(debitCard, 1200);
	}
	
	@Test (expected = NullPointerException.class)
	public void testAtmWithdrawNull() {
		atm.withdraw(null, 350.25);
	}
	
	@Test
	public void testGetAtmCardBln() {
		double balance = atm.getAtmCardBalance(debitCard);
		assertEquals(1100, balance, 0);
	}
	
	@Test (expected = NullPointerException.class)
	public void testGetAtmCardBlnNull() {
		double balance = atm.getAtmCardBalance(null);
	}
	
	@Test
	public void testCardIsValid() {
		boolean condition = atm.cardIsValid(debitCard);
		assertFalse(condition);
	}
	
	@Test (expected = NullPointerException.class)
	public void testCardIsValidNull() {
		boolean condition = atm.cardIsValid(null);
	}
}
