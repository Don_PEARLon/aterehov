package com.training.homework6;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/**
 * Class used to convert text and output every single word in the text with its
 * quantity.
 *
 * @author Alexandr_Terehov
 */
public class TextConverter {
    private Map<String, Integer> converterMap;
    private final String[] punktMarks = { ".", ",", ":", ";", "-", "?", "!" };

    public TextConverter() {
        converterMap = new TreeMap<String, Integer>();
    }

    /**
     * Method used to create a String array with every single word from text.
     *
     * @param text
     *            text for processing.
     * @return String array with every single word from text.
     */
    public String[] createSplittedText(String text) {
        for (int i = 0; i < punktMarks.length; i++) {
            text = text.replace(punktMarks[i], "");
        }
        String[] splitText = text.split(" ");
        return splitText;
    }

    /**
     * Method used to return a map which contains every single word from text with
     * its quantity.
     *
     * @return converterMap map with words and their quantities.
     */
    public Map<String, Integer> getConverterMap() {
        return converterMap;
    }

    /**
     * Method used to convert text and output every single word in the text with its
     * quantity.
     *
     * @param text
     *            text for processing.
     */
    public void convert(String text) {
        String[] splitText = createSplittedText(text.toLowerCase());
        for (int i = 0; i < splitText.length; i++) {
            converterMap.put(splitText[i], getTextElementQuantity(splitText, splitText[i]));
        }
        printText();
    }

    /**
     * Method provides printing result of the text conversion to the console.
     */
    public void printText() {
        Set<String> words = converterMap.keySet();
        Iterator<String> iterator = words.iterator();
        String textWord = iterator.next();
        char firstChar = (textWord.toUpperCase()).charAt(0);
        System.out.println(firstChar + ": " + textWord + " " + converterMap.get(textWord));
        char previousFirstChar = firstChar;
        while (iterator.hasNext()) {
            textWord = iterator.next();
            firstChar = (textWord.toUpperCase()).charAt(0);
            if (firstChar == previousFirstChar) {
                System.out.println("   " + textWord + " " + converterMap.get(textWord));
            } else {
                firstChar = (textWord.toUpperCase()).charAt(0);
                System.out.println(firstChar + ": " + textWord + " " + converterMap.get(textWord));
                previousFirstChar = firstChar;
            }
        }
    }

    /**
     * Method used to return the quantity of a single string element in the String
     * array.
     *
     * @param text
     *            string array.
     * @param element
     *            string element.
     * @return quantity of a single string element in the array.
     */
    public int getTextElementQuantity(String[] text, String element) {
        int quantity = 0;
        for (int i = 0; i < text.length; i++) {
            if (text[i].equals(element)) {
                quantity++;
            }
        }
        return quantity;
    }
}