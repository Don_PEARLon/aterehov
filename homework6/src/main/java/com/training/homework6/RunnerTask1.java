package com.training.homework6;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {
    
    /** Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        String text = "Once upon a time a Wolf was lapping at a spring on a hillside,"
                + " when, looking up, what should he see but a "
                + "Lamb just beginning to drink a little lower down. Down, down, down at, at.";
        TextConverter textConverter = new TextConverter();
        textConverter.convert(text);
    }
}