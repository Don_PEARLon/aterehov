package com.training.homework6;

import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for TextConverter class.
 *
 * @author Alexandr_Terehov
 */
public class TextConverterTest {
	private static TextConverter textConverter;

	@Before
	public void initObjects() {
		textConverter = new TextConverter();
	}

	@Test
	public void testCreateSplittedText() {
		String text = "a, a!, ?a.";
		String[] splitText = textConverter.createSplittedText(text);
		boolean condition = true;
		for (String str : splitText) {
			if (!str.equals("a")) {
				condition = false;
			}
		}
		assertTrue(condition);
	}

	@Test
	public void testGetTextElementQuantity() {
		String text = "a, a!, ?a.";
		String[] splitText = textConverter.createSplittedText(text);
		int result = textConverter.getTextElementQuantity(splitText, "a");
		assertEquals(3, result, 0);
	}

	@Test
	public void testConvert() {
		String text = "Blow, down, down, mind. Blow. Down, at, Mind! At mind?";
		textConverter.convert(text);
		Map<String, Integer> converterMap = textConverter.getConverterMap();
		int size = converterMap.size();
		int quantity = converterMap.get("mind");
		boolean condition = false;
		if ((size == 4) && (quantity == 3)) {
			condition = true;
		}
		assertTrue(condition);
	}
}
