package com.training;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.training.entity.ShopItem;

/**
 * Test suite for ShopItem class.
 *
 * @author Alexandr_Terehov
 */
public class ShopItemTest {
	private static ShopItem shopItem;

	@Before
	public void initObjects() {
		shopItem = new ShopItem("Item1", 100.15);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShopItemNegativeName() {
		ShopItem shopItem1 = new ShopItem("   ", 100);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShopItemNegativePrice() {
		ShopItem shopItem1 = new ShopItem("Item", -1);
	}

	@Test
	public void testGetPrice() {
		double expectedPrice = 100.15;
		assertEquals(expectedPrice, shopItem.getPrice(), 0);
	}

	@Test
	public void testSetPriceTest() {
		double expectedPrice = 110.25;
		shopItem.setPrice(110.25);
		assertEquals(expectedPrice, shopItem.getPrice(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetPriceNegative() {
		shopItem.setPrice(-1);
	}
}
