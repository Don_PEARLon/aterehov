package com.training.servlet;

import com.training.entity.ShopItem;
import com.training.entity.User;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet used for confirmation of the order, provides calculation of the total
 * order price.
 */
@WebServlet(
        name = "submitOrder",
        urlPatterns = "/order")
public class SubmitOrderServlet extends HttpServlet {
    private static final long serialVersionUID = -1882926836615346135L;
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException
     *             can be thrown when servlet encounters difficulty.  
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        final User user = (User) session.getAttribute("user");
        final double totalPrice =  getTotalPrice(user);
        request.setAttribute("user", user);
        request.setAttribute("totalPrice", totalPrice);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("submit-order.jsp");
        requestDispatcher.forward(request, response);
    }
         
    /**
     * Method provides creation of string in HTML format contains information about
     * total sum of user's orders from the online shop.
     *
     * @param user
     *            user of the online shop.
     */
    private double getTotalPrice(User user) {
        double totalPrice = 0;
        for (ShopItem item : user.getItemList()) {
            totalPrice += item.getPrice();
        }
        return totalPrice;
    }
}
