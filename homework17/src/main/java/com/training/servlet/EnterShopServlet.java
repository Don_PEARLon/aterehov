package com.training.servlet;

import com.training.entity.ShopItem;
import com.training.entity.User;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet used during process of shop item selection for the order. 
 */
@WebServlet(
        name = "enterShop",
        urlPatterns = "/enter")
public class EnterShopServlet extends HttpServlet {
    private static final long serialVersionUID = -6881786289314956801L;
    
    /**
     * Map that contains information about items of the online-shop. Name of the
     * item is a key and price is a value.
     */
    private Map<String, Double> shopItemMap;
       
    @Override
    public void init() {
        shopItemMap = new HashMap<>();
        shopItemMap.put("Mobile Phone", 10.0);
        shopItemMap.put("Book", 5.5);
        shopItemMap.put("Laptop", 310.15);
        shopItemMap.put("TV", 250.0);
    }
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException
     *             can be thrown when servlet encounters difficulty.
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        final String userName;
        final User user;
        if (session.getAttribute("user") == null) {
            userName = request.getParameter("userName");
            user = new User(userName);
            session.setAttribute("user", user);
        } else {
            user = (User) session.getAttribute("user");
            final String shopItemName = request.getParameter("shopItemName");
            if (shopItemName != null) {
                user.addItemToList(getShopItem(shopItemName));
            }
        }
        request.setAttribute("user", user);
        request.setAttribute("shopItemMap", shopItemMap);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("enter-shop.jsp");
        requestDispatcher.forward(request, response);
    }
 
    /**
     * Creates instance of the ShopItem class from the input String.
     *
     * @param shopItemName
     *            string representation of the shop item.
     * @return instance of the ShopItem class.
     */
    private ShopItem getShopItem(String shopItemName) {
        ShopItem shopItem = null;
        for (Map.Entry<String, Double> entry : shopItemMap.entrySet()) {
            if (entry.getKey().equals(shopItemName)) {
                shopItem = new ShopItem(entry.getKey(), entry.getValue());
            }
        }
        return shopItem;
    }
}