package com.training.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represents an item of the online-shop.
 *
 * @author Alexandr_Terehov
 */
public class ShopItem {
    private String name;
    private double price;
     
    /**
     * Constructor.
     *
     * @param name
     *            name of the user.
     * @param price
     *            price of the item.
     * @throws IllegalArgumentException
     *             if name or price is not in the correct format.
     */
    public ShopItem(String name, double price) {
        if (checkName(name) && price > 0) {
            this.name = name;
            this.price = price;
        } else {
            throw new IllegalArgumentException("Incorrect item params");
        }
    }
     
    /**
     * @return name of the item.
     */
    public String getName() {
        return name;
    }
     
    /**
     * 
     * @param name
     *            name of the item to set.
     * @throws IllegalArgumentException
     *             if name is not in the correct format.
     */
    public void setName(String name) {
        if (checkName(name)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Incorrect name format");
        }
    }
     
    /**
     * @return name of the item.
     */
    public double getPrice() {
        return price;
    }
     
    /**
     * 
     * @param price
     *            price of the item to set.
     * @throws IllegalArgumentException
     *             if if price <= 0.
     */
    public void setPrice(double price) {
        if (price > 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price should be > 0");
        }
    }
     
    /**
     * Method used to check the correctness of the item's name.
     *
     * @param name
     *            name to check.
     * @return boolean result of the checking.
     */
    private boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^(?!\\s*$).+");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }
}
