package com.training.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represents a user of the online shop.
 *
 * @author Alexandr_Terehov
 */
public class User {
    private String name;
     
    /**
     * List of the shop items related to the current user.
     */
    private List<ShopItem> itemList;
     
    /**
     * Constructor.
     *
     * @param name
     *            name of the user.
     * @throws IllegalArgumentException
     *             if name is not in the correct format.
     */
    public User(String name) {
        if (checkName(name)) {
            this.name = name;
            itemList = new ArrayList<>();
        } else {
            throw new IllegalArgumentException("Illegal 'name' format");
        }
    }
     
    /**
     * @return name of the user.
     */
    public String getName() {
        return name;
    }
      
    /**
     * 
     * @param name
     *            name of the user to set.
     * @throws IllegalArgumentException
     *             if name is not in the correct format.
     */
    public void setName(String name) {
        if (checkName(name)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Illegal 'name' format");
        }
    }
     
    /**
     * Method used to add an instance of the ShopItem class to the list of items of
     * the user.
     *
     * @param item
     *            instance of the ShopItem class to add.
     * @throws NullPointerException
     *             if item is null.
     */
    public void addItemToList(ShopItem item) {
        if (item == null) {
            throw new NullPointerException("Item can't be null");
        } else {
            this.itemList.add(item);
        }
    }
     
    /**
     * @return list of the shop items.
     */
    public List<ShopItem> getItemList() {
        return itemList;
    }
     
    /**
     * Method used to check the correctness of the user name.
     *
     * @param name
     *            name to check.
     * @return boolean result of the checking.
     */
    private boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^(?!\\s*$).+");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }
}
