package com.training.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Filter used during process of authorization into online-shop, checks
 * agreement with terms and conditions.
 */
public class TermsAgreementFilter implements Filter {
    private String termsAgreement;
    private FilterConfig filterConfig;
     
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }
    
    /**
     * Handles {@link HttpServlet} doFilter Method.
     *
     * @param request
     *            the {@link ServletRequest}
     * @param response
     *            the {@link ServletResponse}
     * @param chain
     *            the {@link FilterChain}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest requestHttp = (HttpServletRequest) request;
        HttpSession session = requestHttp.getSession();
        termsAgreement = request.getParameter("termsAgreement");
        if (session.getAttribute("user") == null && termsAgreement == null) {
            RequestDispatcher requestDispatcher = 
                    request.getRequestDispatcher("terms-agreement.jsp");
            requestDispatcher.forward(request, response);
        } else {
            chain.doFilter(request, response);
        }
    }
     
    @Override
    public void destroy() {
        
    }
}