<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<!--     JSTL Tag Library -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<div align=center>
		<h1>Hello ${user.getName()}!</h1>
		<c:choose>
			<c:when test="${user.getItemList().isEmpty()}">
				<h2>Make your order</h2>
			</c:when>
			<c:otherwise>
				<h4>You have already chosen:</h4>
				<% int itemNumber = 1; %>
				<c:forEach items="${user.getItemList()}" var="item">
					<h4><%=itemNumber %>) ${item.getName()} ${item.getPrice()}$</h4>
					<% itemNumber++; %>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		<form action='enter' method='post'>
			<select name='shopItemName'>
				<c:forEach items="${shopItemMap}" var="entry">
					<option value='${entry.key}'>${entry.key} ${entry.value}$
				</c:forEach>
			</select> <input type='submit' value='Add Item'>
		</form>
		<form action='order' method='post'>
			<input type='submit' value='Submit'>
		</form>
	</div>
</body>
</html>

