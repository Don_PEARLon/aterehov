<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<!--     JSTL Tag Library -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<div align=center>
		<h1>Dear ${user.getName()}, your order:</h1>
    	<% int itemNumber = 1; %>
		<c:forEach items="${user.getItemList()}" var="item">
			<h4><%=itemNumber %>) ${item.getName()} ${item.getPrice()}$</h4>
			<% itemNumber++; %>
		</c:forEach>
		<br>
		<h4>Total: ${totalPrice}$</h4>
	</div>
</body>
</html>