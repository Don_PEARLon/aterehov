To run this application go in cmd to the application root folder and print command:
"mvn tomcat7:run" or "mvn clean install tomcat7:run-war-only".
Swagger has been added. Swagger documentation will be available by URL "http://localhost:8080/online-shop". 
There are four users initially avaliable in database: user1 with Id:1, user2 with Id:2, user3 with Id:3, admin with Id:4.
New user admin has been added. So use one of theese users to log in to the app.
Print �user1� & �pass1�, �user2� & �pass2�, �user3� & �pass3� or �admin� & �admin� to log in.

Now Admin has an option to perform operations with other online-shop users. 

Operations available only for Admin:
HTTP GET localhost:8080/online-shop/api/users - Get All Users
HTTP GET localhost:8080/online-shop/api/users/{id} - Get User By Id
HTTP GET localhost:8080/online-shop/api/users/{id}/orders - Get Orders by Users Id
HTTP POST localhost:8080/online-shop/api/users/{id}/orders - Add a new Order to User
HTTP DELETE localhost:8080/online-shop/api/users/{id}/orders/{orderid} - Delete Users Order
HTTP GET localhost:8080/online-shop/api/users/{id}/orders/{orderid} - Get Users Order by Id
HTTP POST localhost:8080/online-shop/api/users/{id}/orders/{orderid} - Add a new Odred with specified Id to User
HTTP PUT localhost:8080/online-shop/api/users/{id}/orders/{orderid} - Updates users order

Operations available for all users:
HTTP GET localhost:8080/online-shop/api/items - Get List of all Items
HTTP GET localhost:8080/online-shop/api/items/{id} - Get Item by Id
HTTP GET localhost:8080/online-shop/api/orders - Get Orders
HTTP POST localhost:8080/online-shop/api/orders/ - Add a new Order
HTTP GET localhost:8080/online-shop/api/orders/{id} - Get Orders by Id
HTTP POST localhost:8080/online-shop/api/orders/{id} - Add a new Order with specified Id
HTTP GET localhost:8080/online-shop/api/orders/{id}/items - Get Items of the Order
HTTP POST localhost:8080/online-shop/api/orders/{id}/items/{itemid} - Add item to the Order

Example of business process for order creation(logging as User): 
1. We are logging in as: "user1" & "pass1"
2. Creating order with Id = 4 with: HTTP POST localhost:8080/online-shop/api/orders/4
3. Adding an Item with Id = 1 to the order with id = 4 with: HTTP POST localhost:8080/online-shop/api/orders/4/items/1
User can perform operations with orders that related only to his Id. 
User with Id:1 can't access info by /api/orders/3, cause order with id: 3 don't relate to user with Id: 1. 


Example of business process for order creation(logging as Admin): 
1. We are logging in as: "admin" & "admin"
2. Initially creating order with Id = 4 for user with Id = 1 with: HTTP POST localhost:8080/online-shop/api/users/1/orders
