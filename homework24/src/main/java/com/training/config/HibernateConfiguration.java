package com.training.config;

import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "com.training.*")
@PropertySource(value = { "classpath:/db/db.properties" })
public class HibernateConfiguration {
   
    @Autowired
    private Environment environment;

    private final ApplicationContext context;
    
    private static final String CREATE_TABLE_SQL_PATH = "sql/schema.sql";
    private static final String FILL_TABLE_SQL_PATH = "sql/data.sql";

    public HibernateConfiguration(final ApplicationContext context) {
        this.context = context;
    }
    
    /**
     * @return {@link LocalSessionFactoryBean} bean.
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "com.training.*" });
        //sessionFactory.setHibernateProperties(hibernateProperties());
        Resource hibernateConfigResource = context.getResource("classpath:hibernate.cfg.xml");
        sessionFactory.setConfigLocation(hibernateConfigResource);
        return sessionFactory;
    }
       
    /*private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.format_sql", "true");
        return properties;        
    }*/
    
    /**
     * @param sessionFactory {@link SessionFactory}.
     * @return {@link HibernateTransactionManager} bean.
     */
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }
    
    /**
     * @return {@link DataSource}.
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("db.driver"));
        dataSource.setUrl(environment.getRequiredProperty("db.url"));
        dataSource.setUsername(environment.getRequiredProperty("db.username"));
        dataSource.setPassword(environment.getRequiredProperty("db.password"));
        
        Resource createTables = new ClassPathResource(CREATE_TABLE_SQL_PATH);
        Resource fillTables = new ClassPathResource(FILL_TABLE_SQL_PATH);
        DatabasePopulator databasePopulatorCreate = new ResourceDatabasePopulator(createTables);
        DatabasePopulator databasePopulatorFill = new ResourceDatabasePopulator(fillTables);
        DatabasePopulatorUtils.execute(databasePopulatorCreate, dataSource);
        DatabasePopulatorUtils.execute(databasePopulatorFill, dataSource);
        
        return dataSource;
    }
}