package com.training.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;

@Configuration 
@EnableSwagger2
public class SwaggerConfig {

    /**
     * @return {@link Docket} bean.
     */
    @Bean
    public Docket shoprApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Online Shop")
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/online-shop/**"))
                .build()
                .apiInfo(getApiInfo())
                .securityContexts(Arrays.asList(shopSecurityContext()))
                .securitySchemes(Arrays.asList(basicAuthScheme()))
                .apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors
                .basePackage("com.training"))
                .paths(PathSelectors.any())
                .build();
    }
    
    /**
     * @return {@link SecurityContext}.
     */
    private SecurityContext shopSecurityContext() {
        return SecurityContext.builder().securityReferences(Arrays.asList(basicAuthReference()))
                .forPaths(PathSelectors.ant("/online-shop/**")).build();
    }
    
    /**
     * @return {@link SecurityScheme}.
     */
    private SecurityScheme basicAuthScheme() {
        return new BasicAuth("basicAuth");
    }
    
    /**
     * @return {@link SecurityReference}.
     */
    private SecurityReference basicAuthReference() {
        return new SecurityReference("basicAuth", new AuthorizationScope[0]);
    }
    
    /**
     * @return {@link ApiInfo}.
     */
    private ApiInfo getApiInfo() {
        return new ApiInfo("Online Shop", "Online Shop Api Documentation", "1.0", "urn:tos",
                new Contact("", "", ""), "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0", 
                new ArrayList<>());
    }
}