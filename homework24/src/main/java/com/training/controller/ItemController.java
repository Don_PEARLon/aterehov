package com.training.controller;

import com.training.dto.ItemDto;
import com.training.service.ItemService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The controller to work with items of the online-shop. 
 *
 * @author Alexandr_Terehov
 */
@RestController
@RequestMapping(path = "/api/")
@PreAuthorize("hasAnyAuthority('ADMIN','USER')")
@PropertySource(value = { "classpath:/swagger/documentation.properties" })
@Api(tags = "3. Operations with items")
public class ItemController {
    public static final String ADMIN_TAG = "1. Admin operations";
    public static final String USER_TAG = "2. User operations";   
    
    private final ItemService itemService;
    
    /**
     * The constructor of the class.
     *
     * @param itemService - the service used to work with items.
     */
    @Autowired
    public ItemController(final ItemService itemService) {
        this.itemService = itemService;
    }
    
    /**
     * Returns the list of all items.
     *
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "items", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Items", notes = "${ItemController.getItems.notes}",
            tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<List<ItemDto>> getItems() {
        return ResponseEntity.ok(itemService.getItems());
    }

    /**
     * Returns item with specified id.
     *
     * @param id - id of the item.
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "items/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Item by Id", notes = "${ItemController.getItemById.notes}",
            tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<ItemDto> getItemById(
            @PathVariable(value = "id") final Long id) {
        return ResponseEntity.ok(itemService.getItemById(id));
    }
}