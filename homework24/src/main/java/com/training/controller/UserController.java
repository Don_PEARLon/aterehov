package com.training.controller;

import com.training.dto.OrderDto;
import com.training.dto.UserDto;
import com.training.service.OrderService;
import com.training.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

/**
 * The controller to work with users of the online-shop. 
 *
 * @author Alexandr_Terehov
 */
@RestController
@RequestMapping("/api/users")
@PreAuthorize("hasAuthority('ADMIN')")
@Api(tags = "5. Operations with users")
public class UserController {
    public static final String ADMIN_TAG = "1. Admin operations"; 

    private final OrderService orderService;
    private final UserService userService;

    /**
     * The constructor of the class.
     *
     * @param userService - the service used to work with users.
     * @param orderService - the service used to work with orders.
     */
    @Autowired
    public UserController(final UserService userService,
            final OrderService orderService) {
        this.userService = userService;
        this.orderService = orderService;
    }

    /**
     * Returns the list of all users.
     *
     * @return {@link ResponseEntity}
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Users", notes = "${UserController.getUsers.notes}",
            tags = ADMIN_TAG)
    @ResponseBody
    public ResponseEntity<List<UserDto>> getUsers() {
        return  ResponseEntity.ok(userService.getUsers());
    }

    /**
     * Returns user with specified id.
     *
     * @param id - id of the user.
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get User By Id", notes = "${UserController.getUserById.notes}",
            tags = ADMIN_TAG)
    @ResponseBody
    public ResponseEntity<UserDto> getUserById(
            @PathVariable(value = "id") final Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    /**
     * Returns the list of orders related to user with specified id.
     *
     * @param id - id of the user.
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "/{id}/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Orders by User's Id", 
            notes = "${UserController.getOrdersByUserId.notes}", tags = ADMIN_TAG)
    @ResponseBody
    public  ResponseEntity<List<OrderDto>> getOrdersByUserId(
            @PathVariable(value = "id") final Long id) {
        UserDto user = userService.getUserById(id);
        return  ResponseEntity.ok(orderService.getOrdersByUserId(user.getId()));
    }

    /**
     * Returns the order with specified id related to the particular user.
     *
     * @param id - id of the user.
     * @param orderId - id of the user.
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "/{id}/orders/{orderid}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Users Order by Id",
            notes = "${UserController.getOrderById.notes}", tags = ADMIN_TAG)
    @ResponseBody
    public  ResponseEntity<OrderDto> getOrderById(@PathVariable(value = "id") final Long id,
            @PathVariable(value = "orderid") final Long orderId) {
        userService.getUserById(id);
        return  ResponseEntity.ok(orderService
            .getOrderByUserIdAndOrderId(userService.getUserById(id).getId(), orderId));
    }

    /**
     * Creates a new order for user with specified id.
     *
     * @param id - id of the user.
     * @return {@link ResponseEntity}
     */
    @PostMapping(value = "/{id}/orders", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add a new Order to User",
            notes = "${UserController.addOrder.notes}", tags = ADMIN_TAG)
    @ResponseBody
    public ResponseEntity<Void> addOrder(@PathVariable(value = "id") final Long id) {
        OrderDto orderDto = new OrderDto(null, 
                userService.getUserById(id).getId(), new BigDecimal(0)); 
        orderService.saveOrder(orderDto);
        return ResponseEntity.noContent().build();
    }

    /**
     * Creates an order with specified id for the particular user.
     *
     * @param id - id of the user.
     * @param orderId - id of the user.
     * @return {@link ResponseEntity}
     */
    @PostMapping(value = "/{id}/orders/{orderid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add a new Odred with specified Id to User",
            notes = "${UserController.addOrderWithId.notes}", tags = ADMIN_TAG)
    @ResponseBody
    public ResponseEntity<Void> addOrderWithId(@PathVariable(value = "id") final Long id, 
                @PathVariable(value = "orderid") final Long orderId) {
        OrderDto orderDto = new OrderDto(orderId,
                userService.getUserById(id).getId(), new BigDecimal(0)); 
        orderService.saveOrder(orderDto);
        return ResponseEntity.noContent().build();
    }

    /**
     * Deletes the order with specified id for the particular user.
     *
     * @param id - id of the user.
     * @param orderId - id of the user.
     * @return {@link ResponseEntity}
     */
    @DeleteMapping(value = "/{id}/orders/{orderid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete Users Order", notes = "${UserController.delteOrder.notes}",
            tags = ADMIN_TAG)
    @ResponseBody
    public ResponseEntity<Void> delteOrder(@PathVariable(value = "id") final Long id, 
                @PathVariable(value = "orderid") final Long orderId) {
        orderService.deleteOrderByUserIdAndOrderId(id, orderId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Updates the order with specified id for the particular user.
     *
     * @param id - id of the user.
     * @param orderDto - {@link OrderDto} to save.
     * @return {@link ResponseEntity}
     */
    @PutMapping(value = "/{id}/orders/{orderid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update Users Order", notes = "${UserController.updateOrder.notes}",
            tags = ADMIN_TAG)
    @ResponseBody
    public ResponseEntity<Void> updateOrder(
            @PathVariable final Long id, 
            @Valid @RequestBody final OrderDto orderDto) {
        orderService.updateOrderByUserId(id, orderDto);
        return ResponseEntity.noContent().build();
    }
}