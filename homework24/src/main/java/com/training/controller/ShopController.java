package com.training.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class ShopController {

    /**
     * @return {@link ModelAndView} related to the '/online-shop' URL.
     */
    @GetMapping("/")
    public ModelAndView loginToShop() {
        return new ModelAndView("redirect:swagger-ui.html");
    }
}