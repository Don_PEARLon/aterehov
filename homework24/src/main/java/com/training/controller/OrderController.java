package com.training.controller;

import com.training.dto.ItemDto;
import com.training.dto.OrderDto;
import com.training.security.ShopUserPrincipal;
import com.training.service.ItemService;
import com.training.service.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

/**
 * The controller to work with orders of the online-shop. 
 *
 * @author Alexandr_Terehov
 */
@RestController
@RequestMapping(path = "/api/")
@PreAuthorize("hasAnyAuthority('ADMIN','USER')")
@Api(tags = "4. Operations with orders")
public class OrderController {
    public static final String ADMIN_TAG = "1. Admin operations";
    public static final String USER_TAG = "2. User operations";  

    private final ItemService itemService;
    private final OrderService orderService;

    /**
     * The constructor of the class.
     *
     * @param itemService - the service used to work with items.
     * @param orderService - the service used to work with orders.
     */
    @Autowired
    public OrderController(final ItemService itemService, 
            final OrderService orderService) {
        this.itemService = itemService;
        this.orderService = orderService;
    }

    /**
     * Returns the list of orders related to particular user.
     *
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "orders", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Orders", notes = "${OrderController.getOrders.notes}",
            tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<List<OrderDto>> getOrders() {
        return ResponseEntity.ok(orderService
                .getOrdersByUserId(getCurrentUserPrincipal().getUserId()));
    }

    /**
     * Returns the order with specified id related to the particular user.
     *
     * @param id - id of the order.
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "orders/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Orders by Id", notes = "${OrderController.getOrderById.notes}",
            tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<OrderDto> getOrderById(@PathVariable(value = "id") final Long id) {
        return ResponseEntity.ok(orderService
                .getOrderByUserIdAndOrderId(getCurrentUserPrincipal().getUserId(), id));
    }

    /**
     * Returns list of the items related to the order with specified id.
     *
     * @param id - id of the order.
     * @return {@link ResponseEntity}
     */
    @GetMapping(value = "orders/{id}/items", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Items of the Order", 
            notes = "${OrderController.getItemsByOrderId.notes}", tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<List<ItemDto>> getItemsByOrderId(
            @PathVariable(value = "id") final Long id) {
        return ResponseEntity.ok(orderService.getOrderWithItemsByUserIdAndOrderId(
                    getCurrentUserPrincipal().getUserId(), id).getItemsDto());
    }

    /**
     * Creates an order with specified id for the particular user.
     *
     * @param id - id of the order.
     * @return {@link ResponseEntity}
     */
    @PostMapping(value = "/orders/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add a new Order with specified Id",
            notes = "${OrderController.addOrderWithId.notes}", tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<Void> addOrderWithId(
            @PathVariable(value = "id") final Long id) {
        OrderDto orderDto = new OrderDto(
                id, getCurrentUserPrincipal().getUserId(), new BigDecimal(0)); 
        orderService.saveOrder(orderDto);
        return ResponseEntity.noContent().build();
    }

    /**
     * Creates a new order for the particular user.
     *
     * @return {@link ResponseEntity}
     */
    @PostMapping(value = "/orders/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add a new Order", notes = "${OrderController.addOrder.notes}",
            tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<Void> addOrder() {
        OrderDto orderDto = new OrderDto(
                null, getCurrentUserPrincipal().getUserId(), new BigDecimal(0)); 
        orderService.saveOrder(orderDto);
        return ResponseEntity.noContent().build();
    }

    /**
     * Adds a new item to the order with specified id.
     *
     * @param id - id of the order.
     * @param itemId - id of the item.
     * @return {@link ResponseEntity}
     */
    @PostMapping(value = "orders/{id}/items/{itemid}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add item to the Order", 
            notes = "${OrderController.addItemToOrder.notes}", tags = {ADMIN_TAG, USER_TAG})
    @ResponseBody
    public ResponseEntity<Void> addItemToOrder(@PathVariable(value = "id") final Long id, 
                @PathVariable(value = "itemid") final Long itemId) {
        orderService.addItemToOrder(
                getCurrentUserPrincipal().getUserId(), id, itemService.getItemById(itemId));
        return ResponseEntity.noContent().build();
    }

    /**
     * Returns information about the user that is currently logged in.
     *
     * @return {@link ShopUserPrincipal}
     */
    private ShopUserPrincipal getCurrentUserPrincipal() {
        return (ShopUserPrincipal)SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
    }
}