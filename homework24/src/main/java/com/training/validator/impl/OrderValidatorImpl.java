package com.training.validator.impl;

import com.training.domain.Order;
import com.training.dto.OrderDescriptionDto;
import com.training.dto.OrderDto;
import com.training.exception.NotFoundException;
import com.training.validator.OrderValidator;
import org.springframework.stereotype.Component;

/**
 * Implementation of the {@Link OrderValidator} interface.
 *
 * @author Alexandr_Terehov
 */
@Component
public class OrderValidatorImpl implements OrderValidator {

    /**
     * Validates the owner of order.
     *
     * @param userId - Id of the user, who made the order.
     * @param order  - {@link OrderDto}
     */
    public void validateOwner(Long userId, OrderDto order) {
        if (!order.getUserId().equals(userId)) {
            throw new NotFoundException(
                    "Order with ID " + order.getId() + " for User with Id "
                            + userId + " has been not found");
        }
    }

    /**
     * Validates the owner of order.
     *
     * @param userId - Id of the user, who made the order.
     * @param order  - {@link Order}
     */
    public void validateOwner(Long userId, Order order) {
        if (!order.getUserId().equals(userId)) {
            throw new NotFoundException(
                    "Order with ID " + order.getId() + " for User with Id "
                            + userId + " has been not found");
        }
    }

    /**
     * Validates the owner of order.
     *
     * @param userId - Id of the user, who made the order.
     * @param order  - {@link OrderDescriptionDto}
     */
    public void validateOwner(Long userId, OrderDescriptionDto order) {
        if ((!order.getUserId().equals(userId))) {
            throw new NotFoundException(
                    "Order with ID " + order.getId() + " for User with Id "
                            + userId + " has been not found");
        }
    }
}
