package com.training.validator;

import com.training.domain.Order;
import com.training.dto.OrderDescriptionDto;
import com.training.dto.OrderDto;

/**
 * Class represents a validator of orders of the online-shop.
 *
 * @author Alexandr_Terehov
 */
public interface OrderValidator {

    /**
     * Validates the owner of order.
     *
     * @param userId - Id of the user, who made the order.
     * @param order  - {@link OrderDto}
     */
    void validateOwner(Long userId, OrderDto order);

    /**
     * Validates the owner of order.
     *
     * @param userId - Id of the user, who made the order.
     * @param order  - {@link Order}
     */
    void validateOwner(Long userId, Order order);

    /**
     * Validates the owner of order.
     *
     * @param userId - Id of the user, who made the order.
     * @param order  - {@link OrderDescriptionDto}
     */
    void validateOwner(Long userId, OrderDescriptionDto order);
}
