package com.training.dto;

import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;

/**
 * Class represents a DTO of an order in the online shop.
 *
 * @author Alexandr_Terehov
 */
@Validated
public class OrderDto {
    @Min(value = 1, message = "User Id must be greater than zero")
    private Long id;
    @Min(value = 1, message = "User Id must be greater than zero")
    private Long userId;
    @DecimalMin(value = "0.0", inclusive = true,
            message = "Price can't be negative")
    private BigDecimal totalPrice;
    
    public OrderDto() {
    
    }
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the order.
     * @param userId
     *            id of the user.
     * @param totalPrice
     *            total price of the order.
     */
    public OrderDto(final Long id, @Min(value = 1,
             message = "User Id must be greater than zero") final Long userId,
             @DecimalMin(value = "0.0", inclusive = true) final BigDecimal totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }
    
    /**
     * @return id of the order.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * 
     * @param id
     *            id of the order to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return Id of the user who made the order.
     */
    public Long getUserId() {
        return userId;
    }
    
    /**
     * 
     * @param userId
     *            id of the user to set.
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return total price of the order.
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }
    
    /**
     * 
     * @param totalPrice
     *            totalPrice to set.
     */
    public void setTotalPrice(final BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return hashCode of the object of the {@link OrderDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((userId == null) ? 0 : userId.hashCode())
                + ((totalPrice == null) ? 0 : totalPrice.hashCode()));
    }

    /**
     * Method used to compare this order to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderDto other = (OrderDto) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(totalPrice, other.getTotalPrice())) {
            return false;
        }
        if (!Objects.equals(userId, other.getUserId())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link OrderDto} class .
     */
    @Override
    public String toString() {
        return "OrderDto [id=" + id + ", userId=" + userId + ", totalPrice=" + totalPrice + "]";
    }
}