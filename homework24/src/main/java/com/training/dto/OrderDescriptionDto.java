package com.training.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;

/**
 * Class represents a DTO of an order in the online shop with detailed info.
 *
 * @author Alexandr_Terehov
 */
public class OrderDescriptionDto {
    private final Long id;
    private final Long userId;
    private final BigDecimal totalPrice;
    private final List<ItemDto> itemsDto;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the order.
     * @param userId
     *            id of the user.
     * @param totalPrice
     *            total price of the order.
     * @param itemsDto
     *            list of the {@link ItemDto} class objects.            
     */
    public OrderDescriptionDto(Long id, @Min(value = 1,
             message = "User Id must be greater than zero")Long userId,
             @DecimalMin(value = "1.0", inclusive = true)BigDecimal totalPrice,
             List<ItemDto> itemsDto) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
        this.itemsDto = itemsDto;
    }
    
    /**
     * @return id of the order.
     */
    public Long getId() {
        return id;
    }
   
    /**
     * @return Id of the user who made the order.
     */
    public Long getUserId() {
        return userId;
    }
       
    /**
     * @return total price of the order.
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }
        
    /**
     * @return hashCode of the object of the {@link OrderDescriptionDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((userId == null) ? 0 : userId.hashCode())
                + ((totalPrice == null) ? 0 : totalPrice.hashCode()));
    }
    
    /**
     * @return list of the goods related to the particular order.
     */
    public List<ItemDto> getItemsDto() {
        return itemsDto;
    }

    /**
     * Method used to compare this order to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderDescriptionDto other = (OrderDescriptionDto) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(totalPrice, other.getTotalPrice())) {
            return false;
        }
        if (!Objects.equals(userId, other.getUserId())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link OrderDescriptionDto} class .
     */
    @Override
    public String toString() {
        return "OrderDto [id=" + id + ", userId=" + userId + ", totalPrice=" + totalPrice + "]";
    }
}