package com.training.dto;

import com.training.entity.role.Role;

import java.util.Objects;

import javax.validation.constraints.NotBlank;

/**
 * Class represents DTO of user in the online shop.
 *
 * @author Alexandr_Terehov
 */
public class UserDto {
    private final Long id;
    private final String name;
    private final Role role;
        
    /**
     * Constructor.
     *
     * @param id
     *            id of the user.
     * @param name
     *            name of the user.
     * @param role
     *            {@link Role} of the user.            
     */
    public UserDto(Long id, @NotBlank String name, 
                Role role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }

    /**
     * @return id of the user.
     */
    public Long getId() {
        return id;
    }
       
    /**
     * @return name of the user.
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return role of the user.
     */
    public Role getRole() {
        return role;
    }

    /**
     * @return hashCode of the object of the {@link UserDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode()) 
                + ((name == null) ? 0 : name.hashCode())
                + ((role == null) ? 0 : role.hashCode()));
    }
    
    /**
     * Method used to compare this userDto to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserDto other = (UserDto) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(name, other.getName())) {
            return false;
        }
        if (role != other.getRole()) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link UserDto} class .
     */
    @Override
    public String toString() {
        return "UserDto [id=" + id + ", name=" + name 
                + ", password=" + "*****" + ", role=" + role + "]";
    }
}