package com.training.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.List;
import java.util.Objects;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Class represents a user of the online shop.
 *
 * @author Alexandr_Terehov
 */
@Entity
@Table(name = "user")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "login")
    private String name;

    @Column(name = "password")
    private String password;
    
    @Column(name = "role")
    private String role;
    
    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Order> orderList;
    
    public User() {

    }

    /**
     * Constructor.
     *
     * @param id
     *            id of the user.
     * @param name
     *            name of the user.
     * @param orderList
     *            list of the {@link Order} class objects.            
     */
    public User(final Long id, final String name, final String password,
                final String role, final List<Order> orderList) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.orderList = orderList;
    }

    /**
     * @return id of the user.
     */
    public Long getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the user to set.
     * @throws IllegalArgumentException
     *             if id <0.
     */
    public void setId(Long id) {
        if (id > 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return name of the user.
     */
    public String getName() {
        return name;
    }
      
    /**
     * 
     * @param name
     *            name of the user to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return password of the user.
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * 
     * @param password
     *            password of the user to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * @return role of the user.
     */
    public String getRole() {
        return role;
    }
    
    /**
     * 
     * @param role
     *            role of the user to set.
     */
    public void setRole(String role) {
        this.role = role;
    }
       
    /**
     * @return list of the orders related to the particular user.
     */    
    public List<Order> getOrderList() {
        return orderList;
    }

    /**
     * @return hashCode of the object of the {@link User} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((name == null) ? 0 : name.hashCode())
                + ((password == null) ? 0 : password.hashCode())
                + ((role == null) ? 0 : role.hashCode()));
    }
    
    /**
     * Method used to compare this user to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(name, other.getName())) {
            return false;
        }
        if (!Objects.equals(password, other.getPassword())) {
            return false;
        }
        if (!role.equals(other.getRole())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link User} class .
     */
    @Override
    public String toString() {
        return "User [id=" + id + ", name=" 
                + name + ", password=" + "*****" + ", role=" + role + "]";
    }
}