package com.training.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;

/**
 * Class represents an order in the online shop.
 *
 * @author Alexandr_Terehov
 */
@Entity
@Table(name = "orders")
public class Order {
    
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "user_id")
    @Min(value = 1, message = "User Id must be greater than zero")
    private Long userId;

    @Column(name = "total_price")
    @DecimalMin(value = "0.0", inclusive = true,
            message = "Price can't be negative")
    private BigDecimal totalPrice;
    
    
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "order_item",joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<Item> items = new ArrayList<>();
    
    public Order() {

    }

    /**
     * Constructor.
     *
     * @param id
     *            id of the order.
     * @param userId
     *            id of the user.
     * @param totalPrice
     *            total price of the order.
     * @param items
     *            list of the {@link Item} class objects.            
     */
    public Order(final Long id, final Long userId,
                final BigDecimal totalPrice, final List<Item> items) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
        this.items = items;
    }
    
    /**
     * @return id of the order.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * 
     * @param id
     *            id of the order to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(Long id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return Id of the user who made the order.
     */
    public Long getUserId() {
        return userId;
    }
    
    /**
     * 
     * @param userId
     *            id of the user who made the order.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setUserId(Long userId) {
        if (userId > 0) {
            this.userId = userId;
        } else {
            throw new IllegalArgumentException(" User ID must be > 0");
        } 
    }
    
    /**
     * @return total price of the order.
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }
    
    /**
     * 
     * @param totalPrice
     *            total price of the order.
     * @throws IllegalArgumentException
     *             if totalPrice < 0.
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        if (totalPrice.compareTo(BigDecimal.ZERO) == 1) {
            this.totalPrice = totalPrice;
        } else {
            throw new IllegalArgumentException("Total price can't be < 0");
        } 
    }
    
    
    /**
     * @return list of the items related to the particular order.
     */
    public List<Item> getItems() {
        return items;
    }
    
    public void addToItems(final Item item) {
        this.items.add(item);
    }
    
    /**
     * @return hashCode of the object of the {@link Order} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((userId == null) ? 0 : userId.hashCode())
                + ((totalPrice == null) ? 0 : totalPrice.hashCode()));
    }
    
    /**
     * Method used to compare this order to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Order other = (Order) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(totalPrice, other.getTotalPrice())) {
            return false;
        }
        if (!Objects.equals(userId, other.getUserId())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link Order} class .
     */
    @Override
    public String toString() {
        return "Order [id=" + id + ", userId=" + userId + ", totalPrice=" + totalPrice + "]";
    }
}