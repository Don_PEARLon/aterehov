package com.training.dao;

import com.training.domain.User;

import java.util.List;
import java.util.Optional;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link User} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface UserDao {

    /**
     * @return Returns a list of all users contained in the database.
     */
    List<User> getUsers();

    /**
     * Get User object by it's ID.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link User} class.
     */
    Optional<User> getUserById(final Long id);

    /**
     * Get User object by it's login.
     *
     * @param login
     *            login of the user.
     * @return object of the {@link User} class.
     */
    Optional<User> getUserByLogin(final String login);
}

