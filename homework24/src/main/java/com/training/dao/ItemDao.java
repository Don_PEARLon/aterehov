package com.training.dao;

import com.training.domain.Item;

import java.util.List;
import java.util.Optional;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link Item} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface ItemDao {

    /**
     * @return Returns a list of all items contained in the database.
     */
    List<Item> getItems();

    /**
     * Get Item object by it's ID.
     *
     * @param id
     *            id of the item.
     * @return object of the {@link Item} class.
     */
    Optional<Item> getItemById(final Long id);
}
