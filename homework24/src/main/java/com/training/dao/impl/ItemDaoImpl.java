package com.training.dao.impl;

import com.training.dao.ItemDao;
import com.training.domain.Item;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of the {@link ItemDao} interface. 
 *
 * @author Alexandr_Terehov
 */
@Repository
public class ItemDaoImpl implements ItemDao {
    
    private static final String SELECT_ALL_ITEMS = "from Item";

    private final SessionFactory sessionFactory;

    /**
     * Constructor
     * 
     * @param sessionFactory
     *            object of the {@link SessionFactory} class to set.
     */
    @Autowired 
    public ItemDaoImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * @return Returns a list of all items contained in the database.
     */
    public List<Item> getItems() {
        return (List<Item>)sessionFactory.getCurrentSession()
                            .createQuery(SELECT_ALL_ITEMS).setCacheable(true).list();
    }
        
    /**
     * Get Item object by it's ID.
     *
     * @param id
     *            id of the item.
     * @return object of the {@link Item} class.
     */
    public Optional<Item> getItemById(final Long id) {
        Item item = (Item)sessionFactory.getCurrentSession().get(Item.class, id);
        return Optional.ofNullable(item);
    }
}