package com.training.dao.impl;

import com.training.dao.UserDao;
import com.training.domain.User;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {
    private final SessionFactory sessionFactory;

    private static final String SELECT_ALL_USERS = "from User";
    private static final String GET_USER_BY_LOGIN = "from User where name = :login";

    /**
     * Constructor
     * 
     * @param sessionFactory
     *            object of the {@link SessionFactory} class to set.
     */
    @Autowired
    public UserDaoImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * @return Returns a list of all users contained in the database.
     */
    public List<User> getUsers() {
        List<User> userList = (List<User>)sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL_USERS).setCacheable(true).list();
        return userList;
    }
    
    /**
     * Get User object by it's ID.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link User} class.
     */
    public Optional<User> getUserById(final Long id) {
        User user = (User)sessionFactory.getCurrentSession().get(User.class, id);
        return Optional.ofNullable(user);
    }
    
    /**
     * Get User object by it's login.
     *
     * @param login
     *            login of the user.
     * @return object of the {@link User} class.
     */
    public Optional<User> getUserByLogin(final String login) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery(GET_USER_BY_LOGIN).setCacheable(true);
        query.setString("login", login);
        return Optional.ofNullable((User)query.uniqueResult());
    }
}