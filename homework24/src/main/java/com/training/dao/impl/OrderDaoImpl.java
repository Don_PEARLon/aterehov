package com.training.dao.impl;

import com.training.dao.OrderDao;
import com.training.domain.Order;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class OrderDaoImpl implements OrderDao {
    
    private static final String SELECT_ALL_ORDERS = "from Order";
    private static final String GET_ORDERS_BY_USER_ID =
            "from Order where userId = :id";
    
    private final SessionFactory sessionFactory;
    
    /**
     * Constructor
     * 
     * @param sessionFactory
     *            object of the {@link SessionFactory} class to set.
     */
    @Autowired
    public OrderDaoImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * @return Returns a list of all orders contained in the database.
     */
    public List<Order> getOrders() {
        return (List<Order>)sessionFactory.getCurrentSession()
                           .createQuery(SELECT_ALL_ORDERS).list();
    }
    
    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    public Optional<Order> getOrderById(final Long id) {
        Order order = (Order)sessionFactory.getCurrentSession().get(Order.class, id);
        return Optional.ofNullable(order);
    }
    
    /**
     * Get Order object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link Order} class.
     */
    public List<Order> getOrdersByUserId(final Long id) {
        Query query = sessionFactory.getCurrentSession().createQuery(GET_ORDERS_BY_USER_ID);
        query.setLong("id", id);
        return (List<Order>)query.list();
    }
    
    /**
     * Insert into database information from instance of the {@link Order} class.
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    public void insertOrder(final Order order) {
        sessionFactory.getCurrentSession().save(order);
    }
    
    /**
     * Updates information about specified order.
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    public void updateOrder(final Order order) {
        sessionFactory.getCurrentSession().update(order);
    }
    
    /**
     * Deletes the order from data base.
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    public void deleteOrder(final Order order) {
        sessionFactory.getCurrentSession().delete(order);
    }
}