package com.training.dao;

import com.training.domain.Order;

import java.util.List;
import java.util.Optional;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link Order} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface OrderDao {

    /**
     * @return Returns a list of all orders contained in the database.
     */
    List<Order> getOrders();

    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    List<Order> getOrdersByUserId(final Long id);

    /**
     * Get Order object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link Order} class.
     */
    Optional<Order> getOrderById(final Long id);

    /**
     * Insert into database information from instance of the {@link Order} class.
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    void insertOrder(final Order order);
    
    /**
     * Updates information about specified order.
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    void updateOrder(final Order order);
    
    /**
     * Deletes the order from data base.
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    void deleteOrder(final Order order);
}