package com.training.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import java.io.IOException;
import java.io.PrintWriter;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Custom extension of the {@link BasicAuthenticationEntryPoint}.
 *
 * @author Alexandr_Terehov
 */
public class ShopBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
    private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;
    
    @Override
    public void commence(final HttpServletRequest request, 
            final HttpServletResponse response, 
            final AuthenticationException authException) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
        PrintWriter writer = response.getWriter();
        writer.println("status: " + HTTP_STATUS);
        writer.println("message: " + authException.getLocalizedMessage());
        writer.println("code: " + HTTP_STATUS.value());
    }
     
    @Override
    public void afterPropertiesSet() {
        setRealmName("SHOP_REALM");
        super.afterPropertiesSet();
    }
}
