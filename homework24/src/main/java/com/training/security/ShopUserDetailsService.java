package com.training.security;

import com.training.dao.UserDao;
import com.training.domain.User;
import com.training.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom implementation of the {@link UserDetailsService}.
 *
 * @author Alexandr_Terehov
 */
@Service
@Transactional
public class ShopUserDetailsService implements UserDetailsService {
    private final UserDao userDao;

    /**
     * Constructor.
     *
     * @param userDao
     *            {@link UserDao}.
     */    
    @Autowired
    public ShopUserDetailsService(final UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * 
     * @param username
     *            name of the user.
     * 
     * @return instance of the {@link ShopUserPrincipal}.
     * 
     * @throws NotFoundException
     *                 if user was not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws NotFoundException {
        User user = userDao.getUserByLogin(username)
                    .orElseThrow(() ->  new NotFoundException("User not found"));
        return new ShopUserPrincipal(user);
    }
}