package com.training.converter;

import com.training.domain.Item;
import com.training.dto.ItemDto;

import org.springframework.stereotype.Component;

/**
 * Class provides conversion operations for instances of the {@link Item} and
 * {@link ItemDto} classes.
 *
 * @author Alexandr_Terehov
 */
@Component
public class ItemConverter {
    
    /**
     * Provides conversion of the instance of the {@link ItemDto} class to the
     * instance of the {@link Item} class
     *
     * @param itemDto
     *            instance of the {@link ItemDto} class.
     */
    public Item toEntity(final ItemDto itemDto) {
        return new Item(itemDto.getId(), itemDto.getTitle(), itemDto.getPrice());
    }
    
    /**
     * Provides conversion of the instance of the {@link Item} class to the
     * instance of the {@link ItemDto} class
     *
     * @param item
     *            instance of the {@link Item} class.
     */
    public ItemDto toDto(final Item item) {
        return new ItemDto(item.getId(), item.getTitle(), item.getPrice());
    }
}