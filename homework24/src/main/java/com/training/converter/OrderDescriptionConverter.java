package com.training.converter;

import com.training.domain.Item;
import com.training.domain.Order;
import com.training.dto.ItemDto;
import com.training.dto.OrderDescriptionDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class provides conversion operations for instances of the {@link Order} and
 * {@link OrderDescriptionDto} classes.
 *
 * @author Alexandr_Terehov
 */
@Component
public class OrderDescriptionConverter {
    private final ItemConverter itemConverter;

    /**
     * Constructor.
     *
     * @param itemConverter
     *            {@link ItemConverter}.
     */    
    @Autowired
    public OrderDescriptionConverter(final ItemConverter itemConverter) {
        this.itemConverter = itemConverter;
    }
    
    /**
     * Provides conversion of the instance of the {@link Order} class to the
     * instance of the {@link OrderDescriptionDto} class
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    public OrderDescriptionDto toDto(final Order order) {
        return new OrderDescriptionDto(order.getId(), order.getUserId(),
                order.getTotalPrice(), toItemsDto(order.getItems()));
    }
    
    /**
     * Provides conversion of the list of the {@link Item} objects 
     * into the list of the {@link ItemDto} objects.
     *
     * @param items
     *            List containing objects of the {@link Item} class.
     * @return List containing objects of the {@link ItemDto} class.
     */
    private List<ItemDto> toItemsDto(final List<Item> items) {
        return items.stream().map(itemConverter::toDto).collect(Collectors.toList());
    }
}