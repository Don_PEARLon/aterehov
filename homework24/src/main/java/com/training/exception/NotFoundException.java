package com.training.exception;

/**
 * Defines an exception that can be thrown when requested element was not found in
 * database.
 *
 * @author Alexandr_Terehov
 */
public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 8276396787919764523L;

    private final String message;

    public NotFoundException(String message) {
        this.message = message;
    }

    public NotFoundException() {
        this.message = "Not found";
    }

    @Override
    public String getMessage() {
        return message;
    }
}