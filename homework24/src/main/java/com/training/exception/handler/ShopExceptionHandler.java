package com.training.exception.handler;

import com.training.exception.NotFoundException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

/**
 * Application exception handler.  
 *
 * @author Alexandr_Terehov
 */
@RestControllerAdvice
public class ShopExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * 
     * @param exception
     *            instance of the {@link NotFoundException}.
     * @param request
     *            instance of the {@link WebRequest}.            
     * @return {@link ResponseEntity}.
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(
                Exception exception, WebRequest request) {
        final ShopErrorApi apiError = new ShopErrorApi(HttpStatus.NOT_FOUND,
                exception.getMessage(), "requested resource has been not found");
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
    
    /**
     * 
     * @param ex
     *            instance of the {@link MethodArgumentTypeMismatchException}.
     * @param request
     *            instance of the {@link WebRequest}.             
     * @return {@link ResponseEntity}.
     */
    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            final MethodArgumentTypeMismatchException ex, final WebRequest request) {
        final String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
        final ShopErrorApi apiError = new ShopErrorApi(
                HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
    
    // 400
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        final List<String> errors = new ArrayList<String>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        final ShopErrorApi apiError = new ShopErrorApi(
                     HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    // 404
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            final NoHandlerFoundException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        final String error = "No handler found for " 
                + ex.getHttpMethod() + " " + ex.getRequestURL();
        final ShopErrorApi apiError = new ShopErrorApi(
                HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    // 405
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers,
            final HttpStatus status, final WebRequest request) {
        final StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));
        final ShopErrorApi apiError = new ShopErrorApi(HttpStatus.METHOD_NOT_ALLOWED,
                ex.getLocalizedMessage(), builder.toString());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    // 500
    /**
     * Handles unspecified exceptions.
     * 
     * @param ex
     *            instance of the {@link MethodArgumentTypeMismatchException}.
     * @param request
     *            instance of the {@link WebRequest}.             
     * @return {@link ResponseEntity}.
     */
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        final ShopErrorApi apiError = new ShopErrorApi(
                HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), "error occurred");
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
    
    /**
     * Handles constraint violation exceptions.
     * 
     * @param ex
     *            instance of the {@link ConstraintViolationException}.
     * @param request
     *            instance of the {@link WebRequest}.             
     * @return {@link ResponseEntity}.
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<Object> handleConstraintViolation(
                ConstraintViolationException ex, WebRequest request) {
        List<String> details = ex.getConstraintViolations().parallelStream().map(
                message -> message.getMessage()).collect(Collectors.toList());
        final ShopErrorApi apiError = new ShopErrorApi(
                HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), details);
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}