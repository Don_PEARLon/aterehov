package com.training.service.impl;

import com.training.converter.ItemConverter;
import com.training.converter.OrderConverter;
import com.training.converter.OrderDescriptionConverter;
import com.training.dao.OrderDao;
import com.training.domain.Order;
import com.training.dto.ItemDto;
import com.training.dto.OrderDescriptionDto;
import com.training.dto.OrderDto;
import com.training.exception.NotFoundException;
import com.training.service.OrderService;
import com.training.validator.OrderValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

/**
 * Implementation of the {@link OrderService} interface.
 *
 * @author Alexandr_Terehov
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    private final OrderDao dao;
    
    private final OrderConverter orderConverter;
    private final OrderDescriptionConverter orderDescriptionConverter;
    private final ItemConverter itemConverter;
    private final OrderValidator orderValidator;
        
    /**
     * Constructor
     * 
     * @param dao
     *            instance of the class implements {@link OrderDao} interface.
     * @param orderConverter
     *            instance of the {@link OrderConverter} class.  
     * @param orderDescriptionConverter
     *            instance of the {@link OrderDescriptionConverter} class.
     * @param itemConverter
     *            instance of the {@link ItemConverter} class.                                  
     */
    @Autowired
    public OrderServiceImpl(final OrderDao dao, final OrderConverter orderConverter,
                final OrderDescriptionConverter orderDescriptionConverter,
                final ItemConverter itemConverter, final OrderValidator orderValidator) {
        this.dao = dao;
        this.orderConverter = orderConverter;
        this.orderDescriptionConverter = orderDescriptionConverter;
        this.itemConverter = itemConverter;
        this.orderValidator = orderValidator;
    }
  
    /**
     * @return Returns a list of all users of the online shop.
     */
    public List<OrderDto> getOrders() {
        return dao.getOrders().stream().map(orderConverter::toDto).collect(Collectors.toList());
    }
  
    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    public List<OrderDto> getOrdersByUserId(final Long id) {
        return dao.getOrdersByUserId(id).stream()
                .map(orderConverter::toDto).collect(Collectors.toList());
    }

    /**
     * Get OrderDto object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link OrderDto} class.
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public OrderDto getOrderById(final Long id) {
        return orderConverter.toDto(dao.getOrderById(id)
            .orElseThrow(() ->  new NotFoundException("Order not found")));
    }
    
    /**
     * Get OrderDto object by order id and id of the user.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.            
     * @return object of the {@link OrderDto} class.
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public OrderDto getOrderByUserIdAndOrderId(final Long userId, final Long orderId) {
        OrderDto order = orderConverter.toDto(dao.getOrderById(orderId)
                .orElseThrow(() ->  new NotFoundException("Order not found")));
        orderValidator.validateOwner(userId, order);
        return order;
    }

    /**
     * Saves information about new order.
     *
     * @param orderDto
     *            instance of the {@link OrderDto} class.
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public void saveOrder(@NotNull final OrderDto orderDto) {
        dao.insertOrder(orderConverter.toEntity(orderDto));
    }
    
    /**
     * Adds new item to the order.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.   
     * @param itemDto
     *            {@link ItemDto} item of the online-shop.                      
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public void addItemToOrder(final Long userId, final Long orderId, final ItemDto itemDto) {
        Order order = dao.getOrderById(orderId).orElseThrow(
                () ->  new NotFoundException("Order not found"));
        orderValidator.validateOwner(userId, order);
        order.addToItems(itemConverter.toEntity(itemDto));
        BigDecimal totalPrice = order.getTotalPrice().add(itemDto.getPrice());
        order.setTotalPrice(totalPrice);
        dao.updateOrder(order);
    }
    
    /**
     * Get OrderDescriptionDto object by order id and id of the user.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.            
     * @return object of the {@link OrderDescriptionDto} class.
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public OrderDescriptionDto getOrderWithItemsByUserIdAndOrderId(
            final Long userId, final Long orderId) {
        OrderDescriptionDto order = orderDescriptionConverter.toDto(dao.getOrderById(orderId)
                .orElseThrow(() ->  new NotFoundException("Order not found")));
        orderValidator.validateOwner(userId, order);
        return order;
    }
    
    /**
     * Deletes form database order with specified id.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.   
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public void deleteOrderByUserIdAndOrderId(final Long userId, final Long orderId) {
        Order order = dao.getOrderById(orderId)
                .orElseThrow(() ->  new NotFoundException("Order not found"));
        orderValidator.validateOwner(userId, order);
        dao.deleteOrder(order);
    }
    
    /**
     * Updates information about specified order.
     *
     * @param userId
     *            id of the user.
     * @param orderDto
     *            {@link OrderDto} the order to update.   
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    public void updateOrderByUserId(final Long userId, final OrderDto orderDto) {
        orderValidator.validateOwner(userId, orderDto);
        dao.updateOrder(orderConverter.toEntity(orderDto));
    }
}