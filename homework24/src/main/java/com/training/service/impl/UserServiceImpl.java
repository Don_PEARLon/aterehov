package com.training.service.impl;

import com.training.converter.UserConverter;
import com.training.dao.UserDao;
import com.training.dto.UserDto;
import com.training.exception.NotFoundException;
import com.training.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

/**
 * Implementation of the {@link UserService} interface.
 *
 * @author Alexandr_Terehov
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserDao dao;
    
    private final UserConverter converter;
    
    /**
     * 
     * @param dao
     *            instance of the class implements {@link UserDao} interface.
     * @param converter
     *            instance of the {@link UserConverter} class.
     */
    @Autowired
    public UserServiceImpl(final UserDao dao, final UserConverter converter) {
        this.dao = dao;
        this.converter = converter;
    }
   
    /**
     * @return Returns a list of all users of she online shop.
     */
    @NotEmpty(message = "The users not found")
    public List<UserDto> getUsers() {
        return dao.getUsers().stream().map(converter::toDto).collect(Collectors.toList());
    }

    /**
     * Get UserDto object by ID of the user.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link UserDto} class.
     * @throws {@link NotFoundException}
     *            if user was not found
     */
    public UserDto getUserById(final Long id) {
        return converter.toDto(dao.getUserById(id)
            .orElseThrow(() ->  new NotFoundException("User not found")));
    }

    /**
     * Get UserDto object by user's login.
     *
     * @param login
     *            login of the user.
     * @return object of the {@link UserDto} class.
     * @throws {@link NotFoundException}
     *            if user was not found
     */
    public UserDto getUserByLogin(final String login) {
        return converter.toDto(dao.getUserByLogin(login)
            .orElseThrow(() ->  new NotFoundException("User not found")));
    }
}