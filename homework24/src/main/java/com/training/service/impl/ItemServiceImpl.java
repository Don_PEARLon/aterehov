package com.training.service.impl;

import com.training.converter.ItemConverter;
import com.training.dao.ItemDao;
import com.training.dto.ItemDto;

import com.training.exception.NotFoundException;
import com.training.service.ItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

/**
 * Implementation of the {@link ItemService} interface.
 *
 * @author Alexandr_Terehov
 */
@Service
@Transactional
public class ItemServiceImpl implements ItemService {
    private final ItemDao dao;
        
    private final ItemConverter converter;
    
    /**
     * Constructor
     * 
     * @param dao
     *            instance of the class implements {@link ItemDao} interface.
     * @param converter
     *            instance of the class implements {@link ItemConverter} interface.            
     */
    @Autowired
    public ItemServiceImpl(final ItemDao dao, final ItemConverter converter) {
        this.dao = dao;
        this.converter = converter;
    }
    
    /**
     * Get ItemDto object by it's ID.
     *
     * @param id
     *            id of the item.
     * @return object of the {@link ItemDto} class.
     * @throws {@link NotFoundException}
     *            if item was not found. 
     * 
     */
    public ItemDto getItemById(final Long id) {
        return  converter.toDto(dao.getItemById(id)
            .orElseThrow(() ->  new NotFoundException("Item not found")));
    }
    
    /**
     * @return Returns a list of all items of the online shop.
     */
    @NotEmpty(message = "The List of items is empty")
    public List<ItemDto> getItems() {
        return dao.getItems().stream().map(converter::toDto).collect(Collectors.toList());
    }
}