package com.training.service;

import com.training.dto.ItemDto;

import java.util.List;

/**
 * Interface used for representing a item service which provides various
 * operations with items of the online shop.
 *
 * @author Alexandr_Terehov
 */
public interface ItemService {

    /**
     * @return Returns a list of all items of the online shop.
     */
    List<ItemDto> getItems();

    /**
     * Get ItemDto object by it's ID.
     *
     * @param id
     *            id of the item.
     * @return object of the {@link ItemDto} class.
     */
    ItemDto getItemById(final Long id);
}