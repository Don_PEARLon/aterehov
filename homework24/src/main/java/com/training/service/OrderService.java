package com.training.service;

import com.training.dto.ItemDto;
import com.training.dto.OrderDescriptionDto;
import com.training.dto.OrderDto;
import com.training.exception.NotFoundException;

import java.util.List;

/**
 * Interface used for representing a user service which provides various
 * operations with orders of the online shop.
 *
 * @author Alexandr_Terehov
 */
public interface OrderService {

    /**
     * @return Returns a list of all users of the online shop.
     */
    List<OrderDto> getOrders();

    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    List<OrderDto> getOrdersByUserId(final Long id);

    /**
     * Get OrderDto object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link OrderDto} class.
     */
    OrderDto getOrderById(final Long id);

    /**
     * Saves information about new order.
     *
     * @param orderDto
     *            instance of the {@link OrderDto} class.
     */
    void saveOrder(final OrderDto orderDto);
    
    /**
     * Adds new item to the order.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.   
     * @param itemDto
     *            {@link ItemDto} item of the online-shop.                      
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    void addItemToOrder(final Long userId, final Long orderId, final ItemDto itemDto);
    
    /**
     * Get OrderDescriptionDto object by order id and id of the user.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.            
     * @return object of the {@link OrderDescriptionDto} class.
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    OrderDescriptionDto getOrderWithItemsByUserIdAndOrderId(final Long userId, final Long orderId);
    
    /**
     * Get OrderDto object by order id and id of the user.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.            
     * @return object of the {@link OrderDto} class.
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    OrderDto getOrderByUserIdAndOrderId(final Long userId, final Long orderId);
    
    /**
     * Deletes form database order with specified id.
     *
     * @param userId
     *            id of the user.
     * @param orderId
     *            id of the order.   
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    void deleteOrderByUserIdAndOrderId(final Long userId, final Long orderId);
    
    /**
     * Updates information about specified order.
     *
     * @param userId
     *            id of the user.
     * @param orderDto
     *            {@link OrderDto} the order to update.   
     * @throws {@link NotFoundException}
     *            if order was not found. 
     */
    void updateOrderByUserId(final Long userId, final OrderDto orderDto);
}