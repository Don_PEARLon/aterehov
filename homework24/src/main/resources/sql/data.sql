INSERT INTO user VALUES (null,'user1','$2y$10$IvqLVhEstyWCtb1uKCsfUub2QLazuYPvYE3rgvseurRE6snRXzFyW','USER');
INSERT INTO user VALUES (null,'user2','$2y$10$3t1Z.X/ZRVUEhmicv52ycuAn7m2r94dY/WAyJlvlruNs47qw9NIZS','USER');
INSERT INTO user VALUES (null,'user3','$2y$10$rly73QU6O8ItF8ecu4irHut/CkfaMAsfO5n0V55DHuV0I842e73Mu','USER');
INSERT INTO user VALUES (null,'admin','$2y$10$acD.lLNRyeopD9ycTxNwme6IVJhwMMb7uzGtXjhyYg.j9jBUxDDkG','ADMIN');

INSERT INTO item VALUES (null,'Mobile Phone',10.0);
INSERT INTO item VALUES (null,'Book',5.5);
INSERT INTO item VALUES (null,'Laptop',310.15);
INSERT INTO item VALUES (null,'TV',250.0);

INSERT INTO orders VALUES (null,1,265.5);
INSERT INTO orders VALUES (null,1,315.65);
INSERT INTO orders VALUES (null,2,255.5);

INSERT INTO order_item VALUES (null,1,4);
INSERT INTO order_item VALUES (null,1,1);
INSERT INTO order_item VALUES (null,1,2);
INSERT INTO order_item VALUES (null,2,3);
INSERT INTO order_item VALUES (null,2,2);
INSERT INTO order_item VALUES (null,3,2);
INSERT INTO order_item VALUES (null,3,4);




