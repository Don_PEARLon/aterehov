package com.training;

import com.training.converter.ItemConverter;
import com.training.domain.Item;
import com.training.dto.ItemDto;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for the ItemConverter class.
 *
 * @author Alexandr_Terehov
 */
public class ItemConverterTest {
    private ItemConverter itemConverter; 
    private Item item;
    private ItemDto itemDto;

    @Before
    public void initObjects() {
        itemConverter = new ItemConverter();
        item = new Item(1L, "item", new BigDecimal(100));
        itemDto = new ItemDto(1L, "item", new BigDecimal(100));
    }

    @Test
    public void testToDto() {
        //given
        ItemDto expectedItemDto = new ItemDto(1L, "item", new BigDecimal(100));

        //when
        ItemDto resultItemDto = itemConverter.toDto(item);

        //then
        assertEquals(expectedItemDto, resultItemDto);
    }

    @Test
    public void testToEntity() {
        //given
        Item expectedItem = new Item(1L, "item", new BigDecimal(100));

        //when
        Item resultItem = itemConverter.toEntity(itemDto);

        //then
        assertEquals(expectedItem, resultItem);
    }
}