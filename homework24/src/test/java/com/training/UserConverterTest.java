package com.training;

import com.training.converter.UserConverter;
import com.training.domain.Item;
import com.training.domain.Order;
import com.training.domain.User;
import com.training.dto.UserDto;
import com.training.entity.role.Role;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for the UserConverter class.
 *
 * @author Alexandr_Terehov
 */
public class UserConverterTest {
  
    private List<Item> itemList1;
    private List<Item> itemList2;
    private Item item1;
    private Item item2;
    private Item item3;

    private Order order1;
    private Order order2;

    private List<Order> orderList1;

    private User user;
       
    private UserConverter userConverter;
    
    @Before
    public void initObjects() {

        itemList1 = new ArrayList<>();
        itemList2 = new ArrayList<>();
        
        item1 = new Item(1L, "item1", new BigDecimal(100));
        item2 = new Item(2L, "item1", new BigDecimal(200));
        item3 = new Item(3L, "item3" , new BigDecimal(300));

        itemList1.add(item1);
        itemList1.add(item3);
        itemList2.add(item2);
        itemList2.add(item2);

        order1 = new Order(1L, 1L, new BigDecimal(400), itemList1);
        order2 = new Order(2L, 1L, new BigDecimal(400), itemList2);

        orderList1 = new ArrayList<>();

        orderList1.add(order1);
        orderList1.add(order2);

        user = new User(1L, "user1", "pass1", "USER", orderList1);

        userConverter = new UserConverter();
    }
    
    @Test
    public void testToDto() {
        //given
        UserDto expectedUserDto  = new UserDto(1L, "user1", Role.USER);

        //when
        UserDto resultUserDto = userConverter.toDto(user);
        
        //then
        assertEquals(resultUserDto, expectedUserDto);
    }
}