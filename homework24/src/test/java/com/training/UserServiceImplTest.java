package com.training;

import com.training.converter.UserConverter;
import com.training.dao.UserDao;
import com.training.domain.Item;
import com.training.domain.Order;
import com.training.domain.User;
import com.training.dto.UserDto;
import com.training.entity.role.Role;
import com.training.exception.NotFoundException;
import com.training.service.impl.UserServiceImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the UserServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    private UserDao userDao;
    
    private UserServiceImpl userServiceImpl;
    
    private UserConverter userConverter;
    
    private List<Item> itemList1;
    private List<Item> itemList2;
    private Item item1;
    private Item item2;
    private Item item3;

    private Order order1;
    private Order order2;

    private List<Order> orderList1;
    private List<Order> orderList2;
    private List<Order> orderList3;

    private List<User> userList;
    private List<UserDto> userListDto;
    
    private User user1;
    private User user2;
    private User user3;
    
    private UserDto userDto1;
    private UserDto userDto2;
    private UserDto userDto3;
    
    @Before
    public void initObjects() {
        userConverter = new UserConverter(); 
    
        userServiceImpl = new UserServiceImpl(userDao, userConverter);
    
        itemList1 = new ArrayList<>();
        itemList2 = new ArrayList<>();

        item1 = new Item(1L, "item1", new BigDecimal(100));
        item2 = new Item(2L, "item1", new BigDecimal(200));
        item3 = new Item(3L, "item3" , new BigDecimal(300));

        itemList1.add(item1);
        itemList1.add(item3);
        itemList2.add(item2);
        itemList2.add(item2);

        order1 = new Order(1L, 1L, new BigDecimal(400), itemList1);
        order2 = new Order(2L, 1L, new BigDecimal(400), itemList2);

        orderList1 = new ArrayList<>();
        orderList2 = new ArrayList<>();
        orderList3 = new ArrayList<>();

        orderList1.add(order1);
        orderList1.add(order2);
    
        userList = new ArrayList<>();
        userListDto = new ArrayList<>();
    
        user1 = new User(1L, "user1", "pass1", "USER", orderList1);
        user2 = new User(2L, "user2", "pass2", "USER", orderList2);
        user3 = new User(3L, "user3", "pass3", "USER", orderList3);
    
        userDto1 = new UserDto(1L, "user1", Role.USER);
        userDto2 = new UserDto(2L, "user2", Role.USER);
        userDto3 = new UserDto(3L, "user3", Role.USER);
  	
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
    
        userListDto.add(userDto1);
        userListDto.add(userDto2);
        userListDto.add(userDto3);
    }

    @Test
    public void testGetUserList() {
        //given:
        Mockito.when(userDao.getUsers()).thenReturn(userList);
        
        //when
        List<UserDto> resultUserDtoList = userServiceImpl.getUsers();
        
        //then
        assertEquals(resultUserDtoList, userListDto);
        Mockito.verify(userDao).getUsers();
    }

    @Test
    public void testGetUserById() {
        //given:
        Long userId = 1L;
        Mockito.when(userDao.getUserById(userId)).thenReturn(Optional.ofNullable(user1));
        
        //when
        UserDto resultUserDto= userServiceImpl.getUserById(userId);
        
        //then
        assertEquals(resultUserDto, userDto1);
        Mockito.verify(userDao).getUserById(userId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserByIdNegative() {
        //given:
        Long userId = 4L;
        Mockito.when(userDao.getUserById(userId)).thenReturn(Optional.ofNullable(null));
        
        //when
        userServiceImpl.getUserById(userId);
        
        //then
        Mockito.verify(userDao).getUserById(userId);
    }

    @Test
    public void testGetUserByLogin() {
        //given:
        String userLogin = "user1";
        Mockito.when(userDao.getUserByLogin(userLogin)).thenReturn(Optional.ofNullable(user1));
        
        //when
        UserDto resultUserDto = userServiceImpl.getUserByLogin(userLogin);
        
        //then
        assertEquals(resultUserDto, userDto1);
        Mockito.verify(userDao).getUserByLogin(userLogin);
    }

    @Test(expected = NotFoundException.class)
    public void testGetUserByLoginNegative() {
        //given:
        String userLogin = "user4";
        Mockito.when(userDao.getUserByLogin(userLogin)).thenReturn(Optional.ofNullable(null));
        
        //when
        userServiceImpl.getUserByLogin(userLogin);
        
        //then
        Mockito.verify(userDao).getUserByLogin(userLogin);
    }
}