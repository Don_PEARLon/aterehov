package com.training;

import com.training.converter.ItemConverter;
import com.training.converter.OrderDescriptionConverter;
import com.training.domain.Item;
import com.training.domain.Order;
import com.training.dto.ItemDto;
import com.training.dto.OrderDescriptionDto;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for the OrderDescriptionConverter class.
 *
 * @author Alexandr_Terehov
 */
public class OrderDescriptionConverterTest {
    private OrderDescriptionConverter orderDescriptionConverter;
    private ItemConverter itemConverter; 

    private List<Item> itemList;
    private Item item1;
    private Item item2;
    private Item item3;

    private List<ItemDto> itemListDto;
    private ItemDto itemDto1;
    private ItemDto itemDto2;
    private ItemDto itemDto3;

    private Order order;

    @Before
    public void initObjects() {
        itemConverter = new ItemConverter();
        orderDescriptionConverter = new OrderDescriptionConverter(itemConverter);

        itemList = new ArrayList<>();
        itemListDto = new ArrayList<>();

        item1 = new Item(1L, "item1", new BigDecimal(100));
        item2 = new Item(2L, "item1", new BigDecimal(200));
        item3 = new Item(3L, "item3" , new BigDecimal(300));

        itemDto1 = new ItemDto(1L, "item1", new BigDecimal(100));
        itemDto2 = new ItemDto(2L, "item1", new BigDecimal(200));
        itemDto3 = new ItemDto(3L, "item3" , new BigDecimal(300));

        itemList.add(item1);
        itemList.add(item2);
        itemList.add(item3);

        itemListDto.add(itemDto1);
        itemListDto.add(itemDto2);
        itemListDto.add(itemDto3);

        order = new Order(1L, 1L, new BigDecimal(600), itemList);
    }

    @Test
    public void testToDto() {
        //given
        OrderDescriptionDto expectedOrderDescriptionDto = 
                new OrderDescriptionDto(1L, 1L, new BigDecimal(600), itemListDto);

        //when
        OrderDescriptionDto resultOrderDescriptionDto = 
                orderDescriptionConverter .toDto(order);

        //then
        assertEquals(expectedOrderDescriptionDto, resultOrderDescriptionDto);
    }
}