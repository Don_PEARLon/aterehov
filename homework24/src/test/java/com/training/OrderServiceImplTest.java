package com.training;

import com.training.converter.ItemConverter;
import com.training.converter.OrderConverter;
import com.training.converter.OrderDescriptionConverter;
import com.training.dao.OrderDao;
import com.training.domain.Item;
import com.training.domain.Order;
import com.training.dto.ItemDto;
import com.training.dto.OrderDescriptionDto;
import com.training.dto.OrderDto;
import com.training.exception.NotFoundException;
import com.training.service.impl.OrderServiceImpl;
import com.training.validator.OrderValidator;
import com.training.validator.impl.OrderValidatorImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the OrderServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
    @Mock
    private OrderDao orderDao;
	
    private OrderServiceImpl orderServiceImpl;

    private ItemConverter itemConverter; 
    private OrderConverter orderConverter; 
    private OrderDescriptionConverter orderDescriptionConverter; 

    private OrderValidator orderValidator;

    private List<Item> itemList1;
    private List<Item> itemList2;
    private List<Item> itemList3;

    private Item item1;
    private Item item2;
    private Item item3;

    private List<ItemDto> itemListDto1;

    private ItemDto itemDto1;

    private List<Order> orderList;
    private Order order1;
    private Order order2;
    private Order order3;

    private List<OrderDto> orderListDto;
    private OrderDto orderDto1;
    private OrderDto orderDto2;
    private OrderDto orderDto3;

    private OrderDescriptionDto orderDescriptionDto;

    @Before
    public void initObjects() {
        itemConverter = new ItemConverter();
        orderConverter = new OrderConverter();
        orderValidator = new OrderValidatorImpl();
        orderDescriptionConverter = new OrderDescriptionConverter(itemConverter);
        orderServiceImpl = new OrderServiceImpl(orderDao, orderConverter,
                       orderDescriptionConverter, itemConverter, orderValidator);

        itemList1 = new ArrayList<>();
        itemList2 = new ArrayList<>();
        itemList3 = new ArrayList<>();

        itemListDto1 = new ArrayList<>();

        item1 = new Item(1L, "item1", new BigDecimal(100));
        item2 = new Item(2L, "item1", new BigDecimal(200));
        item3 = new Item(3L, "item3" , new BigDecimal(300));

        itemDto1 = new ItemDto(1L, "item1", new BigDecimal(100));

        itemList1.add(item1);
        itemList2.add(item2);
        itemList3.add(item3);

        itemListDto1.add(itemDto1);

        orderList = new ArrayList<>();
        orderListDto = new ArrayList<>();

        order1 = new Order(1L, 1L, new BigDecimal(100), itemList1);
        order2 = new Order(2L, 1L, new BigDecimal(200), itemList2);
        order3 = new Order(3L, 2L , new BigDecimal(300), itemList3);

        orderDto1 = new OrderDto(1L, 1L, new BigDecimal(100));
        orderDto2 = new OrderDto(2L, 1L, new BigDecimal(200));
        orderDto3 = new OrderDto(3L, 2L, new BigDecimal(300));

        orderDescriptionDto = new OrderDescriptionDto(
                    1L, 1L, new BigDecimal(100), itemListDto1);

        orderList.add(order1);
        orderList.add(order2);
        orderList.add(order3);

        orderListDto.add(orderDto1);
        orderListDto.add(orderDto2);
        orderListDto.add(orderDto3);
    }

    @Test
    public void testGetOrderList() {
        //given:
        Mockito.when(orderDao.getOrders()).thenReturn(orderList);
       
        //when
        List<OrderDto> resultOrderDtoList = orderServiceImpl.getOrders();
        
        //then
        assertEquals(resultOrderDtoList, orderListDto);
        Mockito.verify(orderDao).getOrders();
    }

    @Test
    public void testGetOrderById() {
        //given:
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));
        
        //when
        OrderDto resultOrderDto = orderServiceImpl.getOrderById(orderId);
        
        //then
        assertEquals(resultOrderDto, orderDto1);
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetOrderByIdNegative() {
        //given:
        Long orderId = 4L; 
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(null));
        
        //when
        orderServiceImpl.getOrderById(orderId);
        
        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test
    public void testGetOrderByUserIdAndOrderId() {
        //given
        Long userId = 1L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));

        //when
        OrderDto resultOrderDto = orderServiceImpl.getOrderByUserIdAndOrderId(userId, orderId);

        //then
        assertEquals(resultOrderDto, orderDto1);
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetOrderByUserIdAndOrderIdNegativeOrder() {
        //given
        Long userId = 1L;
        Long orderId = 4L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(null));

        //when
        orderServiceImpl.getOrderByUserIdAndOrderId(userId, orderId);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetOrderByUserIdAndOrderIdNegativeUser() {
        //given
        Long userId = 2L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));

        //when
        orderServiceImpl.getOrderByUserIdAndOrderId(userId, orderId);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testAddItemToOrderNegativeOrder() {
        //given
        Long userId = 1L;
        Long orderId = 4L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(null));

        //when
        orderServiceImpl.addItemToOrder(userId, orderId, itemDto1);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testAddItemToOrderNegativeUser() {
        //given
        Long userId = 2L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));

        //when
        orderServiceImpl.addItemToOrder(userId, orderId, itemDto1);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test
    public void testDeleteOrderByUserIdAndOrderId() {
        //given
        Long userId = 1L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));
        
        //when
        orderServiceImpl.deleteOrderByUserIdAndOrderId(userId, orderId);
        
        //then
        Mockito.verify(orderDao).deleteOrder(order1);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteOrderByUserIdAndOrderIdNegativeOrder() {
        //given
        Long userId = 1L;
        Long orderId = 4L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(null));

        //when
        orderServiceImpl.deleteOrderByUserIdAndOrderId(userId, orderId);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteOrderByUserIdAndOrderIdNegativeUser() {
        //given
        Long userId = 2L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));

        //when
        orderServiceImpl.deleteOrderByUserIdAndOrderId(userId, orderId);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void updateOrderByUserIdNegative() {
        //given
        Long userId = 2L;
        
        //when
        orderServiceImpl.updateOrderByUserId(userId, orderDto1);
    }

    @Test
    public void updateOrderByUserId() {
        //given
        Long userId = 1L;
        
        //when
        orderServiceImpl.updateOrderByUserId(userId, orderDto1);
        
        //then
        Mockito.verify(orderDao).updateOrder(order1);
    }

    @Test
    public void testGetOrderWithItemsByUserIdAndOrderId() {
        //given
        Long userId = 1L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));

        //when
        OrderDescriptionDto resultOrderDescriptionDto = 
                    orderServiceImpl.getOrderWithItemsByUserIdAndOrderId(userId, orderId);

        //then
        assertEquals(resultOrderDescriptionDto, orderDescriptionDto);
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetOrderWithItemsByUserNegative() {
        //given
        Long userId = 1L;
        Long orderId = 4L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(null));

        //when
        orderServiceImpl.getOrderWithItemsByUserIdAndOrderId(userId, orderId);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetOrderWithItemsByOrderNegative() {
        //given
        Long userId = 2L;
        Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(Optional.ofNullable(order1));

        //when
        orderServiceImpl.getOrderWithItemsByUserIdAndOrderId(userId, orderId);

        //then
        Mockito.verify(orderDao).getOrderById(orderId);
    }
}