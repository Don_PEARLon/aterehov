package com.training;

import com.training.converter.OrderConverter;
import com.training.domain.Item;
import com.training.domain.Order;
import com.training.dto.OrderDto;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for the OrderConverter class.
 *
 * @author Alexandr_Terehov
 */
public class OrderConverterTest {
    private List<Item> itemList1;

    private Item item1;
    private Item item2;
    private Item item3;
    
    private Order order;
    private OrderDto orderDto;

    private OrderConverter orderConverter;

    @Before
    public void initObjects() {
        orderConverter = new OrderConverter();

        itemList1 = new ArrayList<>();

        item1 = new Item(1L, "item1", new BigDecimal(100));
        item2 = new Item(2L, "item1", new BigDecimal(200));
        item3 = new Item(3L, "item3" , new BigDecimal(300));

        itemList1.add(item1);
        itemList1.add(item2);
        itemList1.add(item3);

        order = new Order(1L, 1L, new BigDecimal(600), itemList1);
        orderDto = new OrderDto(1L, 1L, new BigDecimal(600));
    }

    @Test
    public void testToDto() {
        //given
        OrderDto expectedOrderDto = new OrderDto(1L, 1L, new BigDecimal(600));

        //when
        OrderDto resultOrderDto = orderConverter.toDto(order);

        //then
        assertEquals(expectedOrderDto, resultOrderDto);
    }

    @Test
    public void testToEntity() {
        //given
        Order expectedOrder = new Order(1L, 1L, new BigDecimal(600), new ArrayList<>());

        //when
        Order resultOrder = orderConverter.toEntity(orderDto);

        //then
        assertEquals(expectedOrder, resultOrder);
    }
}