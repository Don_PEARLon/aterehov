package com.training;

import com.training.converter.ItemConverter;
import com.training.dao.ItemDao;
import com.training.domain.Item;
import com.training.dto.ItemDto;
import com.training.exception.NotFoundException;
import com.training.service.impl.ItemServiceImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the ItemServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class ItemServiceImplTest {
    @Mock
    private ItemDao itemDao;

    private ItemServiceImpl itemServiceImpl;

    private ItemConverter itemConverter; 

    private List<Item> itemList;
    private Item item1;
    private Item item2;
    private Item item3;

    private List<ItemDto> itemListDto;
    private ItemDto itemDto1;
    private ItemDto itemDto2;
    private ItemDto itemDto3;

    @Before
    public void initObjects() {
        itemConverter = new ItemConverter();
        itemServiceImpl = new ItemServiceImpl(itemDao, itemConverter);

        itemList = new ArrayList<>();
        itemListDto = new ArrayList<>();

        item1 = new Item(1L, "item1", new BigDecimal(100));
        item2 = new Item(2L, "item1", new BigDecimal(200));
        item3 = new Item(3L, "item3" , new BigDecimal(300));

        itemDto1 = new ItemDto(1L, "item1", new BigDecimal(100));
        itemDto2 = new ItemDto(2L, "item1", new BigDecimal(200));
        itemDto3 = new ItemDto(3L, "item3" , new BigDecimal(300));

        itemList.add(item1);
        itemList.add(item2);
        itemList.add(item3);

        itemListDto.add(itemDto1);
        itemListDto.add(itemDto2);
        itemListDto.add(itemDto3);
    }

    @Test
    public void testGetItems() {
        //given:
        Mockito.when(itemDao.getItems()).thenReturn(itemList);
       
        //when
        List<ItemDto> resultItemDtotList = itemServiceImpl.getItems();
        
        //then
        assertEquals(resultItemDtotList, itemListDto);
        Mockito.verify(itemDao).getItems();
    }

    @Test
    public void testGetItemById() {
        //given
        Long itemId = 1L;
        Mockito.when(itemDao.getItemById(itemId)).thenReturn(Optional.ofNullable(item1));
        
        //when
        ItemDto resultItemDto = itemServiceImpl.getItemById(itemId);
        
        //then
        assertEquals(resultItemDto, itemDto1);
        Mockito.verify(itemDao).getItemById(itemId);
    }

    @Test(expected = NotFoundException.class)
    public void testGetItemByIdNegative() {
        Long itemId = 4L;

        //given
        Mockito.when(itemDao.getItemById(itemId)).thenReturn(Optional.ofNullable(null));
        
        //when
        itemServiceImpl.getItemById(itemId);
        
        //then
        Mockito.verify(itemDao).getItemById(itemId);
    }
}