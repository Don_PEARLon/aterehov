package com.training.homework13.task1;

import com.training.homework13.task1.entity.Article;
import com.training.homework13.task1.handler.ArticleHttpReqHandler;
import com.training.homework13.task1.service.ArticleService;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for ArticleHttpReqHandler class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class ArticleHttpReqHandlerTest {
    @Mock
    private ArticleService articleService;
    
    @InjectMocks
    private ArticleHttpReqHandler requestHandler;
   
    private String json;
    private Article articleExpected;
    
    @Before 
    public void initObjects() {
    	json = "{\r\n"  
                + "  \"id\" : \"" + "1" + "\",\r\n" 
                + "  \"title\": \"MyTitle\",\r\n"  
                + "  \"body\": \"Some Body\",\r\n"  
                + "  \"userId\": \"3\"\r\n"  
                + "}";
    	articleExpected = new Article(1);
    	articleExpected.setUserId(3);
    	articleExpected.setTitle("MyTitle");
    	articleExpected.setBody("Some Body");
    }
        
	@Test
	public void testCreateArticleInstanceGetReq() throws Exception {
		Mockito.when(articleService.doGetRequest("1")).thenReturn(json);
		Article article = requestHandler.createArticleInstGetReq("1");
		boolean condition = articleExpected.equals(article);
		assertTrue(condition);
		Mockito.verify(articleService).doGetRequest("1");
	}

	@Test
	public void testCreateArticleInstancePostReq() throws Exception {
		Mockito.when(articleService.doPostRequest("1")).thenReturn(json);
		Article article = requestHandler.createArticleInstPostReq("1");
		boolean condition = articleExpected.equals(article);
		assertTrue(condition);
		Mockito.verify(articleService).doPostRequest("1");
	}
}
