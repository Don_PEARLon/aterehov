package com.training.homework13.task1;

import com.training.homework13.task1.builder.ArticleServiceFactory;
import com.training.homework13.task1.handler.ArticleHttpReqHandler;
import com.training.homework13.task1.service.ArticleService;
import com.training.homework13.task1.util.ArgumentsBase;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main application class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {
    private static final Logger logger = LoggerFactory.getLogger(RunnerTask1.class);    
    
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        try {
            ArgumentsBase.check(args);
            ArticleService articleService;
            if (args.length < 3) {
                articleService = ArticleServiceFactory.getArticleServiceInstance(0);
            } else {
                int arg = Integer.parseInt(args[2]);
                articleService = ArticleServiceFactory.getArticleServiceInstance(arg);
            }
            ArticleHttpReqHandler httpRequestHandler = new ArticleHttpReqHandler(articleService);
            if (args[0].toLowerCase().equals("get")) {
                httpRequestHandler.printResponseToGetRequest(args[1]);
            } else if (args[0].toLowerCase().equals("post")) {
                httpRequestHandler.printResponseToPostRequest(args[1]);
            }
        } catch (Exception exc) {
            logger.error("{}", exc);
            System.out.println("ERROR: " + exc.getMessage());
        }
    }
}
