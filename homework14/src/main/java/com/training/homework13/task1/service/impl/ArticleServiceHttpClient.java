package com.training.homework13.task1.service.impl;

import com.training.homework13.task1.exception.ArticleServiceException;
import com.training.homework13.task1.service.ArticleService;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Implementation of the ArticleService interface. HTTP requests are executed
 * with an instance of the HttpClient class.
 *
 * @author Alexandr_Terehov
 */
public class ArticleServiceHttpClient implements ArticleService {
    private static final Logger logger
            = LoggerFactory.getLogger(ArticleServiceHttpClient.class); 
    
    /**
     * Method used to provide 'GET' request.
     *
     * @param id
     *            id of the article.
     * @return HTTP response code.
     * @throws Exception
     *             may occur while request execution.
     */
    public String doGetRequest(String id) throws Exception {
        String url = SUB_URL + id;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet(url);
        HttpResponse response = client.execute(getRequest);
        int responseCode = response.getStatusLine().getStatusCode();
        if (responseCode >= 200 && responseCode < 300) {
            String responseStr = readResponse(response);
            logger.info("GET request has been executed with HttpClient. Article ID: " + id + ".");
            return responseStr;
        } else {
            String message = "Unexpected response status: " + responseCode;
            throw new ArticleServiceException(message);
        }
    }
    
    /**
     * Method used to provide 'POST' request.
     *
     * @param id
     *            id of the article.
     * @return HTTP response code.
     * @throws Exception
     *             may occur while request execution.
     */
    public String doPostRequest(String id) throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost postRequest = new HttpPost(SUB_URL);
        postRequest.setHeader("Accept", "application/json");
        postRequest.setHeader("Content-type", "application/json");
        String json = "{\r\n"  
                + "  \"id\" : \"" + id + "\",\r\n" 
                + "  \"title\": \"MyTitle\",\r\n"  
                + "  \"body\": \"Some Body\",\r\n"  
                + "  \"userId\": \"3\"\r\n"  
                + "}";
        StringEntity stringEntity = new StringEntity(json);
        postRequest.setEntity(stringEntity);
        HttpResponse response = client.execute(postRequest);
        int responseCode = response.getStatusLine().getStatusCode();
        if (responseCode >= 200 && responseCode < 300) {
            String responseStr = readResponse(response);
            logger.info("POST request has been executed with HttpClient. Article ID: " + id + ".");
            return responseStr;
        } else {
            String message = "Unexpected response status: " + responseCode;
            throw new ArticleServiceException(message);
        }
    }
           
    /**
     * Method used to read information from the HTTP response.
     *
     * @param response
     *            instance of the HttpResponse class.
     * @return HTTP response in String format.
     * @throws Exception
     *             may occur while request execution.
     */
    private String readResponse(HttpResponse response) throws Exception {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        String inputLine;
        StringBuffer responseStrBuf = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            responseStrBuf.append(inputLine);
        }
        in.close();
        return responseStrBuf.toString();
    }
}