package com.training.homework13.task1.entity;

/**
 * Class represents an Article.
 *
 * @author Alexandr_Terehov
 */
public class Article {
    private int id;
    private int userId;
    private String title;
    private String body;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the article.
     */
    public Article(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("Id must be > 0 ");
        } else {
            this.id = id;
        }
    }
    
    /**
     * @return id of the article.
     */
    public int getId() {
        return id;
    }
    
    /**
     * 
     * @param id
     *            id of the article to set.
     */
    public void setId(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("Id must be > 0 ");
        } else {
            this.id = id;
        }
    }
    
    /**
     * @return id of the article's author.
     */
    public int getUserId() {
        return userId;
    }
    
    /**
     * 
     * @param userId
     *            id of the the article's author to set.
     */
    public void setUserId(int userId) {
        if (id <= 0) {
            throw new IllegalArgumentException("Id must be > 0 ");
        } else {
            this.userId = userId;
        }
    }
    
    /**
     * @return title of the article.
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * 
     * @param title
     *            title of the article to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * @return body of the article.
     */
    public String getBody() {
        return body;
    }
    
    /**
     * 
     * @param body
     *            body of the article to set.
     */
    public void setBody(String body) {
        this.body = body;
    }
    
    /**
     * @return hashCode of the object of the Article class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * id + userId + ((title == null) ? 0 : title.hashCode())
                + ((body == null) ? 0 : body.hashCode()));
    }
    
    /**
     * Method used to compare this article to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Article other = (Article) obj;
        if (id != other.id) {
            return false;
        }
        if (userId != other.userId) {
            return false;
        }
        if (!(title.equals(other.title))) {
            return false;
        }
        if (!(body.equals(other.body))) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the Article class .
     */
    @Override
    public String toString() {
        String articleStr = "User [" + userId + "]" + "\n"
                + "Title [" + title + "]" + "\n"
                + "Body [" + body + "]" + "\n";
        return articleStr;
    }
}
