package com.training.homework13.task1.service;

/**
 * Interface used for representing article service which provides HTTP requests
 * to remote article web service.
 *
 * @author Alexandr_Terehov
 */
public interface ArticleService {
    String SUB_URL = "https://jsonplaceholder.typicode.com/posts/";
      
    /**
     * Method used to provide 'GET' request.
     *
     * @param id
     *          id of the article.
     * @return String result of the request.
     * @throws Exception
     *             may occur while request execution.
     */
    String doGetRequest(String id) throws Exception;
          
    /**
     * Method used to provide 'POST' request.
     *
     * @param id
     *          id of the article.
     * @return String result of the request.
     * @throws Exception
     *             may occur while request execution.
     */
    String doPostRequest(String id) throws Exception;
}
