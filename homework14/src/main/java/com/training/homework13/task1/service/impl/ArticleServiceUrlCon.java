package com.training.homework13.task1.service.impl;

import com.training.homework13.task1.exception.ArticleServiceException;
import com.training.homework13.task1.service.ArticleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of the ArticleService interface. HTTP requests are executed
 * with an instance of the HttpURLConnection class.
 *
 * @author Alexandr_Terehov
 */
public class ArticleServiceUrlCon implements ArticleService {
    private static final Logger logger = LoggerFactory.getLogger(ArticleServiceUrlCon.class);
    
    /**
     * Method used to provide 'GET' request.
     *
     * @param id
     *            id of the article.
     * @return HTTP response code.
     * @throws Exception
     *             may occur while request execution.
     */
    public String doGetRequest(String id) throws Exception {
        String urlStr = SUB_URL + id;
        URL url = new URL(urlStr);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        if (responseCode >= 200 && responseCode < 300) {
            String response = readResponse(connection);
            logger.info("GET request has been executed with UrlConncection."
                    + " Article ID: " + id + ".");
            return response;
        } else {
            String message = "Unexpected response status: " + responseCode;
            throw new ArticleServiceException(message);
        }
    }
       
    /**
     * Method used to provide 'POST' request.
     *
     * @param id
     *            id of the article.
     * @return HTTP response code.
     * @throws Exception
     *             may occur while request execution.
     */
    public String doPostRequest(String id) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("title", "myTitle");
        params.put("body", "Some body");
        params.put("userId", "3");
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode(String.valueOf(param.getKey()), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        URL url = new URL(SUB_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        connection.setDoOutput(true);
        connection.getOutputStream().write(postDataBytes);
        int responseCode = connection.getResponseCode();
        if (responseCode >= 200 && responseCode < 300) {
            String response = readResponse(connection);
            logger.info("POST request has been executed with UrlConncection."
                    + " Article ID: " + id + ".");
            return response;
        } else {
            String message = "Unexpected response status: " + responseCode;
            throw new ArticleServiceException(message);
        }
    }
        
    /**
     * Method used to read information from the HTTP response.
     *
     * @param connection
     *            instance of the URLConnection class.
     * @return HTTP response in String format.
     * @throws Exception
     *             may occur while request execution.
     */
    private String readResponse(URLConnection connection) throws Exception {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }
}