package com.training.homework13.task1.handler;

import com.training.homework13.task1.entity.Article;
import com.training.homework13.task1.service.ArticleService;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class provides various operations with HTTP requests to remote article web
 * service.
 *
 * @author Alexandr_Terehov
 */
public class ArticleHttpReqHandler {
    private static final Logger logger = LoggerFactory.getLogger(ArticleHttpReqHandler.class);
    ArticleService articleService;
     
    /**
     * Constructor.
     *
     * @param articleService
     *            instance of a class which implements ArticleService interface.
     */
    public ArticleHttpReqHandler(ArticleService articleService) {
        if (articleService == null) {
            throw new NullPointerException("Instance of ArticleService can't be Null.");
        } else {
            this.articleService = articleService;
        }
    }
     
    /**
     * Method used to create an object of the Article class form the result of the
     * 'GET' request (String in JSON format).
     *
     * @param id
     *            id of the article.
     * @throws Exception
     *             may occur while request execution.
     */
    public Article createArticleInstGetReq(String id) throws Exception {
        String response = articleService.doGetRequest(id);
        Article article = new Article(Integer.parseInt(id));
        setArticleFields(article, response);
        return article;
    }
     
    /**
     * Method used to create an object of the Article class form the result of the
     * 'POST' request (String in JSON format).
     *
     * @param id
     *            id of the article.
     * @throws Exception
     *             may occur while request execution.
     */
    public Article createArticleInstPostReq(String id) throws Exception {
        String response = articleService.doPostRequest(id);
        Article article = new Article(Integer.parseInt(id));
        setArticleFields(article, response);
        return article;
    }
     
    /**
     * Method used to print 'GET' request result to the console.
     *
     * @param id
     *            id of the article.
     * @throws Exception
     *             may occur while request execution.
     */
    public void printResponseToGetRequest(String id) throws Exception {
        Article article = createArticleInstGetReq(id);
        System.out.println("Article [" + id + "]:");
        System.out.println(article);
        logger.info("Console output of the response to HTTP GET request");
    }
     
    /**
     * Method used to print 'POST' request result to the console.
     *
     * @param id
     *            id of the article.
     * @throws Exception
     *             may occur while request execution.
     */
    public void printResponseToPostRequest(String id) throws Exception {
        Article article = createArticleInstPostReq(id);
        System.out.println("Article [" + id + "] " + "has been created:");
        System.out.println(article);
        logger.info("Console output of the response to HTTP POST request");
    }
     
    /**
     * Method used to set Article class instance fields according to response String
     * in JSON format.
     *
     * @param article
     *            instance of the Article class.
     * @param response
     *            string in JSON format.
     */
    private void setArticleFields(Article article, String response) {
        JSONObject jsonResponse = new JSONObject(response);
        article.setUserId(jsonResponse.getInt("userId"));
        article.setTitle(jsonResponse.getString("title"));
        article.setBody(jsonResponse.getString("body"));
    }
}