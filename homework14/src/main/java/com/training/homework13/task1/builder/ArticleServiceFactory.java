package com.training.homework13.task1.builder;

import com.training.homework13.task1.service.ArticleService;
import com.training.homework13.task1.service.impl.ArticleServiceHttpClient;
import com.training.homework13.task1.service.impl.ArticleServiceUrlCon;

/**
 * Class used to create an instance of the class which implements ArticleService
 * interface.
 *
 * @author Alexandr_Terehov
 */
public class ArticleServiceFactory {
    public static final int DEFAULT_ARTICLE_SERVICE = 0;
    public static final int URL_CONNECTION = 1;
    public static final int HTTP_CLIENT = 2;
       
    /**
     * Method used to return an instance of the class which implements
     * ArticleService interface.
     *
     * @param argument
     *            type of the returning class.
     * @return instance of the class which implements ArticleService interface.
     */
    public static ArticleService getArticleServiceInstance(int argument) {
        switch (argument) {
            case DEFAULT_ARTICLE_SERVICE:
                return new ArticleServiceUrlCon();         
            case URL_CONNECTION:
                return new ArticleServiceUrlCon();
            case HTTP_CLIENT:
                return new ArticleServiceHttpClient();
            default:
                throw new IllegalArgumentException("Illegal algorithm id");
        }
    }
}
