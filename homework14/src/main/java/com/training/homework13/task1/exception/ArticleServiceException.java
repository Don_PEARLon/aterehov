package com.training.homework13.task1.exception;

/**
 * Class represents a custom exception that may occur during the process of
 * executing HTTP requests with classes that implements ArticleService
 * interface.
 *
 * @author Alexandr_Terehov
 */
public class ArticleServiceException extends Exception {
    private static final long serialVersionUID = 1L;
    
    public ArticleServiceException(String message) {
        super(message);
    }
}