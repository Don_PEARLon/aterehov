package com.training.homework13.task1.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class that provides operations of arguments validation.
 *
 * @author Alexandr_Terehov
 */
public class ArgumentsBase {
     
    /**
     * Method used to check the correctness of input values.
     *
     * @param args
     *            array of arguments to check.
     * @return boolean result of the checking.
     * @throws IllegalArgumentException
     *             if arguments are not correct.
     */
    public static boolean check(String[] args) throws IllegalArgumentException {
        boolean result = true;
        if (args.length < 2) {
            result = false;
            throw new IllegalArgumentException("Illegal arguments quantity");
        }
        if (!(args[0].equalsIgnoreCase("get") || args[0].equalsIgnoreCase("post"))) {
            result = false;
            throw new IllegalArgumentException("Request should be GET or POST");
        }
        if (!checkItemId(args[1])) {
            result = false;
            throw new IllegalArgumentException("Wrong Item ID");
        }
        if (args.length > 2) {
            if (!checkItemId(args[2])) {
                result = false;
                throw new IllegalArgumentException("Illegal algorithm id");
            }
        }
        return result;
    }
       
    /**
     * Method used to check the correctness of the item ID.
     *
     * @param itemId
     *            id of the item.
     * @return boolean result of the checking.
     */
    public static boolean checkItemId(String itemId) {
        Pattern pattern = Pattern.compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(itemId);
        return matcher.matches();
    }
}