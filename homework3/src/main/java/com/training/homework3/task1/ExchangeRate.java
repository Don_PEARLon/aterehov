package com.training.homework3.task1;

/**
 * Enum for money exchange rates that can be used.
 *
 * @author Alexandr_Terehov
 */
public enum ExchangeRate {
    /**
     * United States Dollar.
     */
    USD(0.5f),
    /**
     * Euro.
     */
    EUR(0.43f),
    /**
     * Russian ruble.
     */
    RUB(31.3f),
    /**
     * Belarussian ruble.
     */
    BYN(1);
    private float rate;
                  
    /**
     * Constructor.
     *
     * @param rate
     *            exchange rate.
     */
    ExchangeRate(float rate) {
        this.rate = rate;
    }
                
    /**
     * @return exchange rate.
     */
    public float getRate() {
        return rate;
    }
             
    /**
     * 
     * @param rate
     *            exchange rate to set.
     */
    public void setRate(float rate) {
        this.rate = rate;
    }
}
