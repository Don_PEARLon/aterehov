package com.training.homework3.task1;

import java.math.BigDecimal;

/**
 * Class represents a bank card.
 *
 * @author Alexandr_Terehov
 */
public class Card {
    private String holderName;
    private BigDecimal accountBalance;

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public Card(String holderName) {
        this.holderName = holderName;
        accountBalance = new BigDecimal(0);
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card (type - double)
     */
    public Card(String holderName, double accountBalance) {
        this.holderName = holderName;
        this.accountBalance = new BigDecimal(accountBalance);
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card (type - BigDecimal)
     */
    public Card(String holderName, BigDecimal accountBalance) {
        this.holderName = holderName;
        this.accountBalance = accountBalance;
    }

    /**
     * Method used to add cash amount to card.
     *
     * @param amount
     *            cash amount (type - double).
     */
    public void addFunds(double amount) {
        BigDecimal amountBd = new BigDecimal(amount);
        addFunds(amountBd);
    }
                       
    /**
     * Method used to add cash amount to the card.
     *
     * @param amount
     *            cash amount (type - BigDecimal).
     */
    public void addFunds(BigDecimal amount) {
        accountBalance = accountBalance.add(amount);
        System.out.println("Amount of " + amount + " has been added to your card balance");
    }

    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount (type - double).
     * @return boolean result of withdrawal operation.
     */
    public boolean withdrawFunds(double amount) {
        BigDecimal amountBd = new BigDecimal(amount);
        return (withdrawFunds(amountBd));
    }

    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount (type - BigDecimal).
     * @return boolean result of withdrawal operation.
     */
    public boolean withdrawFunds(BigDecimal amount) {
        if (accountBalance.compareTo(amount) >= 0) {
            accountBalance = accountBalance.subtract(amount);
            System.out.println("Withdrawal of " + amount + " has been processed");
            return true;
        } else {
            System.out.println("Insufficient funds on your card");
            return false;
        }
    }
                          
    /**
     * Method provides printing card cash balance in BYN to the console.
     */
    public void displayCardBalance() {
        System.out.println("Card balance in BYN: " + accountBalance);
    }

    /**
     * Method provides printing card cash balance to the console.
     * 
     * @param exchangeRate
     *            money exchange rate
     */
    public void displayCardBalance(ExchangeRate exchangeRate) {
        System.out.println("Card balance in " + exchangeRate + ": "
                + accountBalance.multiply(new BigDecimal(exchangeRate.getRate())));
    }
                              
    /**
     * @return card holder name.
     */
    public String getHolderName() {
        return holderName;
    }

    /**
     * 
     * @param holderName
     *            card holder name to set.
     */
    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }
               
    /**
     * @return card cash balance.
     */
    public BigDecimal getCardBalance() {
        return accountBalance;
    }

    /**
     * 
     * @param accountBalance
     *            card cash balance to set.
     */
    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }
}
