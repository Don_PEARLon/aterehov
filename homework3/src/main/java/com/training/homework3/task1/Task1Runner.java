package com.training.homework3.task1;

import java.math.BigDecimal;
    
/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task1Runner {
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        Card card1 = new Card("Holder1", 1100);
        card1.addFunds(100);
        card1.withdrawFunds(200);
        card1.withdrawFunds(2000);
        card1.displayCardBalance();
        card1.displayCardBalance(ExchangeRate.USD);
        Card card2 = new Card("Holder2");
        BigDecimal amount = new BigDecimal(1000);
        card2.addFunds(amount);
        card2.displayCardBalance();
        BigDecimal amountWithdrawal = new BigDecimal(2000);
        card2.withdrawFunds(amountWithdrawal);
        card2.displayCardBalance(ExchangeRate.RUB);
    }
}
