package com.training.homework3.task2;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task2Runner {
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        int[] array1 = { 1, 2, 3, 4 };
        double[] array2 = { 0.5, 0.2, 0.4, 0.3, 0.1 };
        double[] array3 = { 1.0, 2.0, 3.0, 4.0 };
        System.out.println(Median.median(array1));
        System.out.println(Median.median(array2));
        System.out.println(Median.median(array3));
    }
}
