package com.training.homework3.task2;

/**
 * Utility class used to calculate the median of the array.
 *
 * @author Alexandr_Terehov
 */
public final class Median {
    /**
     * Method used to calculate the median of the array of integers.
     *
     * @param array
     *            array of integer values.
     * @return median of the array.
     */
    public static float median(int[] array) {
        float result;
        int n = array.length;
        sortArray(array);
        if (n % 2 == 0) {
            result = (float) (array[n / 2] + array[n / 2 - 1]) / 2;
        } else {
            result = array[n / 2];
        }
        return result;
    }

    /**
     * Method used to calculate the median of the array of doubles.
     *
     * @param array
     *            array of double values.
     * @return median of the array.
     */
    public static double median(double[] array) {
        int n = array.length;
        double result;
        sortArray(array);
        if (n % 2 == 0) {
            result = (array[n / 2] + array[n / 2 - 1]) / 2;
        } else {
            result = array[n / 2];
        }
        return result;
    }

    /**
     * Method used sort the array of integers.
     *
     * @param array
     *            array of integer values.
     */
    private static void sortArray(int[] array) {
        int n = array.length;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (array[i] > array[j]) {
                    exchangeElement(array, i, j);
                }
            }
        }
    }

    /**
     * Method used sort the array of doubles.
     *
     * @param array
     *            array of double values.
     */
    private static void sortArray(double[] array) {
        int n = array.length;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (array[i] > array[j]) {
                    exchangeElement(array, i, j);
                }
            }
        }
    }
    
    /**
     * Method for exchanging elements in the array of integers.
     *
     * @param array
     *            array of integer values.
     * @param i
     *            index of the exchanging element.
     * @param j
     *            index of the exchanging element.
     */
    private static void exchangeElement(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    
    /**
     * Method for exchanging elements in the array of doubles.
     *
     * @param array
     *            array of double values.
     * @param i
     *            index of the exchanging element.
     * @param j
     *            index of the exchanging element.
     */
    private static void exchangeElement(double[] array, int i, int j) {
        double temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
