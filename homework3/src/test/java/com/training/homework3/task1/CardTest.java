package com.training.homework3.task1;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for Card class.
 *
 * @author Alexandr_Terehov
 */
public class CardTest {
	private static Card card;

	@Before
	public void initCard() {
		card = new Card("Holder1", 1000);
	}

	@Test
	public void testCardAddDouble() {
		card.addFunds(150.5);
		assertEquals(1150.5, card.getCardBalance().doubleValue(), 0.0);
	}

	@Test
	public void testCardAddBigDecimal() {
		BigDecimal amount = new BigDecimal(230.25);
		BigDecimal expectedCardBalance = new BigDecimal(1230.25);
		card.addFunds(amount);
		Boolean condition = (card.getCardBalance().compareTo(expectedCardBalance) == 0);
		assertTrue(condition);
	}

	@Test
	public void testCardWithdrawDouble() {
		card.withdrawFunds(50.5);
		assertEquals(949.5, card.getCardBalance().doubleValue(), 0.0);
	}

	@Test
	public void testCardWithDrawBigDecimal() {
		BigDecimal amount = new BigDecimal(130.25);
		BigDecimal expectedCardBalance = new BigDecimal(869.75);
		card.withdrawFunds(amount);
		Boolean condition = (card.getCardBalance().compareTo(expectedCardBalance) == 0);
		assertTrue(condition);
	}

	@Test
	public void testCardWithdrawNegative() {
		boolean condition = card.withdrawFunds(1100);
		assertFalse(condition);
	}

	@Test
	public void testExchangeRate() {
		double result = card.getCardBalance().doubleValue() * ExchangeRate.EUR.getRate();
		assertEquals(430, result, 0.0001);
	}
}
