package com.training.homework4.task1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.training.homework4.task1.card.Card;
import com.training.homework4.task1.card.CreditCard;
import com.training.homework4.task1.card.DebitCard;

/**
 * Test suite for Atm class.
 *
 * @author Alexandr_Terehov
 */
public class AtmTest {
	private static Atm atm;
	private static Card creditCard;
	private static Card debitCard;

	@Before
	public void initObjects() {
		creditCard = new CreditCard("Holder1", 1000);
		debitCard = new DebitCard("Holder1", 1100);
		atm = new Atm(creditCard);
	}

	@Test
	public void testAtmCreditAddFunds() {
		atm.addFunds(250.254);
		assertEquals(1250.254, atm.getAtmCardBalance(), 0.0);
	}

	@Test
	public void testAtmDebitAddFunds() {
		atm.setCard(debitCard);
		atm.addFunds(350.2);
		assertEquals(1450.2, atm.getAtmCardBalance(), 0.0);
	}

	@Test
	public void testCreditCardWithdraw() {
		atm.withdrawFunds(70.25);
		assertEquals(929.75, atm.getAtmCardBalance(), 0.0);
	}

	@Test
	public void testCreditCardWithdrawNegative() {
		atm.withdrawFunds(1200.25);
		assertEquals(-200.25, atm.getAtmCardBalance(), 0.0);
	}

	@Test
	public void testDebitCardWithdraw() {
		atm.setCard(debitCard);
		atm.withdrawFunds(35.25);
		assertEquals(1064.75, atm.getAtmCardBalance(), 0.0);
	}

	@Test
	public void testDebitCardWithdrawNegative() {
		atm.setCard(debitCard);
		boolean condition = atm.withdrawFunds(1200);
		assertFalse(condition);
	}
}
