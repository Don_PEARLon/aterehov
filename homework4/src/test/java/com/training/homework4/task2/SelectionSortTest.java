package com.training.homework4.task2;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for SelecionSort class.
 *
 * @author Alexandr_Terehov
 */
public class SelectionSortTest {
	private static SortingContext sortingContext;
	private static Sorter selectionSort;
	@Before
	public void initObjects() {
		selectionSort = new SelectionSort();
		sortingContext = new SortingContext(selectionSort);
	}
	@Test
	public void testSelectionSort() {
		 int[] array = { 1, 7, -23, 43, 125, 93, 5, 8 };
		 boolean conditionPre = SortingBase.isSorted(array);
		 sortingContext.execute(array);
		 boolean conditionPost = SortingBase.isSorted(array);
		 boolean condition = false;
		 if ((conditionPre==false) & (conditionPost==true)) {
			 condition = true;
		 }
		 assertTrue(condition);
	}
}
