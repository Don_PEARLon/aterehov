package com.training.homework4.task2;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for InsertionSort class.
 *
 * @author Alexandr_Terehov
 */
public class InsertionSortTest {
	private static SortingContext sortingContext;
	private static Sorter insertionSort;
	@Before
	public void initObjects() {
		insertionSort = new InsertionSort();
		sortingContext = new SortingContext(insertionSort);
	}
	@Test
	public void testSelectionSort() {
		 int[] array = { 2, -7, 14, 910, 15, 124, 67, 8, 32, -3 };
		 boolean conditionPre = SortingBase.isSorted(array);
		 sortingContext.execute(array);
		 boolean conditionPost = SortingBase.isSorted(array);
		 boolean condition = false;
		 if ((conditionPre==false) & (conditionPost==true)) {
			 condition = true;
		 }
		 assertTrue(condition);
	}
}
