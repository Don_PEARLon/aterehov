package com.training.homework4.task1;

import com.training.homework4.task1.card.Card;

/**
 * Class represents an ATM.
 *
 * @author Alexandr_Terehov
 */
public class Atm {
    private Card card;

    /**
     * Constructor.
     *
     * @param card
     *            instance of the Card class.
     */
    public Atm(Card card) {
        this.card = card;
    }
             
    /**
     * @return card instance of the Card class.
     */
    public Card getCard() {
        return card;
    }

    /**
     * 
     * @param card
     *            instance of the Card class to set.
     */
    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * Method used to add cash amount to card.
     *
     * @param amount
     *            cash amount.
     */
    public void addFunds(double amount) {
        card.addFunds(amount);
    }

    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount.
     * @return boolean result of withdrawal operation.
     */
    public boolean withdrawFunds(double amount) {
        return card.withdrawFunds(amount);
    }

    /**
     * Method used to show card cash balance.
     *
     * @return card cash balance.
     */
    public double getAtmCardBalance() {
        return card.getCardBalance();
    }

    /**
     * Method provides printing card cash balance to the console.
     */
    public void displayAtmCardBalance() {
        card.displayCardBalance();
    }
}
