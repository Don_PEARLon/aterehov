package com.training.homework4.task1;

import com.training.homework4.task1.card.Card;
import com.training.homework4.task1.card.CreditCard;
import com.training.homework4.task1.card.DebitCard;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task1Runner {

    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        Card card1 = new DebitCard("Holder1", 1000);
        Card card2 = new CreditCard("Holder2", 1000);
        Atm atm = new Atm(card1);
        atm.withdrawFunds(2000);
        atm.setCard(card2);
        atm.withdrawFunds(2000);
        atm.displayAtmCardBalance();
    }
}
