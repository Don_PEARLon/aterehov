package com.training.homework4.task1.card;

/**
 * Class represents a bank card.
 *
 * @author Alexandr_Terehov
 */
public abstract class Card {
    protected String holderName;
    protected double accountBalance;

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public Card(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card 
     */
    public Card(String holderName, double accountBalance) {
        this.holderName = holderName;
        this.accountBalance = accountBalance;
    }

    /**
     * Method used to add cash amount to card.
     *
     * @param amount
     *            cash amount 
     */
    public void addFunds(double amount) {
        this.accountBalance += amount;
    }
    /**
     * Abstract method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount
     * @return boolean result of withdrawal operation.
     */

    public abstract boolean withdrawFunds(double amount);
                          
    /**
     * Method provides printing card cash balance to the console.
     */
    public void displayCardBalance() {
        System.out.println("Card balance: " + accountBalance);
    }
                               
    /**
     * @return card holder name.
     */
    public String getHolderName() {
        return holderName;
    }

    /**
     * 
     * @param holderName
     *            card holder name to set.
     */
    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }
               
    /**
     * @return card cash balance.
     */
    public double getCardBalance() {
        return accountBalance;
    }

    /**
     * 
     * @param accountBalance
     *            card cash balance to set.
     */
    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }
}
