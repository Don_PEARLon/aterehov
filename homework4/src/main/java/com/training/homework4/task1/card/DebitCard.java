package com.training.homework4.task1.card;

/**
 * Class represents a debit card.
 *
 * @author Alexandr_Terehov
 */
public class DebitCard extends Card {
    
    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public DebitCard(String holderName) {
       super(holderName);
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card 
     */
    public DebitCard(String holderName, double accountBalance) {
        super(holderName, accountBalance);
    }

    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount.
     * @return boolean result of withdrawal operation.
     */
    @Override
    public boolean withdrawFunds(double amount) {
        if (accountBalance >= amount) {
            accountBalance -= amount;
            System.out.println("Withdrawal of " + amount + " has been processed");
            return true;
        } else {
            System.out.println("Insufficient funds on your card");
            return false;
        }
    }
}
