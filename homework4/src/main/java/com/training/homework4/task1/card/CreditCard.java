package com.training.homework4.task1.card;

/**
 * Class represents a credit card.
 *
 * @author Alexandr_Terehov
 */
public class CreditCard extends Card {
    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     */
    public CreditCard(String holderName) {
       super(holderName);
    }

    /**
     * Constructor.
     *
     * @param holderName
     *            name of the card holder.
     * @param accountBalance
     *            cash balance on the bank card
     */
    public CreditCard(String holderName, double accountBalance) {
        super(holderName, accountBalance);
    }
    
    /**
     * Method used to withdraw funds from the card.
     *
     * @param amount
     *            cash amount.
     * @return boolean result of withdrawal operation.
     */
    @Override
    public boolean withdrawFunds(double amount) {
        accountBalance -= amount;
        System.out.println("Withdrawal of " + amount + " has been processed");
        return true;
    }
}