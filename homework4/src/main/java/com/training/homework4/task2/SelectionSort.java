package com.training.homework4.task2;

/**
 * Class used for sorting arrays with Selection method.
 *
 * @author Alexandr_Terehov
 */
public class SelectionSort implements Sorter {
    /**
     * Method used to sort an array of integers with Selection type of sorting.
     *
     * @param array
     *            array of integers.
     */
    public void sort(int[] array) {
        int max;
        for (int j = array.length - 1; j >= 1; j--) {
            max = array[j];
            for (int i = 0; i <= j; i++) {
                if (array[i] > max) {
                    max = array[i];
                    SortingBase.exchangeElement(array, i, j);
                }
            }
        }
        System.out.println("Array has been sorted using Selection sorting");
    }
}
