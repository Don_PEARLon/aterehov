package com.training.homework4.task2;

/**
 * Class used for sorting arrays with Insertion method.
 *
 * @author Alexandr_Terehov
 */
public class InsertionSort implements Sorter {
    /**
     * Method used to sort an array of integers with Insertion type of sorting.
     *
     * @param array
     *            array of integers.
     */
    public void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0; j--) {
                if (array[j] < array[j - 1]) {
                    SortingBase.exchangeElement(array, j, j - 1);
                }
            }
        }
        System.out.println("Array has been sorted using Insertion sorting");
    }
}
