package com.training.homework4.task2;

/**
 * Interface used for sorting arrays.
 *
 * @author Alexandr_Terehov
 */
public interface Sorter {
    /**
     * Method used to sort an array of integers.
     *
     * @param array
     *            array of integers.
     */
    void sort(int[] array);
}
