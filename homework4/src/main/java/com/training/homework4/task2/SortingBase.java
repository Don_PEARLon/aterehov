package com.training.homework4.task2;

/**
 * Utility class that provides various array operations.
 *
 * @author Alexandr_Terehov
 */
public class SortingBase {
    /**
     * Method used to print the array of integers to the console.
     *
     * @param array
     *            array of integers.
     */
    public static void printArray(int[] array) {
        System.out.print("{");
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println(array[array.length - 1] + "}");
    }
       
    /**
     * Method for exchanging elements in the array of integers.
     *
     * @param array
     *            array of integer values.
     * @param i
     *            index of the exchanging element.
     * @param j
     *            index of the exchanging element.
     */
    public static void exchangeElement(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    /**
     * Method check array sorting state.
     *
     * @param array
     *            array of integers.
     * @return boolean result of the array sorting state.
     */
    public static boolean isSorted(int[] array) {
        for (int i = 1; i < array.length; i++) {
            if (array[i] < array[i - 1]) {
                return false;
            }
        }
        return true;
    }
}
