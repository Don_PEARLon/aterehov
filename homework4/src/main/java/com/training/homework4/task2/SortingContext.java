package com.training.homework4.task2;

/**
 * Class used for sorting arrays.
 *
 * @author Alexandr_Terehov
 */
public class SortingContext {
    private Sorter sorter;

    /**
     * Constructor.
     *
     * @param sorter
     *            instance of the class which implements the Sorter interface.
     */
    public SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * Method used to sort an array of integers.
     *
     * @param array
     *            array of integers.
     */
    public void execute(int[] array) {
        sorter.sort(array);
    }

    /**
     * @return sorter instance of the class which implements the Sorter interface.
     */
    public Sorter getSorter() {
        return sorter;
    }

    /**
     * 
     * @param sorter
     *            instance of the class which implements the Sorter interface to
     *            set.
     */
    public void setSorter(Sorter sorter) {
        this.sorter = sorter;
    }
}
