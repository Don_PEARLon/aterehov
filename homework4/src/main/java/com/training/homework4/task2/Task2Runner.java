package com.training.homework4.task2;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task2Runner {
           
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        int[] array1 = { 1, 7, -23, 43, 125, 93, 5, 8 };
        SortingContext sortingContext = new SortingContext(new SelectionSort());
        sortingContext.execute(array1);
        SortingBase.printArray(array1);
        System.out.println(SortingBase.isSorted(array1));
        int[] array2 = { 2, -7, 14, 910, 15, 124, 67, 8, 32, -3 };
        sortingContext.setSorter(new InsertionSort());
        sortingContext.execute(array2);
        SortingBase.printArray(array2);
        System.out.println(SortingBase.isSorted(array2));
    }
}
