package com.training.homework13.task1.service.impl;

import com.training.homework13.task1.exception.ArticleServiceException;
import com.training.homework13.task1.service.ArticleService;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of the ArticleService interface. HTTP requests are executed
 * with an instance of the HttpURLConnection class.
 *
 * @author Alexandr_Terehov
 */
public class ArticleServiceUrlCon implements ArticleService {
     
    /**
     * Method used to provide 'GET' request.
     *
     * @param id
     *            id of the article.
     * @return HTTP response code.
     * @throws Exception
     *             may occur while request execution.
     */
    public String doGetRequest(String id) throws Exception {
        String urlStr = SUB_URL + id;
        URL url = new URL(urlStr);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        if (responseCode >= 200 && responseCode < 300) {
            String response = readResponse(connection);
            return response;
        } else {
            String message = "Unexpected response status: " + responseCode;
            throw new ArticleServiceException(message);
        }
    }
       
    /**
     * Method used to provide 'POST' request.
     *
     * @param id
     *            id of the article.
     * @return HTTP response code.
     * @throws Exception
     *             may occur while request execution.
     */
    public String doPostRequest(String id) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("title", "myTitle");
        params.put("body", "Some body");
        params.put("userId", "3");
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode(String.valueOf(param.getKey()), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        URL url = new URL(SUB_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        connection.setDoOutput(true);
        connection.getOutputStream().write(postDataBytes);
        int responseCode = connection.getResponseCode();
        if (responseCode >= 200 && responseCode < 300) {
            String response = readResponse(connection);
            return response;
        } else {
            String message = "Unexpected response status: " + responseCode;
            throw new ArticleServiceException(message);
        }
    }
    
    /**
     * The method used to print 'GET' request result to the console.
     *
     * @param id
     *          id of the article.
     * @throws Exception
     *             may occur while request execution.
     */
    public void printGetRequest(String id) throws Exception {
        String response = doGetRequest(id);
        System.out.println("Article [" + id + "]:");
        printJsonResponse(response);
    }
    
    /**
     * The method used to print 'POST' request result to the console.
     *
     * @param id
     *          id of the article.
     * @throws Exception
     *             may occur while request execution.
     */
    public void printPostRequest(String id) throws Exception {
        String response = doPostRequest(id);
        System.out.println("Article [" + id + "] " + "has been created:");
        printJsonResponse(response);
    }
    
    /**
     * Method used to read information from the HTTP response.
     *
     * @param connection
     *            instance of the URLConnection class.
     * @throws Exception
     *             may occur while request execution.
     */
    private String readResponse(URLConnection connection) throws Exception {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }
       
    /**
     * Method used to output to the console information from the JSON format
     * response.
     *
     * @param response
     *            string in JSON format.
     */
    private void printJsonResponse(String response) {
        JSONObject myJsonResponse = new JSONObject(response);
        System.out.println("User [" + myJsonResponse.getInt("userId") + "]");
        System.out.println("Title [" + myJsonResponse.getString("title") + "]");
        System.out.println("Message [" + myJsonResponse.getString("body") + "]");
    }
}
