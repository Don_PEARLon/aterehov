package com.training.homework13.task1;

import com.training.homework13.task1.builder.ArticleServiceFactory;
import com.training.homework13.task1.service.ArticleService;
import com.training.homework13.task1.util.ArgumentsBase;

/**
 * Main application class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {
        
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        try {
            ArgumentsBase.check(args);
            ArticleService articleService;
            if (args.length < 3) {
                articleService = ArticleServiceFactory.getArticleServiceInstance(0);
            } else {
                int arg = Integer.parseInt(args[2]);
                articleService = ArticleServiceFactory.getArticleServiceInstance(arg);
            }
            if (args[0].toLowerCase().equals("get")) {
                articleService.printGetRequest(args[1]);
            } else if (args[0].toLowerCase().equals("post")) {
                articleService.printPostRequest(args[1]);
            }
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
        }
    }
}
