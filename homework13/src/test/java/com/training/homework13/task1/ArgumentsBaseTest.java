package com.training.homework13.task1;

import com.training.homework13.task1.util.ArgumentsBase;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test suite for ArgumentsBase class.
 *
 * @author Alexandr_Terehov
 */
public class ArgumentsBaseTest {
	@Test
	public void testCheck() {
	    String[] arguments = {"Get", "1", "1"};
	    boolean condition = ArgumentsBase.check(arguments);
	    assertTrue(condition);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testCheckNegative() {
	    String[] arguments = {"Gets", "1", "1"};
	    ArgumentsBase.check(arguments);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testCheckParamQuantityNegative() {
	    String[] arguments = {"Gets"};
	    ArgumentsBase.check(arguments);
	}
	
	@Test
    public void testCheckItemId() {
    	boolean condition = ArgumentsBase.checkItemId("12");
    	assertTrue(condition);
    }
    
    @Test
    public void testCheckItemIdNegative() {
    	boolean condition = ArgumentsBase.checkItemId("12s");
    	assertFalse(condition);
    }
}
