package com.training.homework13.task1;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.training.homework13.task1.builder.ArticleServiceFactory;
import com.training.homework13.task1.service.impl.ArticleServiceHttpClient;
import com.training.homework13.task1.service.impl.ArticleServiceUrlCon;

/**
 * Test suite for ArticleServiceFactory class.
 *
 * @author Alexandr_Terehov
 */
public class ArticleServiceFactoryTest {
    private static ArticleServiceFactory articleServiceFactory; 
    
    @Before 
    public void initFactory() {
    	articleServiceFactory = new ArticleServiceFactory();
    }
    
    @Test
    public void testArticleServiceFactoryInstDefService() {
    	boolean condition = articleServiceFactory.getArticleServiceInstance(0) 
    			instanceof ArticleServiceUrlCon;
    	assertTrue(condition);
    }
    
    @Test
    public void testArticleServiceFactoryInstUrlCon() {
    	boolean condition = articleServiceFactory.getArticleServiceInstance(1) 
    			instanceof ArticleServiceUrlCon;
    	assertTrue(condition);
    }
    
    @Test
    public void testArticleServiceFactoryInstHttpClient() {
    	boolean condition = articleServiceFactory.getArticleServiceInstance(2) 
    			instanceof ArticleServiceHttpClient;
    	assertTrue(condition);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testArticleServiceFactoryNegative() {
    	articleServiceFactory.getArticleServiceInstance(3); 
    }
}
