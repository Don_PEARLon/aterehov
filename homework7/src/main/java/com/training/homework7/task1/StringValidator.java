package com.training.homework7.task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class used for representing validator of string type input data.
 *
 * @author Alexandr_Terehov
 */
public class StringValidator implements Validator<String> {
                             
    /**
     * Method used to validate string type input data.
     *
     * @param value
     *            value to validate.
     * @throws ValidationFailedException
     *             occurs when validation failed.
     */
    @Override
    public void validate(String value) throws ValidationFailedException {
        if (checkFirstLetterInString(value)) {
            System.out.println("String value has been validated");
        } else {
            throw new ValidationFailedException("Validation failed. "
                    + "Sentence must start with capital letter.");
        }
    }

    /**
     * Method used to check the case of the first letter in a sentence.
     *
     * @param value
     *            string value to check.
     * @return boolean result of checking.
     */
    private boolean checkFirstLetterInString(String value) {
        Pattern pattern = Pattern.compile("^[A-Z].*");
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }
}