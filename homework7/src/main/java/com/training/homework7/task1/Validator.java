package com.training.homework7.task1;

/**
 * Interface used for representing validator of input data.
 *
 * @param <T>
 *            This describes my type parameter.
 * @author Alexandr_Terehov
 */
public interface Validator<T> {
    /**
     * Method used to validate input data.
     *
     * @param value
     *            value to validate.
     * @throws ValidationFailedException
     *            occurs when validation failed.
     */
    public void validate(T value) throws ValidationFailedException;
}
