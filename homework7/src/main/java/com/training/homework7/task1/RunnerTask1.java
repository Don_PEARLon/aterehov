package com.training.homework7.task1;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {
                 
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        try {
            ValidationSystem.validate("London is the capital of Great Britain.");
            ValidationSystem.validate(10);
            ValidationSystem.validate(11);
        } catch (ValidationFailedException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
