package com.training.homework7.task1;

/**
 * Class used for representing validator of integer type input data.
 *
 * @author Alexandr_Terehov
 */
public class IntegerValidator implements Validator<Integer> {
    /**
     * Method used to validate integer type input data.
     *
     * @param value
     *            value to validate.
     * @throws ValidationFailedException
     *             occurs when validation failed.
     */
    @Override
    public void validate(Integer value) throws ValidationFailedException {
        if ((value >= 1) && (value <= 10)) {
            System.out.println("Integer value has been validated");
        } else {
            throw new ValidationFailedException("Validation failed. "
                    + "Integer value must be between 1 and 10.");
        }
    }
}
