package com.training.homework7.task1;

/**
 * Class used to represent a custom exception that can occur during the input
 * validation process.
 *
 * @author Alexandr_Terehov
 */
public class ValidationFailedException extends Exception {
    public ValidationFailedException(String message) {
        super(message);
    }
    
    private static final long serialVersionUID = 1L;
}
