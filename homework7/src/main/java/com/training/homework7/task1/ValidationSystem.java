package com.training.homework7.task1;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used for validation of input data.
 *
 * @author Alexandr_Terehov
 */
public class ValidationSystem {
    private static Map<Class<?>, Validator<?>> validatorMap
                = new HashMap<Class<?>, Validator<?>>() {
                    private static final long serialVersionUID = 1L;
                    {
                        this.put(Integer.class, new IntegerValidator());
                        this.put(String.class, new StringValidator());
                    }
                };
   
    /**
     * Method used to validate input data.
     *
     * @param value
     *            value to validate.
     * @param <T>
     *            This describes my type parameter.
     * @throws ValidationFailedException
     *            occurs when validation failed.
     */
    public static <T> void validate(T value) throws ValidationFailedException {
        if (value == null) {
            throw new ValidationFailedException("Validation failed. "
                    + "Input value can't be null.");
        }
        Validator<T> validator = (Validator<T>)validatorMap.get(value.getClass());
        validator.validate(value);
    }
}
