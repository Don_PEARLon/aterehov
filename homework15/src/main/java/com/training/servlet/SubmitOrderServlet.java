package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet used for confirmation of the order, provides calculation of the total
 * order's price.
 */
public class SubmitOrderServlet extends HttpServlet {
    private static final long serialVersionUID = -1882926836615346135L;
     
    /**
     * Map that contains information about items of the online-shop. Name of the
     * item is a key and price is a value.
     */
    private Map<String, Double> shopItemMap;
    
    private String userName;
    private String[] orderArr;
    private String orderStr;
    private double totalPrice;
    private int itemNumber;
    
    @Override
    public void init() {
        shopItemMap = new HashMap<>();
        shopItemMap.put("Mobile Phone", 10.0);
        shopItemMap.put("Book", 5.5);
        shopItemMap.put("Laptop", 310.15);
        shopItemMap.put("TV", 250.0);
        shopItemMap.put("Refrigerator", 290.0);
        orderStr = "";
        itemNumber = 1;
    }
     
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     */ 
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        userName = request.getParameter("userName");
        orderArr = request.getParameterValues("shopItemName");
        if (orderArr != null) {
            for (String item : orderArr) {
                for (Map.Entry<String, Double> entry : shopItemMap.entrySet()) {
                    if (entry.getKey().equals(item)) {
                        orderStr += "<h4 align=center>" + itemNumber + ") " + entry.getKey()
                            + "   " + entry.getValue() + "$" + "</h4>";
                        totalPrice += entry.getValue();
                        itemNumber++;
                    }
                }
            }
        }
        PrintWriter out = response.getWriter();
        final String body = "<html>"
                          + "<body>"
                          + "<h1 align=center>Dear "
                          + userName + ", your order!</h1>"
                          + orderStr
                          + "<br>"
                          + "<h2 align=center>"
                          + "Total: "
                          + totalPrice
                          + "</h2>"
                          + "</body>"   
                          + "</html>";
        out.println(body);
    }
}
