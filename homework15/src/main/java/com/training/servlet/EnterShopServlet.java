package com.training.servlet;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet used during process of shop item selection for the order. 
 */
public class EnterShopServlet extends HttpServlet {
    private static final long serialVersionUID = -6881786289314956801L;
    
    private static final String HEAD_STR = "<head>"
            + "<script src='resources/js/jquery.js'></script>"
            + "<link rel='stylesheet' href='resources/css/bootstrap.min.css'>"
            + "<script type='text/javascript' src='resources/js/bootstrap.min.js'></script>"
            + "<script src='resources/js/bootstrap-multiselect.js'></script>"
            + "<link rel='stylesheet' href='resources/css/bootstrap-multiselect.css'>"
            + "</head>";
    private static final String SCRIPT_STR = "<script type='text/javascript'>"
            + "$(document).ready(function() {"
            + "$('#example-getting-started').multiselect();"
            + "});"
            + "</script>";
    private static final String MULTIPLE_SELECT_STR = 
              "<select input id='example-getting-started' type='text'"
            + " name='shopItemName' multiple='multiple'>"
            + "<option value='Mobile Phone'>Mobile Phone 10$</option>"
            + "<option value='Book'>Book 5.5$</option>"
            + "<option value='Laptop'>Laptop 310.15$</option>"
            + "<option value='TV'>TV 250$</option>"
            + "<option value='Refrigerator'>Refrigerator 290$</option>"
            + "</select>";
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException
     *             exception a servlet can throw when it encounters difficulty.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        final String userName = request.getParameter("userName");
        if (checkName(userName)) {
            final String body = "<html>"
                              + HEAD_STR
                              + "<body>"
                              + "<form align=center action='order' method='post'>"
                              + "<h1 align=center >Hello "
                              + userName + "!</h1>"
                              + "<input hidden type='text' name='userName' value='"
                              + userName + "'>"
                              + "<h2 align=center>Make your order</h2>"
                              + MULTIPLE_SELECT_STR
                              + SCRIPT_STR
                              + "<input type='submit' value='Submit'>"
                              + "</form>"
                              + "</body>"   
                              + "</html>";
            response.getWriter().println(body);
        } else {
            final String errorBody = "<html>"
                                  + "<head>"
                                  + "<style>"
                                  + ".layer1 {"
                                  + "margin-left: 10%;"
                                  + "}"
                                  + "</style>"
                                  + "</head>"
                                  + "<body>"
                                  + "<div class='layer1'>"
                                  + "<h1>Oops!</h1><br>"
                                  + "<h4>Error occured while authentication process</h4>"
                                  + "<h4>Check if user name is entered correctly</h4>"
                                  + "<br>"
                                  + "<h4><a href='/online-shop'> Start page</a></h4>"
                                  + "</div>"
                                  + "</body>"   
                                  + "</html>";
            response.getWriter().println(errorBody);
        }
    }
    
    /**
     * Method used to check the correctness of user's name input.
     *
     * @param name
     *            name of the user.
     * @return boolean result of the checking.
     */
    private boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^(?!\\s*$).+");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }
}