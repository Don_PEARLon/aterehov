package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet starts an applications, provides logging in to online-shop.
 */
public class StartShopServlet extends HttpServlet {
    
    private static final long serialVersionUID = -4281588832138964034L;
    
    /**
     * Handles {@link HttpServlet} GET Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     */
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        PrintWriter out = response.getWriter();
        final String body = "<html>"
                          + "<body>"
                          + "<h2 align=center>Welcome to Online Shop</h2>"
                          + "<form align=center action='enter' method='post'>"
                          + "<input align=center type='text' name='userName'"
                          + " placeholder='Enter your name'><br>"
                          + "<br><input align=center type='submit' value='Enter'>"
                          + "</form>"
                          + "</body>"     
                          + "</html>";
        out.println(body);
    }
}
