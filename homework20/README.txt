To run this application go in cmd to the application root folder and print command:
"mvn clean install tomcat7:run-war-only" or "mvn tomcat7:run".
Application will be available by URL "http://localhost:8080/online-shop". 
There are three userpassword combinations initially avaliable in database:
user1 - pass1, user2 - pass2, user3 - pass3. So use one of them to log in to the app.
Now there is a new option available - you have links to previous orders of the user,
clicking on those links you'll get detailed information about order of the user.
After confirmation of the new order and clicking on 'back' button you'll be returned to the 'orders' page
where link to the information about new order will be available. 