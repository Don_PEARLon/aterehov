<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<style>
.layer1 {
	margin-left: 10%;
}
</style>
</head>
<body>
	<div class='layer1'>
		<h1>Oops!</h1>
		<br>
		<h4>Authentification failed.</h4>
		<h4>${SPRING_SECURITY_LAST_EXCEPTION.message} </h4>
		<br>
		<h4>
			<a href='/online-shop'> Start page</a>
		</h4>
	</div>
</body>
</html>