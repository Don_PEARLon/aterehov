<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
    <div align=center>
	<h2>Order info!</h2>
	<h4>Order ID:  ${orderId}</h4>
	<h4>Ordered By: ${user.getUsername()}</h4><br>
	<h4>Ordered Goods:</h4>
	<% int itemNumber = 1; %>
				<c:forEach items="${goodList}" var="good">
					<h4><%=itemNumber %>) ${good.getTitle()} ${good.getPrice()}$</h4>
					<% itemNumber++; %>
				</c:forEach>
	<br><h4>Total Price:  ${orderPrice}$</h4>
	<form action="/online-shop">
        <input type="submit" value="Back" />
     </form>
	</div>
</body>
</html>

