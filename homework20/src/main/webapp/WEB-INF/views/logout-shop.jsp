<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<html>
<body>
	<h2 align=center>${username}, are you sure you want to
		logout?</h2>
	<div align=center>
		<form action='logout' method='post'>
			<br><br> <input type='submit' value='Logout'>
		</form>
		<br>
		<form action="/online-shop">
        <input type="submit" value="Back" />
     </form>
	</div>
</body>
</html>
