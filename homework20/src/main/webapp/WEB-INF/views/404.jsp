<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true"%>
<html>
<head>
<style>
.layer1 {
	margin-left: 10%;
}
</style>
</head>
<body>
	<div class='layer1'>
		<h1>Error :(</h1>
		<br>
		<h4>Error code: 404</h4>
		<h4>The requested resource was not found</h4>
		<br>
		<h4>
			<a href='/online-shop'> Start page</a>
		</h4>
	</div>
</body>
</html>
