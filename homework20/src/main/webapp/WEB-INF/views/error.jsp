<%@ page contentType="text/html;charset=UTF-8" language="java"
	isErrorPage="true"%>
<html>
<head>
<style>
.layer1 {
	margin-left: 10%;
}
</style>
</head>
<body>
	<div class='layer1'>
		<h1>Oops! Error occurred!</h1>
		<br>
		<h4>Error message: ${errorMessage}</h4>
		<br>
		<h4>
			<a href='/online-shop'> Start page</a>
		</h4>
	</div>
</body>
</html>

