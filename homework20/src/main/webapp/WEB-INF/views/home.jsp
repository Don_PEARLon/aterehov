<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<style>
.layer1 {
	margin-left: 10%;
}
.layer2 {
	margin-left: 60%;
}
</style>
</head>
<body>
    <div class='layer2'>
		<form action='logout-confirm' method='get'>
			<input type='submit' value='Logout'>
		</form>
	</div>
	<div class='layer1'>
		<h1>Welcome ...</h1>
		<h1>Hi ${username}</h1>
		<h1>Id: ${userId}</h1>
		<a href="logout">logout</a>
	</div>
	
	<div class='layer1'>
		<form action='logout' method='post'>
			<br><br> <input type='submit' value='Logout'>
		</form>
	</div>
	
</body>
</html>