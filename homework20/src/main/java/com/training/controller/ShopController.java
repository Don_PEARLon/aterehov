package com.training.controller;

import com.training.dto.GoodDto;
import com.training.dto.OrderDto;
import com.training.dto.OrderGoodInfoDto;
import com.training.security.ShopUserPrincipal;
import com.training.service.ChosenGoodsService;
import com.training.service.GoodService;
import com.training.service.OrderGoodInfoService;
import com.training.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Application main controller.  
 *
 * @author Alexandr_Terehov
 */
@Controller
public class ShopController {

    @Autowired
    private GoodService goodService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ChosenGoodsService chosenGoodsService;
    @Autowired
    private OrderGoodInfoService orderGoodInfoService;
    
    /**
     * @return {@link ModelAndView} related to the '/login' URL.
     */
    @GetMapping("/login")
    public ModelAndView loginToShop() {
        chosenGoodsService.clearGoodsList();
        final ModelAndView content = new ModelAndView("login-shop");
        return content;
    }
    
    /**
     * 
     * @param principal
     *            {@link Principal}.
     * @return {@link ModelAndView} related to the '/logout-confirm' URL.
     */
    @GetMapping("/logout-confirm")
    public ModelAndView logOutFromShop(Principal principal) {
        String username = principal.getName();
        final ModelAndView content = new ModelAndView("logout-shop");
        content.addObject("username", username);
        return content;
    }
    
    /**
     * @return {@link ModelAndView} related to the '/auth-failed' URL.
     */
    @GetMapping("/auth-failed")
    public ModelAndView authFailed() {
        final ModelAndView content = new ModelAndView("auth-failed");
        return content;
    }
    
    /**
     * 
     * @param request
     *            {@link HttpServletRequest}.
     * @return {@link ModelAndView} related to the '/online-shop' URL.
     */
    @GetMapping("/")
    public ModelAndView onlineShopGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("chosenGoodsService") == null) {
            session.setAttribute("chosenGoodsService", chosenGoodsService);
        }
        final ModelAndView content = new ModelAndView("online-shop");
        final ShopUserPrincipal user = (ShopUserPrincipal)SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
        content.addObject("user", user);
        chosenGoodsService = (ChosenGoodsService) session.getAttribute("chosenGoodsService");
        content.addObject("shopGoodList", goodService.getGoodsList());
        content.addObject("usersOrderList", orderService.getOrderListByUserId(user.getUserId()));
        content.addObject("chosenGoodsList", chosenGoodsService.getGoodsList());
        return content;
    }
    
    /**
     * 
     * @param request
     *            {@link HttpServletRequest}.
     * @return {@link ModelAndView} related to the '/enter' URL using POST method.
     */
    @PostMapping("/enter")
    public ModelAndView onlineShopPost(HttpServletRequest request) {
        HttpSession session = request.getSession();
        final ShopUserPrincipal user = (ShopUserPrincipal)SecurityContextHolder
                    .getContext().getAuthentication().getPrincipal();
        chosenGoodsService = (ChosenGoodsService) session.getAttribute("chosenGoodsService");
        final String shopGoodId = request.getParameter("shopGoodId");
        if (shopGoodId != null) {
            GoodDto goodDto = goodService.getGoodById(Long.parseLong(shopGoodId));
            chosenGoodsService.addGoodToList(goodDto);
        }
        final ModelAndView content = new ModelAndView("online-shop");
        content.addObject("user", user);
        content.addObject("shopGoodList", goodService.getGoodsList());
        content.addObject("usersOrderList", orderService.getOrderListByUserId(user.getUserId()));
        content.addObject("chosenGoodsList", chosenGoodsService.getGoodsList());
        return content;
    }
    
    /**
     * 
     * @param id
     *            id of the order.
     * @return {@link ModelAndView} related to the '/order-info/{id}' URL.
     */
    @GetMapping("/order-info/{id}")
    public ModelAndView orderInfo(@PathVariable final Long id) {
        final ShopUserPrincipal user = (ShopUserPrincipal)SecurityContextHolder
                    .getContext().getAuthentication().getPrincipal();
        final BigDecimal orderPrice = orderService.getOrderById(id).getTotalPrice();
        final List<OrderGoodInfoDto> orderInfo = orderGoodInfoService.getInfoByOrderId(id);
        final List<GoodDto> goodList = goodService.getGoodsListByOrdersInfo(orderInfo);
        final ModelAndView content = new ModelAndView("order-info");
        content.addObject("user", user);
        content.addObject("orderId", id);
        content.addObject("orderPrice", orderPrice);
        content.addObject("goodList", goodList);
        return content;
    }
    
    /**
     * 
     * @param request
     *            {@link HttpServletRequest}.
     * @return {@link ModelAndView} related to the '/order' URL using POST method.
     */
    @PostMapping("/order")
    public ModelAndView submitOrder(HttpServletRequest request) {
        final ShopUserPrincipal user = (ShopUserPrincipal)SecurityContextHolder
                    .getContext().getAuthentication().getPrincipal();
        HttpSession session = request.getSession();
        final ChosenGoodsService chosenGoodsService
                = (ChosenGoodsService) session.getAttribute("chosenGoodsService");
        final BigDecimal totalPrice = getTotalPrice(chosenGoodsService.getGoodsList());
        if (!chosenGoodsService.getGoodsList().isEmpty()) {
            orderProcessing(chosenGoodsService.getGoodsList(), user.getUserId(), totalPrice);
        }
        final ModelAndView content = new ModelAndView("submit-order");
        content.addObject("user", user);
        content.addObject("totalPrice", totalPrice);
        content.addObject("chosenGoodsList", chosenGoodsService.getGoodsList());
        return content;
    }
    
    /**
     * Method calculates total price of the user's order.
     *
     * @param chosenGoodsList
     *            list of the shop goods which user wants to order.
     */
    private BigDecimal getTotalPrice(List<GoodDto> chosenGoodsList) {
        BigDecimal totalPrice = new BigDecimal(0);
        for (GoodDto good : chosenGoodsList) {
            totalPrice = totalPrice.add(good.getPrice());
        }
        return totalPrice;
    }
    
    /**
     * Method saves information about user's order into shop database.
     *
     * @param goodsList
     *            list of the shop goods which user has ordered.
     * @param user id
     *            id of the user.
     * @param totalPrice
     *            total price of the order.                      
     */
    private void orderProcessing(List<GoodDto> goodList, Long userId, BigDecimal totalPrice) {
        final Long newOrderId = orderService.generateNewOrderId();
        final OrderDto newOrder = new OrderDto(newOrderId, userId, totalPrice);
        orderService.saveOrder(newOrder);
        for (GoodDto good : chosenGoodsService.getGoodsList()) {
            OrderGoodInfoDto orderGoodInfo = new OrderGoodInfoDto(
                    orderGoodInfoService.generateNewOrderGoodId(),
                    newOrderId, good.getId());
            orderGoodInfoService.saveOrderGoodInfo(orderGoodInfo);
        }
    }
}