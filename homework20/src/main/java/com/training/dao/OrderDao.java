package com.training.dao;

import com.training.entity.Order;

import java.util.List;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link Order} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface OrderDao {

    /**
     * @return Returns a list of all orders contained in the database.
     */
    List<Order> getOrderList();

    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    List<Order> getOrderListByUserId(final Long id);

    /**
     * Get Order object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link Order} class.
     */
    Order getOrderById(final Long id);

    /**
     * Insert into database information from instance of the {@link Order} class.
     *
     * @param order
     *            instance of the {@link Order} class.
     * @return boolean result of the operation.
     */
    boolean insertOrder(final Order order);
}