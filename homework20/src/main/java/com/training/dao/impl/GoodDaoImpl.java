package com.training.dao.impl;

import com.training.dao.GoodDao;
import com.training.entity.Good;
import com.training.util.H2Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link GoodDao} interface. 
 *
 * @author Alexandr_Terehov
 */
@Repository
public class GoodDaoImpl implements GoodDao {
    private static final String SQL_SELECT_ALL_GOODS = "SELECT * FROM good";
    private static final String SQL_SELECT_GOOD_BY_ID =
            "SELECT * FROM good WHERE id=?";
    
    private static final int FIRST_STATEMENT_ARG = 1;
    
    private H2Utils h2Utils;
    
    /**
     * Constructor
     * 
     * @param h2Utils
     *            object of the {@link H2Utils} class to set.
     */
    @Autowired
    public GoodDaoImpl(final H2Utils h2Utils) {
        this.h2Utils = h2Utils;
    }

    /**
     * @return Returns a list of all goods contained in the database.
     */
    public List<Good> getGoodsList() {
        List<Good> goods = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet =
                    statement.executeQuery(SQL_SELECT_ALL_GOODS);
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String title = resultSet.getString("title");
                BigDecimal price = resultSet.getBigDecimal("price");
                Good good = new Good(id, title, price);
                goods.add(good);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return goods;
    }

    /**
     * Get Good object by it's ID.
     *
     * @param id
     *            id of the good.
     * @return object of the {@link Good} class.
     */
    public Good getGoodById(final Long id) {
        Good good = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_GOOD_BY_ID);
            statement.setLong(FIRST_STATEMENT_ARG, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Long goodId = resultSet.getLong("id");
                String title = resultSet.getString("title");
                BigDecimal price = resultSet.getBigDecimal("price");
                good = new Good(goodId, title, price);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return good;
    }
}