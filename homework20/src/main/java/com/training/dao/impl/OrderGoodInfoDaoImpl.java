package com.training.dao.impl;

import com.training.dao.OrderGoodInfoDao;
import com.training.entity.OrderGoodInfo;
import com.training.util.H2Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderGoodInfoDao} interface. 
 *
 * @author Alexandr_Terehov
 */
@Repository
public class OrderGoodInfoDaoImpl implements OrderGoodInfoDao {
    private static final String SQL_SELECT_ALL_ORDER_GOOD = "SELECT * FROM order_good";
    private static final String SQL_SELECT_INFO_BY_ORDER_ID =
            "SELECT * FROM order_good WHERE order_id=?";
    private static final  String SQL_INSERT_ORDER_GOOD_INFO =
            "INSERT INTO order_good(id, order_id, good_id ) VALUES(?,?,?)";
    
    private static final int FIRST_STATEMENT_ARG = 1;
    private static final int SECOND_STATEMENT_ARG = 2;
    private static final int THIRD_STATEMENT_ARG = 3;
   
    private H2Utils h2Utils;
    
    /**
     * Constructor
     * 
     * @param h2Utils
     *            object of the {@link H2Utils} class to set.
     */
    @Autowired
    public OrderGoodInfoDaoImpl(final H2Utils h2Utils) {
        this.h2Utils = h2Utils;
    }
    
    /**
     * @return Returns a list of all {@link OrderGoodInfo} objects contained in the
     *         database.
     */
    public List<OrderGoodInfo> getOrderGoodInfoList() {
        List<OrderGoodInfo>  orderGoodInfoEvents = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_ORDER_GOOD);
            while (resultSet.next()) {
                Long infoId = resultSet.getLong("id");
                Long orderId = resultSet.getLong("order_id");
                Long goodId = resultSet.getLong("good_id");
                OrderGoodInfo infoEvent = new OrderGoodInfo(infoId, orderId, goodId);
                orderGoodInfoEvents.add(infoEvent);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return orderGoodInfoEvents;
    }

    /**
     * Get OrderGoodInfo object by it's ID.
     *
     * @param id
     *            id of the OrderGoodInfo entry.
     * @return object of the {@link OrderGoodInfo} class.
     */
    public  List<OrderGoodInfo> getInfoByOrderId(final Long id) {
        List<OrderGoodInfo> orderGoodInfoEvents = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_INFO_BY_ORDER_ID);
            statement.setLong(FIRST_STATEMENT_ARG, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long infoId = resultSet.getLong("id");
                Long orderId = resultSet.getLong("order_id");
                Long goodId = resultSet.getLong("good_id");
                OrderGoodInfo infoEvent = new OrderGoodInfo(infoId, orderId, goodId);
                orderGoodInfoEvents.add(infoEvent);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return orderGoodInfoEvents;
    }

    /**
     * Insert into database information from instance of the {@link OrderGoodInfo}
     * class.
     *
     * @param orderGoodInfo
     *            instance of the {@link OrderGoodInfo} class.
     * @return boolean result of the operation.
     */
    public boolean insertOrderGoodInfo(final OrderGoodInfo orderGoodInfo) {
        boolean flag = false;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ORDER_GOOD_INFO);
            statement.setLong(FIRST_STATEMENT_ARG, orderGoodInfo.getId()); 
            statement.setLong(SECOND_STATEMENT_ARG, orderGoodInfo.getOrderId());
            statement.setLong(THIRD_STATEMENT_ARG, orderGoodInfo.getGoodId());
            statement.executeUpdate();
            flag = true;
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return flag;
    }
}