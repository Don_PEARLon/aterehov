package com.training.dao.impl;

import com.training.dao.OrderDao;
import com.training.entity.Order;
import com.training.util.H2Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderDao} interface. 
 *
 * @author Alexandr_Terehov
 */
@Repository
public class OrderDaoImpl implements OrderDao {
    private static final String SQL_SELECT_ALL_ORDERS =
            "SELECT * FROM orders";
    private static final String SQL_SELECT_ORDERS_BY_USER_ID =
            "SELECT * FROM orders WHERE user_id=?";
    private static final String SQL_SELECT_ORDER_BY_ID =
            "SELECT * FROM orders WHERE id=?";
    private static final  String SQL_INSERT_ORDER =
            "INSERT INTO orders(id, user_id, total_price ) VALUES(?,?,?)";
    
    private static final int FIRST_STATEMENT_ARG = 1;
    private static final int SECOND_STATEMENT_ARG = 2;
    private static final int THIRD_STATEMENT_ARG = 3;
    
    private H2Utils h2Utils;
    
    /**
     * Constructor
     * 
     * @param h2Utils
     *            object of the {@link H2Utils} class to set.
     */
    @Autowired
    public OrderDaoImpl(final H2Utils h2Utils) {
        this.h2Utils = h2Utils;
    }
    
    /**
     * @return Returns a list of all orders contained in the database.
     */
    public List<Order> getOrderList() {
        List<Order> orders = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_ORDERS);
            while (resultSet.next()) {
                Long orderId = resultSet.getLong("id");
                Long userId = resultSet.getLong("user_id");
                BigDecimal totalPrice = resultSet.getBigDecimal("total_price");
                Order order = new Order(orderId, userId, totalPrice);
                orders.add(order);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return orders;
    }

    /**
     * Get list of the orders related to user's ID.
     *
     * @param userId
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    public List<Order> getOrderListByUserId(final Long userId) {
        List<Order> orders = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ORDERS_BY_USER_ID);
            statement.setLong(FIRST_STATEMENT_ARG, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long orderId = resultSet.getLong("id");
                BigDecimal totalPrice = resultSet.getBigDecimal("total_price");
                Order order = new Order(orderId, userId, totalPrice);
                orders.add(order);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return orders;
    }

    /**
     * Get Order object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link Order} class.
     */
    public Order getOrderById(final Long id) {
        Order order = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ORDER_BY_ID);
            statement.setLong(FIRST_STATEMENT_ARG, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Long orderId = resultSet.getLong("id");
                Long userId = resultSet.getLong("user_id");
                BigDecimal totalPrice = resultSet.getBigDecimal("total_price");
                order = new Order(orderId, userId, totalPrice);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return order;
    }

    /**
     * Insert into database information from instance of the {@link Order} class.
     *
     * @param order
     *            instance of the {@link Order} class.
     * @return boolean result of the operation.
     */
    public boolean insertOrder(final Order order) {
        boolean flag = false;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ORDER);
            statement.setLong(FIRST_STATEMENT_ARG, order.getId());
            statement.setLong(SECOND_STATEMENT_ARG, order.getUserId());
            statement.setBigDecimal(THIRD_STATEMENT_ARG, order.getTotalPrice());
            statement.executeUpdate();
            flag = true;
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return flag;
    }
}