package com.training.dao.impl;

import com.training.dao.UserDao;
import com.training.entity.User;
import com.training.entity.role.Role;
import com.training.util.H2Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link UserDao} interface. 
 *
 * @author Alexandr_Terehov
 */
@Repository
public class UserDaoImpl implements UserDao {
    private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM user";
    private static final String SQL_SELECT_USER_BY_ID =
            "SELECT * FROM user WHERE id=?";
    private static final String SQL_SELECT_USER_BY_LOGIN =
            "SELECT * FROM user WHERE login=?";
    
    private static final int FIRST_STATEMENT_ARG = 1;
        
    
    private H2Utils h2Utils;
    
    /**
     * Constructor
     * 
     * @param h2Utils
     *            object of the {@link H2Utils} class to set.
     */
    @Autowired
    public UserDaoImpl(final H2Utils h2Utils) {
        this.h2Utils = h2Utils;
    }
    
    /**
     * @return Returns a list of all users contained in the database.
     */
    public List<User> getUserList() {
        List<User> users = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet =
                    statement.executeQuery(SQL_SELECT_ALL_USERS);
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String role = resultSet.getString("role");
                User user = new User(id, login, password, Role.valueOf(role.toUpperCase()));
                users.add(user);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return users;
    }

    /**
     * Get User object by it's ID.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link User} class.
     */
    public User getUserById(final Long id) {
        User user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_BY_ID);
            statement.setLong(FIRST_STATEMENT_ARG, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Long userId = resultSet.getLong("id");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String role = resultSet.getString("role");
                user = new User(userId, login, password, Role.valueOf(role.toUpperCase()));
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        }  
        return user;
    }

    /**
     * Get User object by it's login and password.
     *
     * @param login
     *            login of the user.
     * @return object of the {@link User} class.
     */
    public User getUserByLogin(final String login) {
        User user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
            statement.setString(FIRST_STATEMENT_ARG, login);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Long userId = resultSet.getLong("id");
                String userLogin = resultSet.getString("login");
                String password = resultSet.getString("password");
                String role = resultSet.getString("role");
                user = new User(userId, userLogin, password, Role.valueOf(role.toUpperCase()));
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        }  
        return user;
    }
}