package com.training.util;

import org.h2.tools.RunScript;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class provides access to database.
 *
 * @author Alexandr_Terehov
 */
@Component
@Scope("singleton")
public class H2Utils {
    private static final String CREATE_TABLE_SQL_PATH = "/sql/create.sql";
    private static final String FILL_TABLE_SQL_PATH = "/sql/fill.sql";
    private static boolean initFlag = false;
        
    private DriverManagerDataSource dataSource;
    
    /**
     * Constructor
     * 
     * @param dataSource
     *            object of the {@link DriverManagerDataSource} class to set.
     */
    @Autowired
    public H2Utils(final DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @return {@link Connection} connection to database.
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            if (!initFlag) {
                Reader createTableReader = new BufferedReader(new InputStreamReader(
                        getClass().getResourceAsStream(CREATE_TABLE_SQL_PATH)));
                Reader fillTableReader = new BufferedReader(new InputStreamReader(
                        getClass().getResourceAsStream(FILL_TABLE_SQL_PATH)));
                RunScript.execute(connection,createTableReader);
                RunScript.execute(connection, fillTableReader);
                initFlag = true;
            } 
        } catch (SQLException exc) {
            System.err.println(exc.getMessage());
        }
        return connection;
    }

    /**
     * Method used to close statement.
     * 
     * @param statement
     *            the {@link Statement}.
     */
    public void close(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException exc) {
            System.err.println("Statement can't be closed " + exc.getMessage());
        }
    }

    /**
     * Method used to close connection to database.
     * 
     * @param connection
     *            the {@link Connection}.
     */
    public void close(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException exc) {
            System.err.println("Connection can't be closed " + exc.getMessage());
        }
    }
}