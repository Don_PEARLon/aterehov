package com.training.interceptor;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interceptor blocks access to 'login' page for user who has been already
 * successfully logged in.
 * 
 * @author Alexandr_Terehov
 */
public class LogoutInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, 
                HttpServletResponse response, Object handler)
            throws Exception {
        boolean authCondition = false;
        if (SecurityContextHolder.getContext().getAuthentication() != null 
                && SecurityContextHolder.getContext().getAuthentication().isAuthenticated() 
                && !(SecurityContextHolder.getContext().getAuthentication() 
                          instanceof AnonymousAuthenticationToken)) {
            authCondition = true;
        }
        if (authCondition) {
            response.sendRedirect("logout-confirm");
            return false;
        }
        return true;
    }
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {

    }
    
    @Override
    public void afterCompletion(HttpServletRequest request,
                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }
}