package com.training.filter;

import com.training.exception.ShopAuthenticationException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Custom filter called prior spring Spring Security Authentication of login
 * form parameters. Extends {@link UsernamePasswordAuthenticationFilter}
 * 
 * @author Alexandr_Terehov
 */
public class ShopAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    @Override
    public Authentication attemptAuthentication(
                HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        final String termsAgreement = request.getParameter("termsAgreement");
        if (termsAgreement == null) {
            throw new ShopAuthenticationException("You must agree with terms of service");
        }
        return super.attemptAuthentication(request, response);
    }
}