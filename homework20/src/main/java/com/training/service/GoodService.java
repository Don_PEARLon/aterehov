package com.training.service;

import com.training.dto.GoodDto;
import com.training.dto.OrderGoodInfoDto;

import java.util.List;

/**
 * Interface used for representing a good service which provides various
 * operations with goods of the online shop.
 *
 * @author Alexandr_Terehov
 */
public interface GoodService {

    /**
     * @return Returns a list of all goods of the online shop.
     */
    List<GoodDto> getGoodsList();

    /**
     * Get GoodDto object by it's ID.
     *
     * @param id
     *            id of the good.
     * @return object of the {@link GoodDto} class.
     */
    GoodDto getGoodById(final Long id);

    /**
     * Get list of goods related to the information contained in the
     * {@link OrderGoodInfoDto} list.
     *
     * @param orderInfo
     *            {@link OrderGoodInfoDto} list
     * @return list of goods corresponding to the information contained in the
     *         orderInfo.
     */
    List<GoodDto> getGoodsListByOrdersInfo(final List<OrderGoodInfoDto> orderInfo);
}