package com.training.service;

import com.training.dto.OrderGoodInfoDto;

import java.util.List;

/**
 * Interface used for representing a user service which provides various
 * operations with information about orders and goods of the online shop.
 *
 * @author Alexandr_Terehov
 */
public interface OrderGoodInfoService {

    /**
     * @return Returns a list of all entries containing information about orders and
     *         goods of the online shop.
     */
    List<OrderGoodInfoDto> getOrderGoodInfoList(); 

    /**
     * Get List if the {@link OrderGoodInfoDto} objects related to order ID.
     *
     * @param id
     *            id of the OrderGoodInfoDto entry.
     * @return list of the {@link OrderGoodInfoDto} objects related to order Id.
     */
    List<OrderGoodInfoDto> getInfoByOrderId(final Long id);
    
    /**
     * Saves information retrieved from instance of the {@link OrderGoodInfoDto} class.
     *
     * @param orderGoodInfoDto
     *            instance of the {@link OrderGoodInfoDto} class.
     * @return boolean result of the operation.
     */
    boolean saveOrderGoodInfo(final OrderGoodInfoDto orderGoodInfoDto);

    /**
     * Generates ID of the new OrderGoodInfo entry. 
     */
    Long generateNewOrderGoodId();
}