package com.training.service.impl;

import com.training.converter.OrderGoodInfoConverter;
import com.training.dao.OrderGoodInfoDao;
import com.training.dto.OrderGoodInfoDto;
import com.training.entity.OrderGoodInfo;
import com.training.service.OrderGoodInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderGoodInfoService} interface.
 *
 * @author Alexandr_Terehov
 */
@Service
public class OrderGoodInfoServiceImpl implements OrderGoodInfoService {
    private OrderGoodInfoDao orderGoodInfoDao;
        
    private OrderGoodInfoConverter orderGoodInfoConverter;
    
    /**
     * Constructor
     * 
     * @param orderGoodInfoDao
     *            instance of the class implements {@link OrderGoodInfoDao} interface.
     * 
     * @param orderGoodInfoConverter
     *            instance of the {@link OrderGoodInfoConverter} class.            
     */
    @Autowired
    public OrderGoodInfoServiceImpl(final OrderGoodInfoDao orderGoodInfoDao,
            final OrderGoodInfoConverter orderGoodInfoConverter) {
        this.orderGoodInfoDao = orderGoodInfoDao;
        this.orderGoodInfoConverter = orderGoodInfoConverter;
    }
  
    /**
     * @return Returns a list of all entries containing information about orders and
     *         goods of the online shop.
     */
    public List<OrderGoodInfoDto> getOrderGoodInfoList() {
        List<OrderGoodInfoDto> orderGoodInfoDtoList = new ArrayList<>();
        for (OrderGoodInfo orderGoodInfo: 
                orderGoodInfoDao.getOrderGoodInfoList()) {
            OrderGoodInfoDto orderGoodInfoDto = orderGoodInfoConverter.toDto(orderGoodInfo);
            orderGoodInfoDtoList.add(orderGoodInfoDto);
        }
        return orderGoodInfoDtoList;
    }

    /**
     *  Get List if the {@link OrderGoodInfoDto} objects related to order ID.
     *
     * @param id
     *            id of the OrderGoodInfoDto entry.
     * @return list of the {@link OrderGoodInfoDto} objects related to order Id.
     */
    public List<OrderGoodInfoDto> getInfoByOrderId(final Long id) {
        List<OrderGoodInfoDto> orderGoodInfoDtoList = new ArrayList<>();
        for (OrderGoodInfo orderGoodInfo: 
                orderGoodInfoDao.getInfoByOrderId(id)) {
            OrderGoodInfoDto orderGoodInfoDto = orderGoodInfoConverter.toDto(orderGoodInfo);
            orderGoodInfoDtoList.add(orderGoodInfoDto);
        }
        return orderGoodInfoDtoList;
    }

    /**
     * Saves information retrieved from instance of the {@link OrderGoodInfoDto} class.
     *
     * @param orderGoodInfoDto
     *            instance of the {@link OrderGoodInfoDto} class.
     * @return boolean result of the operation.
     */
    public boolean saveOrderGoodInfo(final OrderGoodInfoDto orderGoodInfoDto) {
        OrderGoodInfo orderGoodInfo = orderGoodInfoConverter.toEntity(orderGoodInfoDto);
        return orderGoodInfoDao.insertOrderGoodInfo(orderGoodInfo);
    }

    /**
     * Generates ID of the new OrderGoodInfoDto.
     */
    public Long generateNewOrderGoodId() {
        List<OrderGoodInfo> orderGoodInfoList = orderGoodInfoDao.getOrderGoodInfoList();
        Long maxId = 0L;
        if (!orderGoodInfoList.isEmpty()) {
            maxId = orderGoodInfoList.get(0).getId();
            for (OrderGoodInfo infoEvent : orderGoodInfoList) {
                if (infoEvent.getId() > maxId) {
                    maxId = infoEvent.getId();
                }
            }
        }
        return maxId + 1;
    }
}