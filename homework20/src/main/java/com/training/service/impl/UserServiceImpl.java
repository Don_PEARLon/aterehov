package com.training.service.impl;

import com.training.converter.UserConverter;
import com.training.dao.UserDao;
import com.training.dto.UserDto;
import com.training.entity.User;
import com.training.exception.UserNotFoundExc;
import com.training.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link UserService} interface.
 *
 * @author Alexandr_Terehov
 */
@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;
    
    private UserConverter userConverter;
    
    /**
     * 
     * @param userDao
     *            instance of the class implements {@link UserDao} interface.
     * @param userConverter
     *            instance of the {@link UserConverter} class.
     */
    @Autowired
    public UserServiceImpl(final UserDao userDao, final UserConverter userConverter) {
        this.userDao = userDao;
        this.userConverter = userConverter;
    }
   
    /**
     * @return Returns a list of all users of she online shop.
     */
    public List<UserDto> getUserList() {
        List<UserDto> userDtoList = new ArrayList<>();
        for (User user: userDao.getUserList()) {
            UserDto userDto = userConverter.toDto(user);
            userDtoList.add(userDto);
        }
        return userDtoList;
    }

    /**
     * Get UserDto object by ID of the user.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link UserDto} class.
     * @throws {@link userNotFoundExc}
     *            if user was not found
     */
    public UserDto getUserById(final Long id) {
        User user = userDao.getUserById(id);
        if (user != null) {
            return userConverter.toDto(user);
        } else {
            throw new UserNotFoundExc();
        }
    }

    /**
     * Get UserDto object by user's login.
     *
     * @param login
     *            login of the user.
     * @return object of the {@link UserDto} class.
     * @throws {@link userNotFoundExc}
     *            if user was not found
     */
    public UserDto getUserByLogin(final String login) {
        User user = userDao.getUserByLogin(login);
        if (user != null) {
            return userConverter.toDto(user);
        } else {
            throw new UserNotFoundExc();
        }
    }
}