package com.training.service.impl;

import com.training.converter.OrderConverter;
import com.training.dao.OrderDao;
import com.training.dto.OrderDto;
import com.training.entity.Order;
import com.training.exception.OrderNotFoundExc;
import com.training.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderService} interface.
 *
 * @author Alexandr_Terehov
 */
@Service
public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;
    
    private OrderConverter orderConverter;
    
    /**
     * Constructor
     * 
     * @param orderDao
     *            instance of the class implements {@link OrderDao} interface.
     * @param orderConverter
     *            instance of the {@link OrderConverter} class.            
     */
    @Autowired
    public OrderServiceImpl(final OrderDao orderDao, final OrderConverter orderConverter) {
        this.orderDao = orderDao;
        this.orderConverter = orderConverter;
    }
  
    /**
     * @return Returns a list of all users of the online shop.
     */
    public List<OrderDto> getOrderList() {
        List<OrderDto> orderDtoList = new ArrayList<>();
        for (Order order: orderDao.getOrderList()) {
            OrderDto orderDto = orderConverter.toDto(order);
            orderDtoList.add(orderDto);
        }
        return orderDtoList;
    }
  
    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    public List<OrderDto> getOrderListByUserId(final Long id) {
        List<OrderDto> orderDtoList = new ArrayList<>();
        for (Order order:  orderDao.getOrderListByUserId(id)) {
            OrderDto orderDto = orderConverter.toDto(order);
            orderDtoList.add(orderDto);
        }
        return  orderDtoList;
    }

    /**
     * Get OrderDto object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link OrderDto} class.
       * @throws {@link OrderNotFoundExc}
     *            if order was not found. 
     */
    public OrderDto getOrderById(final Long id) {
        Order order = orderDao.getOrderById(id);
        if (order != null) {
            OrderDto orderDto = orderConverter.toDto(order);
            return orderDto;
        } else {
            throw new OrderNotFoundExc();
        }
    }

    /**
     * Saves information about new order.
     *
     * @param orderDto
     *            instance of the {@link OrderDto} class.
     * @return boolean result of the operation.
     * @throws {@link OrderNotFoundExc}
     *            if order was not found. 
     */
    public boolean saveOrder(final OrderDto orderDto) {
        if (orderDto == null) {
            throw new OrderNotFoundExc();
        }
        Order order = orderConverter.toEntity(orderDto);
        return orderDao.insertOrder(order);
    }

    /**
     * Generates ID of the new order. 
     */
    public Long generateNewOrderId() {
        List<Order> orders = orderDao.getOrderList();
        Long maxId = 0L;
        if (!orders.isEmpty()) {
            maxId = orders.get(0).getId();
            for (Order order : orders) {
                if (order.getId() > maxId) {
                    maxId = order.getId();
                }
            }
        }
        return maxId + 1;
    }
}