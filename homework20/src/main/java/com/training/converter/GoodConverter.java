package com.training.converter;

import com.training.dto.GoodDto;
import com.training.entity.Good;

import org.springframework.stereotype.Component;

/**
 * Class provides conversion operations for instances of the {@link Good} and
 * {@link GoodDto} classes.
 *
 * @author Alexandr_Terehov
 */
@Component
public class GoodConverter {
    
    /**
     * Provides conversion of the instance of the {@link GoodDto} class to the
     * instance of the {@link Good} class
     *
     * @param goodDto
     *            instance of the {@link GoodDto} class.
     */
    public Good toEntity(final GoodDto goodDto) {
        return new Good(goodDto.getId(), goodDto.getTitle(), goodDto.getPrice());
    }
    
    /**
     * Provides conversion of the instance of the {@link Good} class to the
     * instance of the {@link GoodDto} class
     *
     * @param good
     *            instance of the {@link Good} class.
     */
    public GoodDto toDto(final Good good) {
        return new GoodDto(good.getId(), good.getTitle(), good.getPrice());
    }
}