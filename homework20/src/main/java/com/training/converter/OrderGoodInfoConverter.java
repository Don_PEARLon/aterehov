package com.training.converter;

import com.training.dto.OrderGoodInfoDto;
import com.training.entity.OrderGoodInfo;

import org.springframework.stereotype.Component;

/**
 * Class provides conversion operations for instances of the
 * {@link OrderGoodInfo} and {@link OrderGoodInfoDto} classes.
 *
 * @author Alexandr_Terehov
 */
@Component
public class OrderGoodInfoConverter {
    
    /**
     * Provides conversion of the instance of the {@link OrderGoodInfoDto} class to
     * the instance of the {@link OrderGoodInfo} class
     *
     * @param orderGoodInfoDto
     *            instance of the {@link OrderGoodInfoDto} class.
     */
    public OrderGoodInfo toEntity(final OrderGoodInfoDto orderGoodInfoDto) {
        return new OrderGoodInfo(orderGoodInfoDto.getId(),
                orderGoodInfoDto.getOrderId(), orderGoodInfoDto.getGoodId());
    }
    
    /**
     * Provides conversion of the instance of the {@link OrderGoodInfo} class to the
     * instance of the {@link OrderGoodInfoDto} class
     *
     * @param orderGoodInfo
     *            instance of the {@link OrderGoodInfo} class.
     */
    public OrderGoodInfoDto toDto(final OrderGoodInfo orderGoodInfo) {
        return new OrderGoodInfoDto(orderGoodInfo.getId(),
                orderGoodInfo.getOrderId(), orderGoodInfo.getGoodId());
    }
}