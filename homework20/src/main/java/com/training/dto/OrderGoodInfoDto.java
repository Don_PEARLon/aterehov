package com.training.dto;

import java.util.Objects;

import javax.validation.constraints.Min;

/**
 * Class represents an information about goods included in orders of the users
 * of the online shop.
 *
 * @author Alexandr_Terehov
 */
public class OrderGoodInfoDto {
    private final Long id;
    private final Long orderId;
    private final Long goodId;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the orderGoodInfo event.
     * @param orderId
     *            id of the order.
     * @param goodId
     *            id of the good.
     */
    public OrderGoodInfoDto(Long id, 
            @Min(value = 1, message = "Order Id must be greater than zero")Long orderId, 
            @Min(value = 1, message = "Good Id must be greater than zero")Long goodId) {
        this.id = id;
        this.orderId = orderId;
        this.goodId = goodId;
    }

    /**
     * @return id of the order-good info.
     */
    public Long getId() {
        return id;
    }
      
    /**
     * @return Id of the order.
     */
    public Long getOrderId() {
        return orderId;
    }
       
    /**
     * @return Id of the good.
     */
    public Long getGoodId() {
        return goodId;
    }
        
    /**
     * @return hashCode of the object of the {@link OrderGoodInfoDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                 + ((orderId == null) ? 0 : orderId.hashCode())
                 + ((goodId == null) ? 0 : goodId.hashCode()));
    }
    
    /**
     * Method used to compare this orderGoodInfo event to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderGoodInfoDto other = (OrderGoodInfoDto) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(orderId, other.getOrderId())) {
            return false;
        }
        if (!Objects.equals(goodId, other.getGoodId())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of the object of the {@link OrderGoodInfoDto} class .
     */
    @Override
    public String toString() {
        return "OrderGoodInfoDto [id=" + id + ", orderId=" + orderId + ", goodId=" + goodId + "]";
    }
}