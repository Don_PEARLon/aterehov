package com.training.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

/**
 * Class represents an item of the online-shop.
 *
 * @author Alexandr_Terehov
 */
public class GoodDto {
    private final Long id;
    private final String title;
    private final BigDecimal price;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the good.
     * @param title
     *            title of the good.
     * @param price
     *            price of the good.
     */    
    public GoodDto(Long id, @NotBlank String title, 
            @DecimalMin(value = "1.0", inclusive = true)BigDecimal price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    /**
     * @return id of the good.
     */
    public Long getId() {
        return id;
    }
   
    /**
     * @return title of the good.
     */
    public String getTitle() {
        return title;
    }
        
    /**
     * @return title of the item.
     */
    public BigDecimal getPrice() {
        return price;
    }
        
    /**
     * @return hashCode of the object of the {@link GoodDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode()) 
                + ((price == null) ? 0 : price.hashCode())
                + ((title == null) ? 0 : title.hashCode()));
    }
    
    /**
     * Method used to compare this good to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GoodDto other = (GoodDto) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(title, other.getTitle())) {
            return false;
        }
        if (!Objects.equals(price, other.getPrice())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link GoodDto} class .
     */
    @Override
    public String toString() {
        return "GoodDto [id=" + id + ", title=" + title + ", price=" + price + "]";
    }
}