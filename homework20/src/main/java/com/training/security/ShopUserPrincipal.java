package com.training.security;

import com.training.entity.User;
import com.training.entity.role.Role;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Custom implementation of the {@link UserDetails} interface. Represents user
 * of the online shop.
 *
 * @author Alexandr_Terehov
 */
public class ShopUserPrincipal implements UserDetails {
    
    private static final long serialVersionUID = 6568657457345114247L;

    private User user;

    public ShopUserPrincipal(final User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final List<SimpleGrantedAuthority> authorities = new ArrayList<>(3);
        authorities.add(new SimpleGrantedAuthority(Role.SURFER.name()));
        authorities.add(new SimpleGrantedAuthority(Role.USER.name()));
        authorities.add(new SimpleGrantedAuthority(Role.ADMIN.name()));
        return authorities;
    }
    
    /**
     * @return id of the user.
     */
    public Long getUserId() {
        return user.getId();
    }
    
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}