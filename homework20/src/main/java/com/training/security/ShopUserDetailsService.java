package com.training.security;

import com.training.dao.UserDao;
import com.training.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Custom implementation of the {@link UserDetailsService}.
 *
 * @author Alexandr_Terehov
 */
@Service
public class ShopUserDetailsService implements UserDetailsService {
    private UserDao userDao;

    /**
     * Constructor.
     *
     * @param userDao
     *            {@link UserDao}.
     */    
    @Autowired
    public ShopUserDetailsService(final UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * 
     * @param username
     *            name of the user.
     * 
     * @return instance of the {@link ShopUserPrincipal}.
     * 
     * @throws UsernameNotFoundException
     *                 if user was not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.getUserByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("User 404");
        }
        return new ShopUserPrincipal(user);
    }
}