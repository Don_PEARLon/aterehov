package com.training.entity.role;

/**
 * Enum for possible roles of the online shop user.
 *
 * @author Alexandr_Terehov
 */
public enum Role {
    SURFER,
    USER,
    ADMIN
}