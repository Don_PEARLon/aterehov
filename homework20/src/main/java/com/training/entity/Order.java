package com.training.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class represents an order in the online shop.
 *
 * @author Alexandr_Terehov
 */
public class Order {
    private Long id;
    private Long userId;
    private BigDecimal totalPrice;
   
    /**
     * Constructor.
     *
     * @param id
     *            id of the order.
     * @param userId
     *            id of the user.
     * @param totalPrice
     *            total price of the order.
     */
    public Order(final Long id, final Long userId, final BigDecimal totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }
    
    /**
     * @return id of the good.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * 
     * @param id
     *            id of the order to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(Long id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return Id of the user who made the order.
     */
    public Long getUserId() {
        return userId;
    }
    
    /**
     * 
     * @param userId
     *            id of the user who made the order.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setUserId(Long userId) {
        if (userId > 0) {
            this.userId = userId;
        } else {
            throw new IllegalArgumentException(" User ID must be > 0");
        } 
    }
    
    /**
     * @return total price of the order.
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }
    
    /**
     * 
     * @param totalPrice
     *            total price of the order.
     * @throws IllegalArgumentException
     *             if totalPrice < 0.
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        if (totalPrice.compareTo(BigDecimal.ZERO) == 1) {
            this.totalPrice = totalPrice;
        } else {
            throw new IllegalArgumentException("Total price can't be < 0");
        } 
    }
    
    /**
     * @return hashCode of the object of the {@link Order} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((userId == null) ? 0 : userId.hashCode())
                + ((totalPrice == null) ? 0 : totalPrice.hashCode()));
    }
    
    /**
     * Method used to compare this order to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Order other = (Order) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(totalPrice, other.getTotalPrice())) {
            return false;
        }
        if (!Objects.equals(userId, other.getUserId())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link Order} class .
     */
    @Override
    public String toString() {
        return "Order [id=" + id + ", userId=" + userId + ", totalPrice=" + totalPrice + "]";
    }
}