package com.training.entity;

import java.util.Objects;

/**
 * Class represents an information about goods included in orders of the users
 * of the online shop.
 *
 * @author Alexandr_Terehov
 */
public class OrderGoodInfo {
    private Long id;
    private Long orderId;
    private Long goodId;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the orderGoodInfo event.
     * @param orderId
     *            id of the order.
     * @param goodId
     *            id of the good.
     */
    public OrderGoodInfo(final Long id, final Long orderId, final Long goodId) {
        this.id = id;
        this.orderId = orderId;
        this.goodId = goodId;
    }

    /**
     * @return id of the order-good info.
     */
    public Long getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the order-good info to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(Long id) {
        if (id > 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return Id of the order.
     */
    public Long getOrderId() {
        return orderId;
    }
    
    /**
     * 
     * @param orderId
     *            id of the order.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setOrderId(Long orderId) {
        if (orderId > 0) {
            this.orderId = orderId;
        } else {
            throw new IllegalArgumentException(" User ID must be > 0");
        } 
    }
    
    /**
     * @return Id of the good.
     */
    public Long getGoodId() {
        return goodId;
    }
    
    /**
     * 
     * @param goodId
     *            id of the good.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setGoodId(Long goodId) {
        if (goodId > 0) {
            this.goodId = goodId;
        } else {
            throw new IllegalArgumentException(" Good ID must be > 0");
        } 
    }
    
    /**
     * @return hashCode of the object of the {@link OrderGoodInfo} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((orderId == null) ? 0 : orderId.hashCode())
                + ((goodId == null) ? 0 : goodId.hashCode()));
    }
    
    /**
     * Method used to compare this orderGoodInfo event to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderGoodInfo other = (OrderGoodInfo) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(orderId, other.getOrderId())) {
            return false;
        }
        if (!Objects.equals(goodId, other.getGoodId())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of the object of the {@link OrderGoodInfo} class .
     */
    @Override
    public String toString() {
        return "OrderGoodInfo [id=" + id + ", orderId=" + orderId + ", goodId=" + goodId + "]";
    }
}