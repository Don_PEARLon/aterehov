package com.training.entity;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class represents an item of the online-shop.
 *
 * @author Alexandr_Terehov
 */
public class Good {
    private Long id;
    private String title;
    private BigDecimal price;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the good.
     * @param title
     *            title of the good.
     * @param price
     *            price of the good.
     */    
    public Good(Long id, String title, BigDecimal price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    /**
     * @return id of the good.
     */
    public Long getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the good to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(Long id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return title of the good.
     */
    public String getTitle() {
        return title;
    }
     
    /**
     * 
     * @param title
     *            title of the good to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }
     
    /**
     * @return title of the item.
     */
    public BigDecimal getPrice() {
        return price;
    }
     
    /**
     * 
     * @param price
     *            price of the item to set.
     * @throws IllegalArgumentException
     *             if if price <= 0.
     */
    public void setPrice(BigDecimal price) {
        if (price.compareTo(BigDecimal.ZERO) == 1) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price should be > 0");
        }
    }
    
    /**
     * @return hashCode of the object of the {@link Good} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode()) 
                + ((price == null) ? 0 : price.hashCode())
                + ((title == null) ? 0 : title.hashCode()));
    }
    
    /**
     * Method used to compare this good to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Good other = (Good) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(title, other.getTitle())) {
            return false;
        }
        if (!Objects.equals(price, other.getPrice())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link Good} class .
     */
    @Override
    public String toString() {
        return "Good [id=" + id + ", title=" + title + ", price=" + price + "]";
    }
}