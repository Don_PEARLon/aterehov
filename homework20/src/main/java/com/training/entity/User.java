package com.training.entity;

import com.training.entity.role.Role;

import java.util.Objects;

/**
 * Class represents a user of the online shop.
 *
 * @author Alexandr_Terehov
 */
public class User {
    private Long id;
    private String name;
    private String password;
    private Role role;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the user.
     * @param name
     *            name of the user.
     */
    public User(final Long id, final String name, final String password, final Role role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
    }

    /**
     * @return id of the user.
     */
    public Long getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the user to set.
     * @throws IllegalArgumentException
     *             if id <0.
     */
    public void setId(Long id) {
        if (id > 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return name of the user.
     */
    public String getName() {
        return name;
    }
      
    /**
     * 
     * @param name
     *            name of the user to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return password of the user.
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * 
     * @param password
     *            password of the user to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * @return role of the user.
     */
    public Role getRole() {
        return role;
    }
    
    /**
     * 
     * @param role
     *            role of the user to set.
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return hashCode of the object of the {@link User} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * ((id == null) ? 0 : id.hashCode())
                + ((name == null) ? 0 : name.hashCode())
                + ((password == null) ? 0 : password.hashCode())
                + ((role == null) ? 0 : role.hashCode()));
    }
    
    /**
     * Method used to compare this user to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (!Objects.equals(id, other.getId())) {
            return false;
        }
        if (!Objects.equals(name, other.getName())) {
            return false;
        }
        if (!Objects.equals(password, other.getPassword())) {
            return false;
        }
        if (role != other.getRole()) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link User} class .
     */
    @Override
    public String toString() {
        return "User [id=" + id + ", name=" 
                + name + ", password=" + "*****" + ", role=" + role + "]";
    }
}