package com.training.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Defines an exception that can be thrown while authentication process when
 * user's credentials are incorrect.
 *
 * @author Alexandr_Terehov
 */
public class ShopAuthenticationException extends AuthenticationException {

    private static final long serialVersionUID = -3813632887026715723L;

    private final String message;

    public ShopAuthenticationException(String message) {
        super(message);
        this.message = message;
    }
    
    public ShopAuthenticationException() {
        super("Authentication failed");
        this.message = "Authentication failed";
    }

    @Override
    public String getMessage() {
        return message;
    }
}