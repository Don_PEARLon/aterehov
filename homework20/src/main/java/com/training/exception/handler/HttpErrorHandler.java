package com.training.exception.handler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Application handler for HTTP exceptions.  
 *
 * @author Alexandr_Terehov
 */
@Controller
public class HttpErrorHandler {
       
    @RequestMapping(value = "/404",  method = RequestMethod.GET)
    public String error404() {
        return "404";
    }
    
    @RequestMapping(value = "/405",  method = RequestMethod.GET)
    public String error405() {
        return "405";
    }
}