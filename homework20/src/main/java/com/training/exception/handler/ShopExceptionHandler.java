package com.training.exception.handler;

import com.training.exception.GoodNotFoundExc;
import com.training.exception.OrderNotFoundExc;
import com.training.exception.UserNotFoundExc;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * Application exception handler.  
 *
 * @author Alexandr_Terehov
 */
@ControllerAdvice
public class ShopExceptionHandler {
    
    /**
     * 
     * @param exception
     *            instance of the {@link UserNotFoundExc}.
     * @return {@link ModelAndView} of the error page.
     */
    @ExceptionHandler(UserNotFoundExc.class)
    public ModelAndView handleUserNotFoundExc(final UserNotFoundExc exception) {
        final ModelAndView content = new ModelAndView("error");
        content.addObject("errorMessage", exception.getMessage());
        return content;
    }
    
    /**
     * 
     * @param exception
     *            instance of the {@link OrderNotFoundExc}.
     * @return {@link ModelAndView} of the error page.
     */
    @ExceptionHandler(OrderNotFoundExc.class)
    public ModelAndView handleOrderNotFoundExc(final OrderNotFoundExc exception) {
        final ModelAndView content = new ModelAndView("error");
        content.addObject("errorMessage", exception.getMessage());
        return content;
    }
    
    /**
     * 
     * @param exception
     *            instance of the {@link GoodNotFoundExc}.
     * @return {@link ModelAndView} of the error page.
     */
    @ExceptionHandler(GoodNotFoundExc.class)
    public ModelAndView handleGoodNotFoundExc(final GoodNotFoundExc exception) {
        final ModelAndView content = new ModelAndView("error");
        content.addObject("errorMessage", exception.getMessage());
        return content;
    }
}