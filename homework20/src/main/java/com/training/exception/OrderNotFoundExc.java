package com.training.exception;

/**
 * Defines an exception that can be thrown when requested order was not found in
 * database.
 *
 * @author Alexandr_Terehov
 */
public class OrderNotFoundExc extends RuntimeException {

    private static final long serialVersionUID = -4198852339587049133L;

    private final String message;

    public OrderNotFoundExc(String message) {
        this.message = message;
    }

    public OrderNotFoundExc() {
        this.message = "Order not found";
    }

    @Override
    public String getMessage() {
        return message;
    }
}