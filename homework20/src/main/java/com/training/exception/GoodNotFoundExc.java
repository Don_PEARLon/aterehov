package com.training.exception;

/**
 * Defines an exception that can be thrown when requested good was not found in
 * database.
 *
 * @author Alexandr_Terehov
 */
public class GoodNotFoundExc extends RuntimeException {
    private static final long serialVersionUID = 8276396787919764523L;

    private final String message;

    public GoodNotFoundExc(String message) {
        this.message = message;
    }

    public GoodNotFoundExc() {
        this.message = "Good not found";
    }

    @Override
    public String getMessage() {
        return message;
    }
}