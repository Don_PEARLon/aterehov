package com.training;

import com.training.converter.GoodConverter;
import com.training.dao.GoodDao;
import com.training.dto.GoodDto;
import com.training.entity.Good;
import com.training.exception.GoodNotFoundExc;
import com.training.service.impl.GoodServiceImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the GoodServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class GoodServiceImplTest {
	@Mock
	private GoodDao goodDao;
	
	private GoodServiceImpl goodServiceImpl;
	
	private GoodConverter goodConverter; 
	
	private List<Good> goodList;
	private Good good1;
	private Good good2;
	private Good good3;
	
	private List<GoodDto> goodListDto;
	private GoodDto goodDto1;
	private GoodDto goodDto2;
	private GoodDto goodDto3;
	
	@Before
	public void initObjects() {
		goodConverter = new GoodConverter();
		goodServiceImpl = new GoodServiceImpl(goodDao, goodConverter);
		
		goodList = new ArrayList<>();
		goodListDto = new ArrayList<>();
		
		good1 = new Good(1L, "good1", new BigDecimal(100));
		good2 = new Good(2L, "good1", new BigDecimal(200));
		good3 = new Good(3L, "good3" , new BigDecimal(300));
		
		goodDto1 = new GoodDto(1L, "good1", new BigDecimal(100));
		goodDto2 = new GoodDto(2L, "good1", new BigDecimal(200));
		goodDto3 = new GoodDto(3L, "good3" , new BigDecimal(300));
		
		goodList.add(good1);
		goodList.add(good2);
		goodList.add(good3);
		
		goodListDto.add(goodDto1);
		goodListDto.add(goodDto2);
		goodListDto.add(goodDto3);
		
	}
	
	@Test
	public void testGetGoodsList() {
		//given:
        Mockito.when(goodDao.getGoodsList()).thenReturn(goodList);
       
        //when
        List<GoodDto> expectedGoodDtotList = goodServiceImpl.getGoodsList();
        
        //then
        assertEquals(expectedGoodDtotList, goodListDto);
        Mockito.verify(goodDao).getGoodsList();
	}
	
	@Test
	public void testGetGoodById() {
		Long goodId = 1L;
		
		//given:
        Mockito.when(goodDao.getGoodById(goodId)).thenReturn(good1);
        
        //when
        GoodDto expectedGoodDto = goodServiceImpl.getGoodById(goodId);
        
        //then
        assertEquals(expectedGoodDto, goodDto1);
        Mockito.verify(goodDao).getGoodById(goodId);
	}
	
	@Test(expected = GoodNotFoundExc.class)
	public void testGetGoodByIdNegative() {
		Long goodId = 4L;
		
		//given:
        Mockito.when(goodDao.getGoodById(goodId)).thenReturn(null);
        
        //when
        goodServiceImpl.getGoodById(goodId);
        
        //then
        Mockito.verify(goodDao).getGoodById(goodId);
	}
}
