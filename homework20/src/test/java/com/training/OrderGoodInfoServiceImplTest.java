package com.training;

import com.training.converter.OrderGoodInfoConverter;
import com.training.dao.OrderGoodInfoDao;
import com.training.dto.OrderGoodInfoDto;
import com.training.entity.OrderGoodInfo;
import com.training.service.impl.OrderGoodInfoServiceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the OrderGoodInfoServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderGoodInfoServiceImplTest {
	@Mock
	private OrderGoodInfoDao orderGoodInfoDao;
	
	OrderGoodInfoConverter orderGoodInfoConverter;
	
	private OrderGoodInfoServiceImpl orderGoodInfoServiceImpl;
	
	private List<OrderGoodInfo> orderGoodInfoList;
	private OrderGoodInfo orderGoodInfo1;
	private OrderGoodInfo orderGoodInfo2;
	private OrderGoodInfo orderGoodInfo3;
	
	private List<OrderGoodInfoDto> orderGoodInfoListDto;
	private OrderGoodInfoDto orderGoodInfoDto1;
	private OrderGoodInfoDto orderGoodInfoDto2;
	private OrderGoodInfoDto orderGoodInfoDto3;
	
	@Before
	public void initObjects() {
		orderGoodInfoConverter = new OrderGoodInfoConverter();
		orderGoodInfoServiceImpl = new OrderGoodInfoServiceImpl(orderGoodInfoDao, orderGoodInfoConverter); 
		
		orderGoodInfoList = new ArrayList<>();
		orderGoodInfoListDto = new ArrayList<>();
		
		orderGoodInfo1 = new OrderGoodInfo(1L, 1L, 1L);
		orderGoodInfo2 = new OrderGoodInfo(2L, 1L, 2L);
		orderGoodInfo3 = new OrderGoodInfo(3L, 1L, 3L);
				
		orderGoodInfoList.add(orderGoodInfo1);
		orderGoodInfoList.add(orderGoodInfo2);
		orderGoodInfoList.add(orderGoodInfo3);
		
		orderGoodInfoDto1 = new OrderGoodInfoDto(1L, 1L, 1L);
		orderGoodInfoDto2 = new OrderGoodInfoDto(2L, 1L, 2L);
		orderGoodInfoDto3 = new OrderGoodInfoDto(3L, 1L, 3L);
				
		orderGoodInfoListDto.add(orderGoodInfoDto1);
		orderGoodInfoListDto.add(orderGoodInfoDto2);
		orderGoodInfoListDto.add(orderGoodInfoDto3);
	}
	
	@Test
	public void testGetOrderGoodInfoList() {
		
		//given:
		Mockito.when(orderGoodInfoDao.getOrderGoodInfoList()).thenReturn(orderGoodInfoList);
		
		//when:
		List<OrderGoodInfoDto> expectedList= orderGoodInfoServiceImpl.getOrderGoodInfoList();
		
		//then:
		assertEquals(expectedList, orderGoodInfoListDto);
		Mockito.verify(orderGoodInfoDao).getOrderGoodInfoList();
	}
	
	@Test
	public void testGenerateNewOrderId() {
		Long expectedId = 4L;
		
		//given:
		Mockito.when(orderGoodInfoDao.getOrderGoodInfoList()).thenReturn(orderGoodInfoList);
		
		//when:
		Long newOrderGoodId = orderGoodInfoServiceImpl.generateNewOrderGoodId();
		
		//then:
		assertEquals(expectedId, newOrderGoodId, 0);
		Mockito.verify(orderGoodInfoDao).getOrderGoodInfoList();
	}
	
	@Test
	public void testGenerateNewOrderFirstId() {
		Long expectedId = 1L;
		
		//given:
		List<OrderGoodInfo> newOrderGoodInfoList = new ArrayList<>();
		Mockito.when(orderGoodInfoDao.getOrderGoodInfoList()).thenReturn(newOrderGoodInfoList);
		
		//when:
		Long newOrderGoodId = orderGoodInfoServiceImpl.generateNewOrderGoodId();
		
		//then:
		assertEquals(expectedId, newOrderGoodId, 0);
		Mockito.verify(orderGoodInfoDao).getOrderGoodInfoList();
	}
}
