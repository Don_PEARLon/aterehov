package com.training;

import com.training.converter.OrderConverter;
import com.training.dao.OrderDao;
import com.training.dto.OrderDto;
import com.training.entity.Order;
import com.training.exception.OrderNotFoundExc;
import com.training.service.impl.OrderServiceImpl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the OrderServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
	@Mock
	private OrderDao orderDao;
		
	private OrderServiceImpl orderServiceImpl;
	
	private OrderConverter orderConverter; 
	
	private List<Order> orderList;
	private Order order1;
	private Order order2;
	private Order order3;
	
	private List<OrderDto> orderListDto;
	private OrderDto orderDto1;
	private OrderDto orderDto2;
	private OrderDto orderDto3;
	
	@Before
	public void initObjects() {
		orderConverter = new OrderConverter();
		orderServiceImpl = new OrderServiceImpl(orderDao, orderConverter);
		
		orderList = new ArrayList<>();
		orderListDto = new ArrayList<>();
		
		order1 = new Order(1L, 1L, new BigDecimal(100));
		order2 = new Order(2L, 1L, new BigDecimal(200));
		order3 = new Order(3L, 2L , new BigDecimal(300));
		
		orderDto1 = new OrderDto(1L, 1L, new BigDecimal(100));
		orderDto2 = new OrderDto(2L, 1L, new BigDecimal(200));
		orderDto3 = new OrderDto(3L, 2L, new BigDecimal(300));
		
		orderList.add(order1);
		orderList.add(order2);
		orderList.add(order3);
		
		orderListDto.add(orderDto1);
		orderListDto.add(orderDto2);
		orderListDto.add(orderDto3);
		
	}
	
	@Test
	public void testGetOrderList() {
		//given:
        Mockito.when(orderDao.getOrderList()).thenReturn(orderList);
       
        //when
        List<OrderDto> expectedOrderDtoList = orderServiceImpl.getOrderList();
        
        //then
        assertEquals(expectedOrderDtoList, orderListDto);
        Mockito.verify(orderDao).getOrderList();
	}
	
	@Test
	public void testGetOrderById() {
		//given:
		Long orderId = 1L;
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(order1);
        
        //when
        OrderDto expectedOrderDto = orderServiceImpl.getOrderById(orderId);
        
        //then
        assertEquals(expectedOrderDto, orderDto1);
        Mockito.verify(orderDao).getOrderById(orderId);
	}
	
	@Test(expected = OrderNotFoundExc.class)
	public void testGetOrderByIdNegative() {
		//given:
		Long orderId = 4L; 
        Mockito.when(orderDao.getOrderById(orderId)).thenReturn(null);
        
        //when
        orderServiceImpl.getOrderById(orderId);
        
        //then
        Mockito.verify(orderDao).getOrderById(orderId);
	}
	
	@Test
	public void testGenerateNewOrderId() {
		Long expextedOrderId = 4L;
		
		//given:
		Mockito.when(orderDao.getOrderList()).thenReturn(orderList);
		
		//when:
		Long newOrderId = orderServiceImpl.generateNewOrderId();
		
		//then:
		assertEquals(expextedOrderId, newOrderId, 0);
		Mockito.verify(orderDao).getOrderList();
	}
	
	@Test
	public void testGenerateNewOrderFirstId() {
		Long expextedOrderId = 1L;
		
		//given:
		List<Order> newOrderList = new ArrayList<>();
		Mockito.when(orderDao.getOrderList()).thenReturn(newOrderList);
		
		//when
		Long newOrderId = orderServiceImpl.generateNewOrderId();
		
		//then
		assertEquals(expextedOrderId, newOrderId, 0);
		Mockito.verify(orderDao).getOrderList();
	}
}
