package com.training;

import com.training.converter.UserConverter;
import com.training.dao.UserDao;
import com.training.dto.UserDto;
import com.training.entity.User;
import com.training.entity.role.Role;
import com.training.exception.UserNotFoundExc;
import com.training.service.impl.UserServiceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the UserServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    private UserDao userDao;
    
    private UserServiceImpl userServiceImpl;
    
    private UserConverter userConverter;
    
    private List<User> userList;
    private List<UserDto> userListDto;
    
    private User user1;
    private User user2;
    private User user3;
    
    private UserDto userDto1;
    private UserDto userDto2;
    private UserDto userDto3;
    
    @Before
	public void initObjects() {
    	userConverter = new UserConverter(); 
    	
    	userServiceImpl = new UserServiceImpl(userDao, userConverter);
    	
    	userList = new ArrayList<>();
    	userListDto = new ArrayList<>();
    	
    	user1 = new User(1L, "user1", "pass1", Role.USER);
    	user2 = new User(2L, "user2", "pass2", Role.USER);
    	user3 = new User(3L, "user3", "pass3", Role.USER);
    	
    	userDto1 = new UserDto(1L, "user1", "pass1", Role.USER);
    	userDto2 = new UserDto(2L, "user2", "pass2", Role.USER);
    	userDto3 = new UserDto(3L, "user3", "pass3", Role.USER);
    	
    	userList.add(user1);
    	userList.add(user2);
    	userList.add(user3);
    	
    	userListDto.add(userDto1);
    	userListDto.add(userDto2);
    	userListDto.add(userDto3);
    	
    }
	
	@Test
	public void testGetUserList() {
		
		//given:
        Mockito.when(userDao.getUserList()).thenReturn(userList);
        
        //when
        List<UserDto> expectedUserDtoList = userServiceImpl.getUserList();
        
        //then
        assertEquals(expectedUserDtoList, userListDto);
        Mockito.verify(userDao).getUserList();
    }
	
	@Test
	public void testGetUserById() {
		//given:
		Long userId = 1L;
        Mockito.when(userDao.getUserById(userId)).thenReturn(user1);
        
        //when
        UserDto expectedUserDto= userServiceImpl.getUserById(userId);
        
        //then
        assertEquals(expectedUserDto, userDto1);
        Mockito.verify(userDao).getUserById(userId);
	}
	
	@Test(expected = UserNotFoundExc.class)
	public void testGetUserByIdNegative() {
		//given:
		Long userId = 4L;
        Mockito.when(userDao.getUserById(userId)).thenReturn(null);
        
        //when
        userServiceImpl.getUserById(userId);
        
        //then
        Mockito.verify(userDao).getUserById(userId);
	}
	
	@Test
	public void testGetUserByLogin() {
		//given:
		String userLogin = "user1";
        Mockito.when(userDao.getUserByLogin(userLogin)).thenReturn(user1);
        
        //when
        UserDto expectedUserDto = userServiceImpl.getUserByLogin(userLogin);
        
        //then
        assertEquals(expectedUserDto, userDto1);
        Mockito.verify(userDao).getUserByLogin(userLogin);
	}
	
	@Test(expected = UserNotFoundExc.class)
	public void testGetUserByLoginNegative() {
		//given:
		String userLogin = "user4";
        Mockito.when(userDao.getUserByLogin(userLogin)).thenReturn(null);
        
        //when
        userServiceImpl.getUserByLogin(userLogin);
        
        //then
        Mockito.verify(userDao).getUserByLogin(userLogin);
	}
}
