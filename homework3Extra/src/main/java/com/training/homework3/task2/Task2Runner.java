package com.training.homework3.task2;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task2Runner {
	/**
	 * Application start point.
	 *
	 * @param args
	 *            Command line arguments.
	 */
	public static void main(String[] args) {
		int[] array1 = { 1, 6, 2, 8, 7, 2 };// 4
		double[] array2 = { 1, 0.5, 0.5, 0.5, 0.5, 0.55, 0.5, 0.5 };
		System.out.println(Median.median(array1));
		System.out.println(Median.median(array2));

	}
}
