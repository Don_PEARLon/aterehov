package com.training.homework3.task2;

/**
 * Utility class used to calculate the median of the array.
 *
 * The essence of this approach is to create an additional array to store
 * quantity of elements which each element of the initial array is bigger or
 * less. We are using parameter quantityElementMedian ((array.length - 2) / 2
 * for even numbers and (array.length - 1) / 2 for odd numbers). Then we can
 * find the indexes of elementMidLess and elementMidBigger and use a formula
 * (elementMidLess + elementMidBigger) / 2 for median calculation. Also this
 * approach solves the collision situation when initial array has a few similar
 * elements.
 * 
 * @author Alexandr_Terehov
 */
public final class Median {

    /**
     * Method used to calculate the median of the array of integers.
     *
     * @param array
     *            array of integer values.
     * @return median of the array.
     */
    public static float median(int[] array) {
        int quantityElementMedian;
        if ((array.length % 2) == 0) {
            quantityElementMedian = (array.length - 2) / 2;
        } else {
            quantityElementMedian = (array.length - 1) / 2;
        }
        float result = (float) (getElementMidLess(array, quantityElementMedian)
                + getElementMidBigger(array, quantityElementMedian)) / 2;
        return result;
    }

    /**
     * Method used to calculate the median of the array of doubles.
     *
     * @param array
     *            array of integer values.
     * @return median of the array.
     */
    public static double median(double[] array) {
        int quantityMedian;
        if ((array.length % 2) == 0) {
            quantityMedian = (array.length - 2) / 2;
        } else {
            quantityMedian = (array.length - 1) / 2;
        }
        double result = (getElementMidLess(array, quantityMedian) + getElementMidBigger(array, quantityMedian)) / 2;
        return result;
    }

    /**
     * Method used to find one of the elements for median calculation in the
     * unsorted array of integers.
     *
     * @param array
     *            array of integer values.
     * @param quantityLess
     *            quantity of elements less than required element.
     * @return median of the array.
     */
    private static int getElementMidLess(int[] array, int quantityLess) {
        int elementMidLess = 0;
        int[] quantityArr = new int[array.length];
        int elementLess;
        for (int i = 0; i < array.length; i++) {
            elementLess = 0;
            for (int j = 0; j < array.length; j++) {
                if ((i != j) && (array[i] > array[j])) {
                    elementLess++;
                }
            }
            // Solving collision situation which may happen if initial array has a few
            // similar elements
            for (int j = 0; j < array.length; j++) {
                if ((i != j) && (array[i] == array[j]) && (elementLess < quantityLess)) {
                    elementLess++;
                }
            }
            quantityArr[i] = elementLess;
        }
        for (int i = 0; i < array.length; i++) {
            if (quantityArr[i] == quantityLess) {
                elementMidLess = i;
            }
        }
        return array[elementMidLess];
    }

    /**
     * Method used to find one of the elements for median calculation in the
     * unsorted array of doubles.
     *
     * @param array
     *            array of integer values.
     * @param quantityLess
     *            quantity of elements less than required element.
     * @return median of the array.
     */
    private static double getElementMidLess(double[] array, int quantityLess) {
        int elementMidLess = 0;
		int[] quantityArr = new int[array.length];
		int elementLess;
		for (int i = 0; i < array.length; i++) {
			elementLess = 0;
			for (int j = 0; j < array.length; j++) {
				if ((i != j) && (array[i] > array[j])) {
					elementLess++;
				}
			}
			// Solving collision situation which may happen if initial array has a few
			// similar elements
			for (int j = 0; j < array.length; j++) {
				if ((i != j) && (array[i] == array[j]) && (elementLess < quantityLess)) {
					elementLess++;
				}
			}
			quantityArr[i] = elementLess;
		}
		for (int i = 0; i < array.length; i++) {
			if (quantityArr[i] == quantityLess) {
				elementMidLess = i;
			}
		}
		return array[elementMidLess];
	}

	/**
	 * Method used to find one of the elements for median calculation in the
	 * unsorted array of integers.
	 *
	 * @param array
	 *            array of integer values.
	 * @param quantityLess
	 *            quantity of elements bigger than required element.
	 * @return median of the array.
	 */
	private static int getElementMidBigger(int[] array, int quantityBigger) {
		int elementMidBigger = 0;
		int[] quantityArr = new int[array.length];
		int elementBigger;
		for (int i = 0; i < array.length; i++) {
			elementBigger = 0;
			for (int j = 0; j < array.length; j++) {
				if ((i != j) && (array[i] < array[j])) {
					elementBigger++;
				}
			}
			// Solving collision situation which may happen if initial array has a few
			// similar elements
			for (int j = 0; j < array.length; j++) {
				if ((i != j) && (array[i] == array[j]) && (elementBigger < 3)) {
					elementBigger++;
				}
			}
			quantityArr[i] = elementBigger;
		}
		for (int i = 0; i < array.length; i++) {
			if (quantityArr[i] == quantityBigger) {
				elementMidBigger = i;
			}
		}
		return array[elementMidBigger];
	}

	/**
	 * Method used to find one of the elements for median calculation in the
	 * unsorted array of doubles.
	 *
	 * @param array
	 *            array of integer values.
	 * @param quantityLess
	 *            quantity of elements bigger than required element.
	 * @return median of the array.
	 */
	private static double getElementMidBigger(double[] array, int quantityBigger) {
		int elementMidBigger = 0;
		int[] quantityArr = new int[array.length];
		int elementBigger;
		for (int i = 0; i < array.length; i++) {
			elementBigger = 0;
			for (int j = 0; j < array.length; j++) {
				if ((i != j) && (array[i] < array[j])) {
					elementBigger++;
				}
			}
			// Solving collision situation which may happen if initial array has a few
			// similar elements
			for (int j = 0; j < array.length; j++) {
				if ((i != j) && (array[i] == array[j]) && (elementBigger < 3)) {
					elementBigger++;
				}
			}
			quantityArr[i] = elementBigger;
		}
		for (int i = 0; i < array.length; i++) {
			if (quantityArr[i] == quantityBigger) {
				elementMidBigger = i;
			}
		}
		return array[elementMidBigger];
	}
}
