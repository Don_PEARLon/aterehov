package com.training.homework5.task1;

import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import com.training.homework5.task1.entity.ComponentFileSystem;
import com.training.homework5.task1.entity.File;

/**
 * Test suite for DirectoryBuilder class.
 *
 * @author Alexandr_Terehov
 */
public class DirectoryBuilderTest {
	private static DirectoryBuilder directoryBuilder;

	@Before
	public void initObjects() {
		directoryBuilder = new DirectoryBuilder();
	}

	@Test
	public void buildNewTest() {
		String path = ("root/folder1/file1.txt");
		Directory dir = directoryBuilder.buildNew(path);
		ComponentFileSystem file1 = new File("file1", "txt");
		ComponentFileSystem folder1 = dir.getRoot().getComponentByName("folder1");
		ComponentFileSystem file2 = folder1.getComponentByName("file1");
		boolean condition = file1.equals(file2);
		assertTrue(condition);
	}

	@Test
	public void buildTest() {
		String path1 = ("root/folder1/file1.txt");
		String path2 = ("root/folder1/folder2/file2.txt");
		Directory dir = directoryBuilder.buildNew(path1);
		directoryBuilder.build(dir, path2);
		ComponentFileSystem file1 = new File("file2", "txt");
		ComponentFileSystem folder1 = dir.getRoot().getComponentByName("folder1");
		ComponentFileSystem folder2 = folder1.getComponentByName("folder2");
		ComponentFileSystem file2 = folder2.getComponentByName("file2");
		boolean condition = file1.equals(file2);
		assertTrue(condition);
	}

	@Test
	public void buildCmpFsInstance() {
		ComponentFileSystem file1 = new File("file1", "txt");
		ComponentFileSystem file2 = directoryBuilder.buildCmpFsInstance("file1.txt");
		boolean condition = ((file2 instanceof File) && (file1.equals(file2)));
		assertTrue(condition);
	}
}
