package com.training.homework5.task1;

import java.util.List;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;

import com.training.homework5.task1.entity.ComponentFileSystem;
import com.training.homework5.task1.entity.File;
import com.training.homework5.task1.entity.Folder;

/**
 * Test suite for Folder class.
 *
 * @author Alexandr_Terehov
 */
public class FolderTest {
	private static ComponentFileSystem folder1;
	private static ComponentFileSystem folder2;

	@Before
	public void initObjects() {
		folder1 = new Folder("folder1");
		folder2 = new Folder("folder2");
	}

	@Test
	public void testGetName() {
		String name = "folder1";
		boolean condition = name.equals(folder1.getName());
		assertTrue(condition);
	}

	@Test
	public void testSetName() {
		String name = "folder2";
		folder1.setName("folder2");
		boolean condition = name.equals(folder1.getName());
		assertTrue(condition);
	}

	@Test
	public void testAdd() {
		boolean condition = folder1.add(folder2);
		assertTrue(condition);
	}

	@Test
	public void testAddNegative() {
		ComponentFileSystem folder3 = new Folder("folder2");
		folder1.add(folder2);
		boolean condition = folder1.add(folder3);
		assertFalse(condition);
	}

	@Test
	public void testContainsComponent() {
		folder1.add(folder2);
		boolean condition = folder1.containsComponent("folder2");
		assertTrue(condition);
	}

	@Test
	public void testContainsComponentNegative() {
		folder1.add(folder2);
		boolean condition = folder1.containsComponent("folder3");
		assertFalse(condition);
	}

	@Test
	public void testGetComponentByName() {
		folder1.add(folder2);
		ComponentFileSystem folder = folder1.getComponentByName("folder2");
		assertTrue(folder == folder2);
	}

	@Test
	public void testEquals() {
		ComponentFileSystem folder3 = new Folder("folder2");
		boolean condition = folder2.equals(folder3);
		assertTrue(condition);
	}

	@Test
	public void testGetComponents() {
		ComponentFileSystem file1 = new File("file1", "txt");
		folder1.add(file1);
		List<ComponentFileSystem> components = folder1.getComponents();
		boolean condition = components.contains(file1);
		assertTrue(condition);
	}
}
