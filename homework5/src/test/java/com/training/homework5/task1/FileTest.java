package com.training.homework5.task1;

import static org.junit.Assert.assertTrue;

import java.util.List;

import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;

import com.training.homework5.task1.entity.ComponentFileSystem;
import com.training.homework5.task1.entity.File;
import com.training.homework5.task1.entity.Folder;

/**
 * Test suite for File class.
 *
 * @author Alexandr_Terehov
 */
public class FileTest {
	private static ComponentFileSystem file1;

	@Before
	public void initObjects() {
		file1 = new File("file1", "txt");
	}

	@Test
	public void testGetName() {
		String name = "file1";
		boolean condition = name.equals(file1.getName());
		assertTrue(condition);
	}

	@Test
	public void testSetName() {
		String name = "file2";
		file1.setName("file2");
		boolean condition = name.equals(file1.getName());
		assertTrue(condition);
	}

	@Test
	public void testAdd() {
		boolean condition = file1.add(new Folder("folder1"));
		assertFalse(condition);
	}

	@Test
	public void testContainsComponent() {
		boolean condition = file1.containsComponent("folder2");
		assertFalse(condition);
	}

	@Test
	public void testGetComponentByName() {
		ComponentFileSystem folder = file1.getComponentByName("folder2");
		assertTrue(folder == null);
	}

	@Test
	public void testEquals() {
		ComponentFileSystem file2 = new File("file1", "txt");
		boolean condition = file1.equals(file2);
		assertTrue(condition);
	}

	@Test
	public void testGetComponents() {
		ComponentFileSystem file2 = new File("file2", "txt");
		file1.add(file2);
		List<ComponentFileSystem> components = file1.getComponents();
		assertTrue(components == null);
	}
}
