package com.training.homework5.task1;

import com.training.homework5.task1.entity.ComponentFileSystem;
import com.training.homework5.task1.entity.File;
import com.training.homework5.task1.entity.Folder;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to build Directory object using String path.
 *
 * @author Alexandr_Terehov
 */
public class DirectoryBuilder {
    /**
     * Method used to build a new Directory object from the String path.
     *
     * @param path
     *            String representation of the file system component path.
     * @return object of the Directory class.
     */
    public Directory buildNew(String path) {
        List<ComponentFileSystem> componentsFs = new ArrayList<ComponentFileSystem>();
        String[] directoryStr = path.split("/");
        ComponentFileSystem root = buildCmpFsInstance(directoryStr[0]);
        Directory directory = new Directory(buildRoot(directoryStr, root));
        System.out.println("Path: " + path + " has been processed");
        return directory;
    }
       
    /**
     * Method used to rebuild an existing Directory object using the String path.
     *
     * @param directory
     *            object of the Directory class.
     * @param path
     *            String representation of the file system component path.
     */
    public void build(Directory directory, String path) {
        List<ComponentFileSystem> componentsFs = new ArrayList<ComponentFileSystem>();
        String[] directoryStr = path.split("/");
        ComponentFileSystem root = directory.getRoot();
        buildRoot(directoryStr, root);
        System.out.println("Path: " + path + " has been processed");
    }
         
    /**
     * Method used to build a root folder from the String array.
     *
     * @param directoryStr
     *            String array containing file names.
     * @return ComponentFileSystem object.
     */
    private ComponentFileSystem buildRoot(String[] directoryStr, ComponentFileSystem root) {
        ComponentFileSystem currentFolder = root;
        for (int i = 1; i < directoryStr.length; i++) {
            if (currentFolder.containsComponent(directoryStr[i])) {
                currentFolder = currentFolder.getComponentByName(directoryStr[i]);
            } else {
                ComponentFileSystem subFolder = buildCmpFsInstance(directoryStr[i]);
                currentFolder.add(subFolder);
                currentFolder = subFolder;
            }
        }
        return root;
    }
       
    /**
     * Method used to build a ComponentFileSystem object from the String.
     *
     * @param componentStr
     *            name of the File or Folder .
     * @return ComponentFileSystem object.
     */
    public ComponentFileSystem buildCmpFsInstance(String componentStr) {
        if (componentStr.contains(".")) {
            String[] subComponentStr = componentStr.split("\\.");
            return new File(subComponentStr[0], subComponentStr[1]);
        } else {
            return new Folder(componentStr);
        }
    }
}
