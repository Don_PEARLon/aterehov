package com.training.homework5.task1.entity;

import java.util.List;

/**
 * Interface used for representing components of file system.
 *
 * @author Alexandr_Terehov
 */
public interface ComponentFileSystem {
    /**
     * Method used to add file system component to an existing component.
     *
     * @param component
     *            instance of ComponentFileSystem.
     * @return boolean result of adding.
     */
    boolean add(ComponentFileSystem component);
 
    /**
     * @return name of the file system component.
     */
    String getName();
         
    /**
     * 
     * @param name
     *            file system components name to set.
     */
    void setName(String name);
        
    /**
     * @return list of the subcomponents of the current file system component.
     */
    List<ComponentFileSystem> getComponents();
          
    /**
     * Method used to search for subcomponent inside the current file system
     * component.
     *
     * @param componentName
     *            name name of the file system component.
     * @return boolean result of searching.
     */
    boolean containsComponent(String componentName);

    /**
     * Method used to return a subcomponent from the list of subcomponents of
     * current file system component.
     *
     * @param name
     *            name of the file system component.
     * @return ComponentFileSystem object.
     */
    ComponentFileSystem getComponentByName(String name);
}
