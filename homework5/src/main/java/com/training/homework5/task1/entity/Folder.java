package com.training.homework5.task1.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to represent a Folder.
 *
 * @author Alexandr_Terehov
 */
public class Folder implements ComponentFileSystem {
    private String name;
    List<ComponentFileSystem> components;
            
    /**
     * Constructor.
     *
     * @param name
     *            name of the Folder.
     */
    public Folder(String name) {
        this.name = name;
        components = new ArrayList<ComponentFileSystem>();
    }
          
    /**
     * @return name of the Folder.
     */
    public String getName() {
        return name;
    }
                   
    /**
     * 
     * @param name
     *            name of the Folder to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method used to add a file system component into the current Folder.
     *
     * @param component
     *            instance of ComponentFileSystem.
     * @return boolean result of adding.
     */
    public boolean add(ComponentFileSystem component) {
        boolean isPresent = this.containsComponent(component.getName());
        if (isPresent == false) {
            components.add(component);
            return true;
        } else {
            System.out.println(component.getName() + " is already exist in "
                    + this.getName() + " folder");
            return false;
        }
    }

    /**
     * Method used to search for subcomponent inside the current Folder.
     *
     * @param componentName
     *            name name of the file system component.
     * @return boolean result of searching.
     */
    public boolean containsComponent(String componentName) {
        boolean result = false;
        for (ComponentFileSystem subComponent : components) {
            if (subComponent.getName().equals(componentName)) {
                result = true;
            }
        }
        return result;
    }
          
    /**
     * Method used to return a subcomponent from the list of subcomponents of
     * current Folder.
     *
     * @param componentName
     *            name of the file system component.
     * @return ComponentFileSystem object.
     */
    public ComponentFileSystem getComponentByName(String componentName) {
        ComponentFileSystem result = null;
        for (ComponentFileSystem subComponent : components) {
            if (subComponent.getName().equals(componentName)) {
                result = subComponent;
            }
        }
        return result;
    }
        
    /**
     * Method used for a string representation of the Folder.
     *
     * @return string representation of the Folder.
     */
    @Override
    public String toString() {
        return name + "/";
    }
           
    /**
     * Method used to check the equality of the Folder objects.
     *
     * @param object
     *            object to compare.
     * @return boolean result of comparison.
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        ComponentFileSystem component = (ComponentFileSystem) object;
        if (!(this.getName().equals(component.getName()))) {
            return false;
        }
        return true;
    }
          
    /**
     * @return list of the subcomponents of the Folder.
     */
    public List<ComponentFileSystem> getComponents() {
        return components;
    }
}
