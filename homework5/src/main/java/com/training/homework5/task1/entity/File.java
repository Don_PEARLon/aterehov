package com.training.homework5.task1.entity;

import java.util.List;

/**
 * Class used to represent a File.
 *
 * @author Alexandr_Terehov
 */
public class File implements ComponentFileSystem {
    private String name;
    private String extension;
                   
    /**
     * Constructor.
     *
     * @param name
     *            name of the File.
     * @param extension
     *            extension of the file.
     */
    public File(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }
           
    /**
     * @return name of the File.
     */
    public String getName() {
        return name;
    }
              
    /**
     * 
     * @param name
     *            name of the File to set.
     */
    public void setName(String name) {
        this.name = name;
    }
       
    /**
     * @return extension of the File.
     */
    public String getExtension() {
        return extension;
    }
          
    /**
     * 
     * @param extension
     *            extension of the File to set.
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }
                     
    /**
     * Method used to add a subcomponent into the current ComponentFileSystem
     * object. This method always returns false, because Files can't contain any
     * subcomponents.
     *
     * @param component
     *            instance of ComponentFileSystem.
     * @return false.
     */
    public boolean add(ComponentFileSystem component) {
        System.out.println("You can't add component: " + component.getName() + " into file "
                + this.getName() + "." + this.extension);
        return false;
    }
                             
    /**
     * Method used to search for subcomponent inside the current file system
     * component. This method always returns false, because Files can't contain any
     * subcomponents.
     *
     * @param componentName
     *            name of the component.
     * @return false.
     */
    public boolean containsComponent(String componentName) {
        return false;
    }
       
    /**
     * Method used to return a subcomponent from the list of subcomponents. This
     * method always returns null, because Files can't contain any subcomponents.
     *
     * @param componentName
     *            name of the component.
     * @return null.
     */
    public ComponentFileSystem getComponentByName(String componentName) {
        return null;
    }
              
    /**
     * Method used for a string representation of the File.
     *
     * @return string representation of the File.
     */
    @Override
    public String toString() {
        return name + "." + extension;
    }
                             
    /**
     * Method used to check the equality of the Folder objects.
     *
     * @param object
     *            object to compare.
     * @return boolean result of comparison.
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        File file = (File) object;
        if (!((this.getName() + this.getExtension()).equals((file.getName()
                + file.getExtension())))) {
            return false;
        }
        return true;
    }
               
    /**
     * @return null, because File can't have any subcomponents.
     */
    public List<ComponentFileSystem> getComponents() {
        return null;
    }
}