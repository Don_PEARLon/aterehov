package com.training.homework5.task1;

import com.training.homework5.task1.entity.ComponentFileSystem;

/**
 * Class used for representing a file directory.
 *
 * @author Alexandr_Terehov
 */
public class Directory {
    private ComponentFileSystem root;
                  
    /**
     * Constructor.
     *
     * @param root
     *            root folder of the directory.
     */
    public Directory(ComponentFileSystem root) {
        this.root = root;
    }
                      
    /**
     * @return root folder of the directory.
     */
    public ComponentFileSystem getRoot() {
        return root;
    }
                                
    /**
     * 
     * @param root
     *            root folder of the directory to set.
     */
    public void setRoot(ComponentFileSystem root) {
        this.root = root;
    }
                                                  
    /**
     * Method prints the directory tree to the console.
     */
    public void print() {
        DirectoryPrinter.print(root);
    }
}
