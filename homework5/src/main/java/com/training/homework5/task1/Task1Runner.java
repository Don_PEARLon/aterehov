package com.training.homework5.task1;

import java.util.Scanner;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task1Runner {
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the root folder of the directory.");
        String path = String.valueOf(scanner.nextLine());
        DirectoryBuilder directoryBuilder = new DirectoryBuilder();
        Directory dir = directoryBuilder.buildNew(path);
        do {
            System.out.println("Enter next path in the directory or enter 'print'"
                    + " to print directory tree.");
            System.out.println("You can enter 'exit' if you want to close this application.");
            path = scanner.nextLine();
            if (path.equals("print")) {
                dir.print();
                System.out.println("Application has been closed.");
                break;
            }
            if (path.equals("exit")) {
                System.out.println("Application has been closed.");
                break;
            }
            directoryBuilder.build(dir, path);
        } while (true);
        scanner.close();
        /*String path1 = ("root/folder1/folder2/file1.txt");
        String path2 = ("root/folder2");
        String path3 = ("root/folder3/folder1/file3.txt");
        String path4 = ("root/folder2/file2.txt");
        DirectoryBuilder directoryBuilder = new DirectoryBuilder();
        Directory dir = directoryBuilder.buildNew(path1);
        directoryBuilder.build(dir, path2);
        directoryBuilder.build(dir, path3);
        directoryBuilder.build(dir, path4);
        dir.print();*/
    }
}
