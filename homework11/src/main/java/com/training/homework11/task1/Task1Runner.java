package com.training.homework11.task1;

import java.io.InvalidObjectException;
import java.util.Scanner;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class Task1Runner {
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     * @throws InvalidObjectException
     *             occurs if Directory class instance wasn't loaded.            
     */
    public static void main(String[] args) throws InvalidObjectException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the root folder or first path of the directory,");
        System.out.println("print 'load' to load directory state.");
        String path = String.valueOf(scanner.nextLine());
        DirectoryBuilder directoryBuilder = new DirectoryBuilder();
        Directory dir;
        if (path.equals("load")) {
            dir = directoryBuilder.loadDiretory();
        } else {
            dir = directoryBuilder.buildNew(path);
        }
        do {
            System.out.println("Enter next path in the directory or enter 'print'"
                    + " to print directory tree.");
            System.out.println("You can enter 'exit' if you want to close this application.");
            path = scanner.nextLine();
            if (path.equals("print")) {
                dir.print();
            } else if (path.equals("save")) {
                directoryBuilder.saveDiretoryState(dir);
            } else if (path.equals("load")) {
                dir = directoryBuilder.loadDiretory();
            } else if (path.equals("exit")) {
                System.out.println("Application has been closed.");
                break;
            } else {
                directoryBuilder.build(dir, path);
            }
            
        } while (true);
        scanner.close();
        /*String path1 = ("root/folder1/folder2/file1.txt");
        String path2 = ("root/folder2");
        String path3 = ("root/folder3/folder1/file3.txt");
        String path4 = ("root/folder2/file2.txt");
        DirectoryBuilder directoryBuilder = new DirectoryBuilder();
        Directory dir = directoryBuilder.buildNew(path1);
        directoryBuilder.build(dir, path2);
        directoryBuilder.build(dir, path3);
        directoryBuilder.build(dir, path4);
        dir.print();*/
    }
}
