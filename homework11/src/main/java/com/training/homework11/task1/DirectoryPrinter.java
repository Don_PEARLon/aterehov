package com.training.homework11.task1;

import com.training.homework11.task1.entity.ComponentFileSystem;

/**
 * Utility class used to output components tree of the ComponentFileSystem
 * object to the console.
 *
 * @author Alexandr_Terehov
 */
public final class DirectoryPrinter {
    private static final String separatorStr1 = ("    ");
    private static String separatorStr2 = ("    ");
                          
    /**
     * Method used to output components tree of the ComponentFileSystem object to
     * the console.
     *
     * @param component
     *            instance of ComponentFileSystem.
     */
    public static void print(ComponentFileSystem component) {
        System.out.println(component);
        if (component.getComponents() != null) {
            for (ComponentFileSystem subComponent : component.getComponents()) {
                System.out.print(separatorStr2);
                separatorStr2 = separatorStr2 + separatorStr1;
                print(subComponent);
            }
            separatorStr2 = separatorStr1;
        }
    }
}
