package com.training.homework11.task1.entity;

import com.training.homework11.task1.exception.ComponentFileSystemException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class used to represent a Folder.
 *
 * @author Alexandr_Terehov
 */
public class Folder implements ComponentFileSystem {
    private String name;
    List<ComponentFileSystem> components;
    private static final long serialVersionUID = 1L;
    
    /**
     * Constructor.
     *
     * @param name
     *            name of the Folder.
     * @throws ComponentFileSystemException
     *             if folder name is incorrect.
     */
    public Folder(String name) {
        if (checkName(name)) {
            this.name = name;
            components = new ArrayList<ComponentFileSystem>();
        } else {
            throw new ComponentFileSystemException("Incorrect Folder name");
        }
    }
          
    /**
     * @return name of the Folder.
     */
    public String getName() {
        return name;
    }
                   
    /**
     * 
     * @param name
     *            name of the Folder to set.
     * @throws ComponentFileSystemException
     *             if folder name is incorrect.            
     */
    public void setName(String name) {
        if (checkName(name)) {
            this.name = name;
        } else {
            throw new ComponentFileSystemException("Incorrect Folder name");
        }
    }

    /**
     * Method used to add a file system component into the current Folder.
     *
     * @param component
     *            instance of ComponentFileSystem.
     * @return boolean result of adding.
     */
    public boolean add(ComponentFileSystem component) {
        boolean isPresent = this.containsComponent(component.getName());
        if (isPresent == false) {
            components.add(component);
            return true;
        } else {
            System.out.println(component.getName() + " is already exist in "
                    + this.getName() + " folder");
            return false;
        }
    }

    /**
     * Method used to search for subcomponent inside the current Folder.
     *
     * @param componentName
     *            name name of the file system component.
     * @return boolean result of searching.
     */
    public boolean containsComponent(String componentName) {
        boolean result = false;
        for (ComponentFileSystem subComponent : components) {
            if (subComponent.getName().equals(componentName)) {
                result = true;
            }
        }
        return result;
    }
          
    /**
     * Method used to return a subcomponent from the list of subcomponents of
     * current Folder.
     *
     * @param componentName
     *            name of the file system component.
     * @return ComponentFileSystem object.
     */
    public ComponentFileSystem getComponentByName(String componentName) {
        ComponentFileSystem result = null;
        for (ComponentFileSystem subComponent : components) {
            if (subComponent.getName().equals(componentName)) {
                result = subComponent;
            }
        }
        return result;
    }
        
    /**
     * Method used for a string representation of the Folder.
     *
     * @return string representation of the Folder.
     */
    @Override
    public String toString() {
        return name + "/";
    }
           
    /**
     * Method used to check the equality of the Folder objects.
     *
     * @param object
     *            object to compare.
     * @return boolean result of comparison.
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        ComponentFileSystem component = (ComponentFileSystem) object;
        if (!(this.getName().equals(component.getName()))) {
            return false;
        }
        return true;
    }
          
    /**
     * @return list of the subcomponents of the Folder.
     */
    public List<ComponentFileSystem> getComponents() {
        return components;
    }
    
    /**
     * Method used to check Folder name.
     *
     * @param name
     *            name of the folder to check.
     * @return boolean result of checking.
     */
    private boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9!]*$");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }
}
