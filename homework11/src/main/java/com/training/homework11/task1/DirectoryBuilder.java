package com.training.homework11.task1;

import com.training.homework11.task1.entity.ComponentFileSystem;
import com.training.homework11.task1.entity.File;
import com.training.homework11.task1.entity.Folder;
import com.training.homework11.task1.exception.ComponentFileSystemException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.InvalidObjectException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * Class used to build Directory object using String path.
 *
 * @author Alexandr_Terehov
 */
public class DirectoryBuilder {
    /**
     * Method used to build a new Directory object from the String path.
     *
     * @param path
     *            String representation of the file system component path.
     * @return object of the Directory class.
     * @throws ComponentFileSystemException
     *             if path is incorrect.
     */
    public Directory buildNew(String path) {
        String[] directoryStr = path.split("/");
        ComponentFileSystem root = new Folder(directoryStr[0]);
        Directory directory = new Directory(buildRoot(directoryStr, root));
        System.out.println("Path: " + path + " has been processed");
        return directory;
    }
       
    /**
     * Method used to rebuild an existing Directory object using the String path.
     *
     * @param directory
     *            object of the Directory class.
     * @param path
     *            String representation of the file system component path.
     */
    public void build(Directory directory, String path) {
        String[] directoryStr = path.split("/");
        if (directoryStr[0].equals(directory.getRoot().getName())) {
            ComponentFileSystem root = directory.getRoot();
            buildRoot(directoryStr, root);
            System.out.println("Path: " + path + " has been processed");
        } else {
            throw new ComponentFileSystemException("Incorrect path.");
        }
    }
         
    /**
     * Method used to build a root folder from the String array.
     *
     * @param directoryStr
     *            String array containing file names.
     * @return ComponentFileSystem object.
     */
    private ComponentFileSystem buildRoot(String[] directoryStr, ComponentFileSystem root) {
        ComponentFileSystem currentFolder = root;
        for (int i = 1; i < directoryStr.length; i++) {
            if (currentFolder.containsComponent(directoryStr[i])) {
                currentFolder = currentFolder.getComponentByName(directoryStr[i]);
            } else {
                ComponentFileSystem subFolder = buildCmpFsInstance(directoryStr[i]);
                currentFolder.add(subFolder);
                currentFolder = subFolder;
            }
        }
        return root;
    }
       
    /**
     * Method used to build a ComponentFileSystem object from the String.
     *
     * @param componentStr
     *            name of the File or Folder.
     * @return ComponentFileSystem object.
     * @throws ComponentFileSystemException
     *             if name of the component FS is incorrect.
     */
    public ComponentFileSystem buildCmpFsInstance(String componentStr) {
        if (componentStr.contains(".")) {
            String[] subComponentStr = componentStr.split("\\.");
            return new File(subComponentStr[0], subComponentStr[1]);
        } else {
            return new Folder(componentStr);
        }
    }
    
    /**
     * Method used to save current directory state.
     * @param dir
     *            directory to save. 
     */
    public boolean saveDiretoryState(Directory dir) {
        boolean flag = false;
        ObjectOutputStream ostream = null;
        try {
            FileOutputStream fos = new FileOutputStream("root.out");
            if (fos != null) {
                ostream = new ObjectOutputStream(fos);
                ostream.writeObject(dir.getRoot());
                System.out.println("Current directory state has been saved.");
                flag = true;
            }
        } catch (FileNotFoundException exc) {
            System.err.println("File can't be created: " + exc);
        } catch (NotSerializableException exc) {
            System.err.println("Class cant'be serialized: " + exc);
        } catch (IOException exc) {
            System.err.println(exc);
        } finally {
            try {
                if (ostream != null) {
                    ostream.flush();
                    ostream.close();
                }
            } catch (IOException exc) {
                System.err.println("Error of closing stream: ");
            }
        }
        return flag;
    }
                
    /**
     * Method used to load directory state from disk.
     * @return instance of the Directory class.
     * @throws InvalidObjectException
     *             occurs if directory wasn't loaded.
     */
    public Directory loadDiretory() throws InvalidObjectException {
        ComponentFileSystem root = null;
        ObjectInputStream istream = null;
        try {
            FileInputStream fis = new FileInputStream("root.out");
            istream = new ObjectInputStream(fis);
            root = (ComponentFileSystem) istream.readObject();
            System.out.println("Directory state has been loaded.");
            Directory dir = new Directory(root);
            return dir;
        } catch (ClassNotFoundException exc) {
            System.err.println("Class doesn't exist: " + exc);
        } catch (FileNotFoundException exc) {
            System.err.println("File for serialization doesn't exist: " + exc);
        } catch (InvalidClassException ioe) {
            System.err.println("Class versions mismatch: " + ioe);
        } catch (IOException ioe) {
            System.err.println("Common I/O error: " + ioe);
        } finally {
            try {
                if (istream != null) {
                    istream.close();
                }
            } catch (IOException exc) {
                System.err.println("Error of closing stream: ");
            }
        }
        throw new InvalidObjectException("Object wasn't serialized");
    }
}
