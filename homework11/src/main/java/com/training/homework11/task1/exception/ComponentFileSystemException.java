package com.training.homework11.task1.exception;

/**
 * Class used to represent a custom exception that may occur while entering an
 * incorrect name of the file system component.
 *
 * @author Alexandr_Terehov
 */
public class ComponentFileSystemException extends RuntimeException {
    public ComponentFileSystemException(String message) {
        super(message);
    }
                   
    private static final long serialVersionUID = 1L;
}
