package com.training.exception;

public class GoodNotFoundExc extends RuntimeException {
    
    private static final long serialVersionUID = 8276396787919764523L;

    private final String message;

    public GoodNotFoundExc(String message) {
        this.message = message;
    }

    public GoodNotFoundExc() {
        this.message = "Good not found";
    }

    @Override
    public String getMessage() {
        return message;
    }
}