package com.training.exception;

public class UserNotFoundExc extends RuntimeException {

    private static final long serialVersionUID = -7714760289816277318L;

    private final String message;

    public UserNotFoundExc(String message) {
        this.message = message;
    }

    public UserNotFoundExc() {
        this.message = "User not found";
    }

    @Override
    public String getMessage() {
        return message;
    }
}