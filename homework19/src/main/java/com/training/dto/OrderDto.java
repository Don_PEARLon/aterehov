package com.training.dto;

/**
 * Class represents a DTO of an order in the online shop.
 *
 * @author Alexandr_Terehov
 */
public class OrderDto {
    private int id;
    private int userId;
    private double totalPrice;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the order.
     * @param userId
     *            id of the user.
     * @param totalPrice
     *            total price of the order.
     */
    public OrderDto(final int id, final int userId, final double totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }
    
    /**
     * @return id of the good.
     */
    public int getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the order to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(int id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return Id of the user who made the order.
     */
    public int getUserId() {
        return userId;
    }
    
    /**
     * 
     * @param userId
     *            id of the user who made the order.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setUserId(int userId) {
        if (userId > 0) {
            this.userId = userId;
        } else {
            throw new IllegalArgumentException(" User ID must be > 0");
        } 
    }
    
    /**
     * @return total price of the order.
     */
    public double getTotalPrice() {
        return totalPrice;
    }
    
    /**
     * 
     * @param totalPrice
     *            total price of the order.
     * @throws IllegalArgumentException
     *             if totalPrice < 0.
     */
    public void setTotalPrice(double totalPrice) {
        if (totalPrice > 0) {
            this.totalPrice = totalPrice;
        } else {
            throw new IllegalArgumentException("Total price can't be < 0");
        } 
    }
    
    /**
     * @return hashCode of the object of the {@link OrderDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * id + userId + totalPrice);
    }
    
    /**
     * Method used to compare this order to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderDto other = (OrderDto) obj;
        if (id != other.id) {
            return false;
        }
        if (totalPrice != other.totalPrice) {
            return false;
        }
        if (userId != other.userId) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link OrderDto} class .
     */
    @Override
    public String toString() {
        return "OrderDto [id=" + id + ", userId=" + userId + ", totalPrice=" + totalPrice + "]";
    }
}