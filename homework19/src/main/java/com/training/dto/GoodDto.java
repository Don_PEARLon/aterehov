package com.training.dto;

/**
 * Class represents an item of the online-shop.
 *
 * @author Alexandr_Terehov
 */
public class GoodDto {
    private int id;
    private String title;
    private double price;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the good.
     * @param title
     *            title of the good.
     * @param price
     *            price of the good.
     */    
    public GoodDto(int id, String title, double price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    /**
     * @return id of the good.
     */
    public int getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the good to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(int id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return title of the good.
     */
    public String getTitle() {
        return title;
    }
     
    /**
     * 
     * @param title
     *            title of the good to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }
     
    /**
     * @return title of the item.
     */
    public double getPrice() {
        return price;
    }
     
    /**
     * 
     * @param price
     *            price of the item to set.
     * @throws IllegalArgumentException
     *             if if price <= 0.
     */
    public void setPrice(double price) {
        if (price > 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price should be > 0");
        }
    }
    
    /**
     * @return hashCode of the object of the {@link GoodDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * id + price + ((title == null) ? 0 : title.hashCode()));
    }
    
    /**
     * Method used to compare this good to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GoodDto other = (GoodDto) obj;
        if (id != other.id) {
            return false;
        }
        if (!title.equals(other.getTitle())) {
            return false;
        }
        if (price != other.price) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link GoodDto} class .
     */
    @Override
    public String toString() {
        return "GoodDto [id=" + id + ", title=" + title + ", price=" + price + "]";
    }
}