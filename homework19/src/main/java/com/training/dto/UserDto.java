package com.training.dto;

/**
 * Class represents DTO of user in the online shop.
 *
 * @author Alexandr_Terehov
 */
public class UserDto {
    private int id;
    private String name;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the user.
     * @param name
     *            name of the user.
     */
    public UserDto(final int id, final String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return id of the user.
     */
    public int getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the user to set.
     * @throws IllegalArgumentException
     *             if id <0.
     */
    public void setId(int id) {
        if (id > 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return name of the user.
     */
    public String getName() {
        return name;
    }
      
    /**
     * 
     * @param name
     *            name of the user to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return hashCode of the object of the {@link UserDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * id + ((name == null) ? 0 : name.hashCode()));
    }
    
    /**
     * Method used to compare this userDto to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserDto other = (UserDto) obj;
        if (id != other.id) {
            return false;
        }
        if (!name.equals(other.getName())) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }
    
    /**
     * @return String representation of the object of the {@link UserDto} class .
     */
    @Override
    public String toString() {
        return "UserDto [id=" + id + ", name=" + name + "]";
    }
}
