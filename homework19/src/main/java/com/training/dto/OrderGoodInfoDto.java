package com.training.dto;

/**
 * Class represents an information about goods included in orders of the users
 * of the online shop.
 *
 * @author Alexandr_Terehov
 */
public class OrderGoodInfoDto {
    private int id;
    private int orderId;
    private int goodId;
    
    /**
     * Constructor.
     *
     * @param id
     *            id of the orderGoodInfo event.
     * @param orderId
     *            id of the order.
     * @param goodId
     *            id of the good.
     */
    public OrderGoodInfoDto(final int id, final int orderId, final int goodId) {
        this.id = id;
        this.orderId = orderId;
        this.goodId = goodId;
    }

    /**
     * @return id of the order-good info.
     */
    public int getId() {
        return id;
    }
   
    /**
     * 
     * @param id
     *            id of the order-good info to set.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setId(int id) {
        if (id > 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("ID must be > 0");
        }
    }

    /**
     * @return Id of the order.
     */
    public int getOrderId() {
        return orderId;
    }
    
    /**
     * 
     * @param orderId
     *            id of the order.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setOrderId(int orderId) {
        if (orderId > 0) {
            this.orderId = orderId;
        } else {
            throw new IllegalArgumentException(" User ID must be > 0");
        } 
    }
    
    /**
     * @return Id of the good.
     */
    public int getGoodId() {
        return goodId;
    }
    
    /**
     * 
     * @param goodId
     *            id of the good.
     * @throws IllegalArgumentException
     *             if id <= 0.
     */
    public void setGoodId(int goodId) {
        if (goodId > 0) {
            this.goodId = goodId;
        } else {
            throw new IllegalArgumentException(" Good ID must be > 0");
        } 
    }
    
    /**
     * @return hashCode of the object of the {@link OrderGoodInfoDto} class.
     */
    @Override
    public int hashCode() {
        return (int) (31 * id + orderId + goodId);
    }
    
    /**
     * Method used to compare this orderGoodInfo event to the specified object.
     *
     * @param obj
     *            object to compare.
     * @return result of the comparison.            
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OrderGoodInfoDto other = (OrderGoodInfoDto) obj;
        if (id != other.id) {
            return false;
        }
        if (orderId != other.getOrderId()) {
            return false;
        }
        if (goodId != other.getGoodId()) {
            return false;
        }
        if (this.hashCode() != other.hashCode()) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of the object of the {@link OrderGoodInfoDto} class .
     */
    @Override
    public String toString() {
        return "OrderGoodInfoDto [id=" + id + ", orderId=" + orderId + ", goodId=" + goodId + "]";
    }
}