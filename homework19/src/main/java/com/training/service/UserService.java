package com.training.service;

import com.training.dto.UserDto;

import java.util.List;

/**
 * Interface used for representing a user service which provides various
 * operations with users of the online shop.
 *
 * @author Alexandr_Terehov
 */
public interface UserService {

    /**
     * @return Returns a list of all users of the online shop.
     */
    List<UserDto> getUserList();

    /**
     * Get UserDto object by ID of the user.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link UserDto} class.
     */
    UserDto getUserById(final int id);

    /**
     * Get UserDto object by users's login and password.
     *
     * @param login
     *            login of the user.
     * @param password
     *            password of the user.
     * @return object of the {@link UserDto} class.
     */
    UserDto getUserByLoginPwd(final String login, final String password);
}
