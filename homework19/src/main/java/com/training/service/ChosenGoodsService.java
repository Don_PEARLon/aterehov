package com.training.service;

import com.training.dto.GoodDto;

import java.util.List;

public interface ChosenGoodsService {
 
    /**
     * Method used to add an instance of the GoodDto class to the list of goods
     * chosen by user.
     *
     * @param goodDto
     *            instance of the {@link GoodDto} class to add.
     */
    void addGoodToList(GoodDto goodDto);
    
    /**
     * @return list of the shop goods chosen by the user.
     */
    List<GoodDto> getGoodsList();
    
    /**
     * clears list of the goods chosen by user.
     */
    void clearGoodsList();
}
