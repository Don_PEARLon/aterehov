package com.training.service;

/**
 * Interface used for representing an authentification service which provides
 * operation of the online shop user authentification.
 *
 * @author Alexandr_Terehov
 */
public interface AuthentificationService {
    /**
     * Provides operation of the online shop user authentification.
     *
     * @param login
     *            login of the user.
     * @return boolean result of the authentification operation.
     */
    boolean doAuth(final String login);
}
