package com.training.service;

import com.training.dto.OrderDto;

import java.util.List;

/**
 * Interface used for representing a user service which provides various
 * operations with orders of the online shop.
 *
 * @author Alexandr_Terehov
 */
public interface OrderService {

    /**
     * @return Returns a list of all users of the online shop.
     */
    List<OrderDto> getOrderList();

    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    List<OrderDto> getOrderListByUserId(final int id);

    /**
     * Get OrderDto object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link OrderDto} class.
     */
    OrderDto getOrderById(final int id);

    /**
     * Saves information about new order.
     *
     * @param orderDto
     *            instance of the {@link OrderDto} class.
     * @return boolean result of the operation.
     */
    boolean saveOrder(final OrderDto orderDto);

    /**
     * Generates ID of the new order. 
     */
    int generateNewOrderId();
}