package com.training.service.impl;

import com.training.converter.OrderConverter;
import com.training.dao.OrderDao;
import com.training.dto.OrderDto;
import com.training.entity.Order;
import com.training.exception.OrderNotFoundExc;
import com.training.service.OrderService;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderService} interface.
 *
 * @author Alexandr_Terehov
 */
public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;
    private OrderConverter orderConverter;
    
    /**
     * 
     * @param orderDao
     *            instance of the class implements {@link OrderDao} interface.
     */
    public void setOrderDao(final OrderDao orderDao) {
        this.orderDao = orderDao;
    }
    
    /**
     * 
     * @param orderConverter
     *            instance of the {@link OrderConverter} class.
     */
    public void setOrderConverter(final OrderConverter orderConverter) {
        this.orderConverter = orderConverter;
    }

    /**
     * @return Returns a list of all users of the online shop.
     */
    public List<OrderDto> getOrderList() {
        List<OrderDto> orderDtoList = new ArrayList<>();
        for (Order order: orderDao.getOrderList()) {
            OrderDto orderDto = orderConverter.toDto(order);
            orderDtoList.add(orderDto);
        }
        return orderDtoList;
    }
  
    /**
     * Get list of the orders related to user's ID.
     *
     * @param id
     *            id of the user.
     * @return list of the orders related to user's ID.
     */
    public List<OrderDto> getOrderListByUserId(final int id) {
        List<OrderDto> orderDtoList = new ArrayList<>();
        for (Order order:  orderDao.getOrderListByUserId(id)) {
            OrderDto orderDto = orderConverter.toDto(order);
            orderDtoList.add(orderDto);
        }
        return  orderDtoList;
    }

    /**
     * Get OrderDto object by it's ID.
     *
     * @param id
     *            id of the order.
     * @return object of the {@link OrderDto} class.
     */
    public OrderDto getOrderById(final int id) {
        Order order = orderDao.getOrderById(id);
        if (order != null) {
            OrderDto orderDto = orderConverter.toDto(order);
            return orderDto;
        } else {
            throw new OrderNotFoundExc();
        }
    }

    /**
     * Saves information about new order.
     *
     * @param orderDto
     *            instance of the {@link OrderDto} class.
     * @return boolean result of the operation.
     */
    public boolean saveOrder(final OrderDto orderDto) {
        if (orderDto == null) {
            throw new OrderNotFoundExc();
        }
        Order order = orderConverter.toEntity(orderDto);
        return orderDao.insertOrder(order);
    }

    /**
     * Generates ID of the new order. 
     */
    public int generateNewOrderId() {
        List<Order> orders = orderDao.getOrderList();
        int maxId = 0;
        if (!orders.isEmpty()) {
            maxId = orders.get(0).getId();
            for (Order order : orders) {
                if (order.getId() > maxId) {
                    maxId = order.getId();
                }
            }
        }
        return maxId + 1;
    }
}