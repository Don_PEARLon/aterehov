package com.training.service.impl;

import com.training.converter.UserConverter;
import com.training.dao.UserDao;
import com.training.dto.UserDto;
import com.training.entity.User;
import com.training.exception.UserNotFoundExc;
import com.training.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link UserService} interface.
 *
 * @author Alexandr_Terehov
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao;
    private UserConverter userConverter;
    
    /**
     * 
     * @param userDao
     *            instance of the class implements {@link UserDao} interface.
     */
    public void setUserDao(final UserDao userDao) {
        this.userDao = userDao;
    }
    
    /**
     * 
     * @param userConverter
     *            instance of the {@link UserConverter} class.
     */
    public void setUserConverter(final UserConverter userConverter) {
        this.userConverter = userConverter;
    }

    /**
     * @return Returns a list of all users of she online shop.
     */
    public List<UserDto> getUserList() {
        List<UserDto> userDtoList = new ArrayList<>();
        for (User user: userDao.getUserList()) {
            UserDto userDto = userConverter.toDto(user);
            userDtoList.add(userDto);
        }
        return userDtoList;
    }

    /**
     * Get UserDto object by ID of the user.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link UserDto} class.
     */
    public UserDto getUserById(final int id) {
        User user = userDao.getUserById(id);
        if (user != null) {
            return userConverter.toDto(user);
        } else {
            throw new UserNotFoundExc();
        }
    }

    /**
     * Get UserDto object by user's login and password.
     *
     * @param login
     *            login of the user.
     * @param password
     *            password of the user.
     * @return object of the {@link UserDto} class.
     */
    public UserDto getUserByLoginPwd(final String login, final String password) {
        User user = userDao.getUserByLoginPwd(login, password);
        if (user != null) {
            return userConverter.toDto(user);
        } else {
            throw new UserNotFoundExc();
        }
    }
}