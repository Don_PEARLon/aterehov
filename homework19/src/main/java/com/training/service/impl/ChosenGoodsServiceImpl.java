package com.training.service.impl;

import com.training.dto.GoodDto;
import com.training.service.ChosenGoodsService;

import java.util.ArrayList;
import java.util.List;

public class ChosenGoodsServiceImpl implements ChosenGoodsService {
    private List<GoodDto> goodsList;

    public ChosenGoodsServiceImpl() {
        goodsList = new ArrayList<>();
    }

    /**
     * Method used to add an instance of the GoodDto class to the list of goods
     * chosen by user.
     *
     * @param goodDto
     *            instance of the {@link GoodDto} class to add.
     * @throws NullPointerException
     *             if item is null.
     */
    public void addGoodToList(GoodDto goodDto) {
        if (goodDto != null) {
            goodsList.add(goodDto);
        } else {
            throw new NullPointerException("GoodDto is null.");
        }
    }
   
    /**
     * @return list of the shop goods chosen by the user.
     */
    public List<GoodDto> getGoodsList() {
        return goodsList;
    }

    /**
     * lears list of the goods chosen by user.
     */
    public void clearGoodsList() {
        this.goodsList.clear();
    }
}