package com.training.service.impl;

import com.training.dao.UserDao;
import com.training.entity.User;
import com.training.service.AuthentificationService;


/**
 * Implementation of the {@link AuthentificationService} interface.
 *
 * @author Alexandr_Terehov
 */
public class AuthentificationServiceImpl implements AuthentificationService {
    private UserDao userDao;
    
    /**
     * 
     * @param userDao
     *            instance of the class implements {@link UserDao} interface.
     */
    public void setUserDao(final UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * Provides operation of the online shop user authentification.
     *
     * @param login
     *            login of the user.
     * @return boolean result of the authentification operation.
     */
    public boolean doAuth(final String login) {
        boolean auth = false;
        for (User user : userDao.getUserList()) {
            if (user.getName().equals(login)) {
                auth = true;
            }
        }
        return auth;
    }
}