package com.training.service.impl;

import com.training.converter.OrderGoodInfoConverter;
import com.training.dao.OrderGoodInfoDao;
import com.training.dto.OrderGoodInfoDto;
import com.training.entity.OrderGoodInfo;
import com.training.service.OrderGoodInfoService;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderGoodInfoService} interface.
 *
 * @author Alexandr_Terehov
 */
public class OrderGoodInfoServiceImpl implements OrderGoodInfoService {
    private OrderGoodInfoDao orderGoodInfoDao;
    private OrderGoodInfoConverter orderGoodInfoConverter;
    
    /**
     * 
     * @param orderGoodInfoDao
     *            instance of the class implements {@link OrderGoodInfoDao} interface.
     */
    public void setOrderGoodInfoDao(final OrderGoodInfoDao orderGoodInfoDao) {
        this.orderGoodInfoDao = orderGoodInfoDao;
    }
    
    /**
     * 
     * @param orderGoodInfoConverter
     *            instance of the {@link OrderGoodInfoConverter} class.
     */
    public void setOrderGoodInfoConverter(final OrderGoodInfoConverter orderGoodInfoConverter) {
        this.orderGoodInfoConverter = orderGoodInfoConverter;
    }

    /**
     * @return Returns a list of all entries containing information about orders and
     *         goods of the online shop.
     */
    public List<OrderGoodInfoDto> getOrderGoodInfoList() {
        List<OrderGoodInfoDto> orderGoodInfoDtoList = new ArrayList<>();
        for (OrderGoodInfo orderGoodInfo: 
                orderGoodInfoDao.getOrderGoodInfoList()) {
            OrderGoodInfoDto orderGoodInfoDto = orderGoodInfoConverter.toDto(orderGoodInfo);
            orderGoodInfoDtoList.add(orderGoodInfoDto);
        }
        return orderGoodInfoDtoList;
    }

    /**
     *  Get List if the {@link OrderGoodInfoDto} objects related to order ID.
     *
     * @param id
     *            id of the OrderGoodInfoDto entry.
     * @return list of the {@link OrderGoodInfoDto} objects related to order Id.
     */
    public List<OrderGoodInfoDto> getInfoByOrderId(final int id) {
        List<OrderGoodInfoDto> orderGoodInfoDtoList = new ArrayList<>();
        for (OrderGoodInfo orderGoodInfo: 
                orderGoodInfoDao.getInfoByOrderId(id)) {
            OrderGoodInfoDto orderGoodInfoDto = orderGoodInfoConverter.toDto(orderGoodInfo);
            orderGoodInfoDtoList.add(orderGoodInfoDto);
        }
        return orderGoodInfoDtoList;
    }

    /**
     * Saves information retrieved from instance of the {@link OrderGoodInfoDto} class.
     *
     * @param orderGoodInfoDto
     *            instance of the {@link OrderGoodInfoDto} class.
     * @return boolean result of the operation.
     */
    public boolean saveOrderGoodInfo(final OrderGoodInfoDto orderGoodInfoDto) {
        OrderGoodInfo orderGoodInfo = orderGoodInfoConverter.toEntity(orderGoodInfoDto);
        return orderGoodInfoDao.insertOrderGoodInfo(orderGoodInfo);
    }

    /**
     * Generates ID of the new OrderGoodInfoDto.
     */
    public int generateNewOrderGoodId() {
        List<OrderGoodInfo> orderGoodInfoList = orderGoodInfoDao.getOrderGoodInfoList();
        int maxId = 0;
        if (!orderGoodInfoList.isEmpty()) {
            maxId = orderGoodInfoList.get(0).getId();
            for (OrderGoodInfo infoEvent : orderGoodInfoList) {
                if (infoEvent.getId() > maxId) {
                    maxId = infoEvent.getId();
                }
            }
        }
        return maxId + 1;
    }
}