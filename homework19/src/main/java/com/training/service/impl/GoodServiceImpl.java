package com.training.service.impl;

import com.training.converter.GoodConverter;
import com.training.dao.GoodDao;
import com.training.dto.GoodDto;
import com.training.dto.OrderGoodInfoDto;
import com.training.entity.Good;
import com.training.exception.GoodNotFoundExc;
import com.training.service.GoodService;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link GoodService} interface.
 *
 * @author Alexandr_Terehov
 */
public class GoodServiceImpl implements GoodService {
    private GoodDao goodDao;
    private GoodConverter goodConverter;
    
    /**
     * 
     * @param goodDao
     *            instance of the class implements {@link GoodDao} interface.
     */
    public void setGoodDao(final GoodDao goodDao) {
        this.goodDao = goodDao;
    }
    
    /**
     * 
     * @param goodConverter
     *            instance of the class implements {@link GoodConverter} interface.
     */
    public void setGoodConverter(final GoodConverter goodConverter) {
        this.goodConverter = goodConverter;
    }
    
    /**
     * @return Returns a list of all goods of the online shop.
     */
    public List<GoodDto> getGoodsList() {
        List<GoodDto> goodDtoList = new ArrayList<>();
        for (Good good: goodDao.getGoodsList()) {
            GoodDto goodDto = goodConverter.toDto(good);
            goodDtoList.add(goodDto);
        }
        return goodDtoList;
    }

    /**
     * Get GoodDto object by it's ID.
     *
     * @param id
     *            id of the good.
     * @return object of the {@link GoodDto} class.
     */
    public GoodDto getGoodById(final int id) {
        Good good = goodDao.getGoodById(id);
        if (good != null) {
            GoodDto goodDto = goodConverter.toDto(good);
            return goodDto;
        } else {
            throw new GoodNotFoundExc();
        }
    }

    /**
     * Get list of goods related to the information contained in the
     * {@link OrderGoodInfoDto} list.
     *
     * @param orderInfo
     *            {@link OrderGoodInfoDto} list
     * @return list of goods related to the information contained in the
     *         orderInfo.
     */
    public List<GoodDto> getGoodsListByOrdersInfo(final List<OrderGoodInfoDto> orderInfo) {
        List<GoodDto> goodDtoList = new ArrayList<>();
        for (OrderGoodInfoDto orderInfoEvent : orderInfo) {
            Good good = goodDao.getGoodById(orderInfoEvent.getGoodId());
            goodDtoList.add(goodConverter.toDto(good));
        }
        return goodDtoList;
    }
}