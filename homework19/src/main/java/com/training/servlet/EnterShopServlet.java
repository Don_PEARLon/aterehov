package com.training.servlet;

import com.training.dto.GoodDto;
import com.training.dto.OrderDto;
import com.training.dto.UserDto;
import com.training.service.ChosenGoodsService;
import com.training.service.GoodService;
import com.training.service.OrderService;
import com.training.service.UserService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet used during process of shop item selection for the order. 
 */
@WebServlet(
        name = "enterShop",
        urlPatterns = "/enter")
public class EnterShopServlet extends HttpServlet {
    private static final long serialVersionUID = -6881786289314956801L;
    
    private ApplicationContext context;
    private GoodService goodService;
    private List<GoodDto> shopGoodList;
    private UserService userService;
    private OrderService orderService;
    private List<OrderDto> usersOrderList;
    private ChosenGoodsService chosenGoodsService;
          
    @Override
    public void init() {
        context = new ClassPathXmlApplicationContext("context.xml");
        goodService = (GoodService)context.getBean("GoodService");
        userService = (UserService)context.getBean("UserService");
        shopGoodList = goodService.getGoodsList();
        orderService = (OrderService)context.getBean("OrderService");
        usersOrderList = new ArrayList<>();
    }
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException 
     *             can be thrown when servlet encounters difficulty. 
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        final String userName;
        final UserDto user;
        if (session.getAttribute("user") == null) {
            userName = request.getParameter("userName");
            user = userService.getUserByLoginPwd(userName, "1");
            chosenGoodsService = (ChosenGoodsService)context.getBean("ChosenGoodsService");
            session.setAttribute("user", user);
            session.setAttribute("chosenGoodsService", chosenGoodsService);
        } else {
            user = (UserDto) session.getAttribute("user");
            chosenGoodsService = (ChosenGoodsService) session.getAttribute("chosenGoodsService");
            String shopGoodId = request.getParameter("shopGoodId");
            if (shopGoodId != null) {
                GoodDto goodDto = goodService.getGoodById(Integer.parseInt(shopGoodId));
                chosenGoodsService.addGoodToList(goodDto);
            }
        }
        usersOrderList = orderService.getOrderListByUserId(user.getId());
        request.setAttribute("user", user);
        request.setAttribute("shopGoodList", shopGoodList);
        request.setAttribute("usersOrderList", usersOrderList);
        request.setAttribute("chosenGoodsList", chosenGoodsService.getGoodsList());
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("enter-shop.jsp");
        requestDispatcher.forward(request, response);
    }
}