package com.training.servlet;

import com.training.dto.GoodDto;
import com.training.dto.OrderDto;
import com.training.dto.OrderGoodInfoDto;
import com.training.dto.UserDto;
import com.training.service.ChosenGoodsService;
import com.training.service.OrderGoodInfoService;
import com.training.service.OrderService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet used for confirmation of the order, provides calculation of the total
 * order price.
 */
@WebServlet(
        name = "submitOrder",
        urlPatterns = "/order")
public class SubmitOrderServlet extends HttpServlet {
    private static final long serialVersionUID = -1882926836615346135L;
    
    private ApplicationContext context;
    private OrderService orderService;
    private OrderGoodInfoService orderGoodInfoService;
        
    @Override
    public void init() {
        context = new ClassPathXmlApplicationContext("context.xml");
        orderService = (OrderService)context.getBean("OrderService");
        orderGoodInfoService =
                (OrderGoodInfoService)context.getBean("OrderGoodInfoService");
    }
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException
     *             can be thrown when servlet encounters difficulty. 
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        final UserDto user = (UserDto) session.getAttribute("user");
        final ChosenGoodsService chosenGoodsService
                = (ChosenGoodsService)session.getAttribute("chosenGoodsService"); 
        final double totalPrice =  getTotalPrice(chosenGoodsService.getGoodsList());
        request.setAttribute("user", user);
        request.setAttribute("totalPrice", totalPrice);
        request.setAttribute("chosenGoodsList", chosenGoodsService.getGoodsList());
        if (!chosenGoodsService.getGoodsList().isEmpty()) {
            final int newOrderId = orderService.generateNewOrderId();
            final OrderDto newOrder = new OrderDto(newOrderId, user.getId(), totalPrice);
            orderService.saveOrder(newOrder);
            for (GoodDto good : chosenGoodsService.getGoodsList()) {
                OrderGoodInfoDto orderGoodInfo = new OrderGoodInfoDto(
                        orderGoodInfoService.generateNewOrderGoodId(), newOrderId, good.getId());
                orderGoodInfoService.saveOrderGoodInfo(orderGoodInfo);
            }
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("submit-order.jsp");
        requestDispatcher.forward(request, response);
    }
         
    /**
     * Method provides creation of string in HTML format contains information about
     * total sum of user's orders from the online shop.
     *
     * @param user
     *            user of the online shop.
     */
    private double getTotalPrice(List<GoodDto> chosenGoodsList) {
        double totalPrice = 0;
        for (GoodDto good : chosenGoodsList) {
            totalPrice += good.getPrice();
        }
        return totalPrice;
    }
}