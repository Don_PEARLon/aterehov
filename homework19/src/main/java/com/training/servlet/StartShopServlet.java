package com.training.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet starts an applications, provides logging in to online-shop.
 */
@WebServlet(
        name = "startShop",
        urlPatterns = "/")
public class StartShopServlet extends HttpServlet {
    private static final long serialVersionUID = -4281588832138964034L;
    
    /**
     * Handles {@link HttpServlet} GET Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException
     *             can be thrown when servlet encounters difficulty.  
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("online-shop.jsp");
        requestDispatcher.forward(request, response);
    }
}