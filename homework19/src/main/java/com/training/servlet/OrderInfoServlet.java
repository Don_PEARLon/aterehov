package com.training.servlet;

import com.training.dto.GoodDto;
import com.training.dto.OrderGoodInfoDto;
import com.training.dto.UserDto;
import com.training.service.GoodService;
import com.training.service.OrderGoodInfoService;
import com.training.service.OrderService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet provides detailed information about orders of the  online-shop.
 */
@WebServlet(
        name = "orderInfo",
        urlPatterns = "/order-info")
public class OrderInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 4189637074938548368L;
    
    private ApplicationContext context;
    private OrderService orderService;
    private GoodService goodService;
    private OrderGoodInfoService orderGoodInfoService;

    @Override
    public void init() {
        context = new ClassPathXmlApplicationContext("context.xml");
        orderService = (OrderService)context.getBean("OrderService");
        goodService = (GoodService)context.getBean("GoodService");
        orderGoodInfoService =
                (OrderGoodInfoService)context.getBean("OrderGoodInfoService");
    }

    /**
     * Handles {@link HttpServlet} GET Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     * @throws ServletException 
     *             can be thrown when servlet encounters difficulty. 
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        final UserDto user = (UserDto) session.getAttribute("user");
        final int orderId = Integer.parseInt(request.getParameter("orderId"));
        final double orderPrice = orderService.getOrderById(orderId).getTotalPrice();
        final List<OrderGoodInfoDto> orderInfo = orderGoodInfoService.getInfoByOrderId(orderId);
        final List<GoodDto> goodList = goodService.getGoodsListByOrdersInfo(orderInfo);
        request.setAttribute("user", user);
        request.setAttribute("orderId", orderId);
        request.setAttribute("orderPrice", orderPrice);
        request.setAttribute("goodList", goodList);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("order-info.jsp");
        requestDispatcher.forward(request, response);
    }
}