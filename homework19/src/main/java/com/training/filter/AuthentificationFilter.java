package com.training.filter;

import com.training.service.AuthentificationService;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Filter used during process of authentification in online-shop.
 */
public class AuthentificationFilter implements Filter {
    private ApplicationContext context;
    private AuthentificationService authentificationService;
    private FilterConfig filterConfig;
     
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        context = new ClassPathXmlApplicationContext("context.xml");
        authentificationService =
                (AuthentificationService)context.getBean("AuthentificationService");
    }
    
    /**
     * Handles {@link HttpServlet} doFilter Method.
     *
     * @param request
     *            the {@link ServletRequest}
     * @param response
     *            the {@link ServletResponse}
     * @param chain
     *            the {@link FilterChain}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest requestHttp = (HttpServletRequest) request;
        HttpSession session = requestHttp.getSession();
        if (session.getAttribute("user") == null) {
            String userName = request.getParameter("userName");
            if (authentificationService.doAuth(userName)) {
                chain.doFilter(request, response);
            } else {
                RequestDispatcher requestDispatcher = 
                        request.getRequestDispatcher("auth-failed.jsp");
                requestDispatcher.forward(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }
     
    @Override
    public void destroy() {
        
    }
}