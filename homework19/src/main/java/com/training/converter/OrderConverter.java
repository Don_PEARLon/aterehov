package com.training.converter;

import com.training.dto.OrderDto;
import com.training.entity.Order;

/**
 * Class provides conversion operations for instances of the {@link Order} and
 * {@link OrderDto} classes.
 *
 * @author Alexandr_Terehov
 */
public class OrderConverter {
    
    /**
     * Provides conversion of the instance of the {@link OrderDto} class to the
     * instance of the {@link Order} class
     *
     * @param orderDto
     *            instance of the {@link OrderDto} class.
     */
    public Order toEntity(final OrderDto orderDto) {
        return new Order(orderDto.getId(), orderDto.getUserId(), orderDto.getTotalPrice());
    }
    
    /**
     * Provides conversion of the instance of the {@link Order} class to the
     * instance of the {@link OrderDto} class
     *
     * @param order
     *            instance of the {@link Order} class.
     */
    public OrderDto toDto(final Order order) {
        return new OrderDto(order.getId(), order.getUserId(), order.getTotalPrice());
    }
}
