package com.training.converter;

import com.training.dto.UserDto;
import com.training.entity.User;

/**
 * Class provides conversion operations for instances of the {@link User} and
 * {@link UserDto} classes.
 *
 * @author Alexandr_Terehov
 */
public class UserConverter {
    
    /**
     * Provides conversion of the instance of the {@link UserDto} class to the
     * instance of the {@link User} class
     *
     * @param userDto
     *            instance of the {@link UserDto} class.
     */
    public User toEntity(final UserDto userDto) {
        return new User(userDto.getId(), userDto.getName());
    }
    
    /**
     * Provides conversion of the instance of the {@link User} class to the
     * instance of the {@link UserDto} class
     *
     * @param user
     *            instance of the {@link User} class.
     */
    public UserDto toDto(final User user) {
        return new UserDto(user.getId(), user.getName());
    }
}
