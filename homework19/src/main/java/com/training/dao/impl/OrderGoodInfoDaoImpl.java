package com.training.dao.impl;

import com.training.dao.OrderGoodInfoDao;
import com.training.entity.OrderGoodInfo;
import com.training.util.H2Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link OrderGoodInfoDao} interface. 
 *
 * @author Alexandr_Terehov
 */
public class OrderGoodInfoDaoImpl implements OrderGoodInfoDao {
    private static final String SQL_SELECT_ALL_ORDER_GOOD = "SELECT * FROM order_good";
    private static final String SQL_SELECT_INFO_BY_ORDER_ID =
            "SELECT * FROM order_good WHERE order_id=?";
    private static final  String SQL_INSERT_ORDER_GOOD_INFO =
            "INSERT INTO order_good(id, order_id, good_id ) VALUES(?,?,?)";

    private H2Utils h2Utils;
    
    /**
     * 
     * @param h2Utils
     *            object of the {@link H2Utils} class to set.
     */
    public void setH2Utils(final H2Utils h2Utils) {
        this.h2Utils = h2Utils;
    }
    
    /**
     * @return Returns a list of all {@link OrderGoodInfo} objects contained in the
     *         database.
     */
    public List<OrderGoodInfo> getOrderGoodInfoList() {
        List<OrderGoodInfo>  orderGoodInfoEvents = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_ORDER_GOOD);
            while (resultSet.next()) {
                int infoId = resultSet.getInt("id");
                int orderId = resultSet.getInt("order_id");
                int goodId = resultSet.getInt("good_id");
                OrderGoodInfo infoEvent = new OrderGoodInfo(infoId, orderId, goodId);
                orderGoodInfoEvents.add(infoEvent);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return orderGoodInfoEvents;
    }

    /**
     * Get OrderGoodInfo object by it's ID.
     *
     * @param id
     *            id of the OrderGoodInfo entry.
     * @return object of the {@link OrderGoodInfo} class.
     */
    public  List<OrderGoodInfo> getInfoByOrderId(final int id) {
        List<OrderGoodInfo> orderGoodInfoEvents = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_INFO_BY_ORDER_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int infoId = resultSet.getInt("id");
                int orderId = resultSet.getInt("order_id");
                int goodId = resultSet.getInt("good_id");
                OrderGoodInfo infoEvent = new OrderGoodInfo(infoId, orderId, goodId);
                orderGoodInfoEvents.add(infoEvent);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return orderGoodInfoEvents;
    }

    /**
     * Insert into database information from instance of the {@link OrderGoodInfo}
     * class.
     *
     * @param orderGoodInfo
     *            instance of the {@link OrderGoodInfo} class.
     * @return boolean result of the operation.
     */
    public boolean insertOrderGoodInfo(final OrderGoodInfo orderGoodInfo) {
        boolean flag = false;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ORDER_GOOD_INFO);
            statement.setInt(1, orderGoodInfo.getId()); 
            statement.setInt(2, orderGoodInfo.getOrderId());
            statement.setInt(3, orderGoodInfo.getGoodId());
            statement.executeUpdate();
            flag = true;
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return flag;
    }
}