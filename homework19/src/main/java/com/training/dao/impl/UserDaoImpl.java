package com.training.dao.impl;

import com.training.dao.UserDao;
import com.training.entity.User;
import com.training.util.H2Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the {@link UserDao} interface. 
 *
 * @author Alexandr_Terehov
 */
public class UserDaoImpl implements UserDao {
    private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM user";
    private static final String SQL_SELECT_USER_BY_ID =
            "SELECT * FROM user WHERE id=?";
    private static final String SQL_SELECT_USER_BY_LOGIN_PASSWORD =
            "SELECT * FROM user WHERE login=? AND password=?";
    
    private H2Utils h2Utils;
    
    /**
     * 
     * @param h2Utils
     *            object of the {@link H2Utils} class to set.
     */
    public void setH2Utils(final H2Utils h2Utils) {
        this.h2Utils = h2Utils;
    }
    
    /**
     * @return Returns a list of all users contained in the database.
     */
    public List<User> getUserList() {
        List<User> users = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.createStatement();
            ResultSet resultSet =
                    statement.executeQuery(SQL_SELECT_ALL_USERS);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String login = resultSet.getString("login");
                User user = new User(id, login);
                users.add(user);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        } 
        return users;
    }

    /**
     * Get User object by it's ID.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link User} class.
     */
    public User getUserById(final int id) {
        User user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int userId = resultSet.getInt("id");
                String login = resultSet.getString("title");
                user = new User(userId, login);
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        }  
        return user;
    }

    /**
     * Get User object by it's login and password.
     *
     * @param login
     *            login of the user.
     * @param password
     *            password of the user.
     * @return object of the {@link User} class.
     */
    public User getUserByLoginPwd(final String login, final String password) {
        User user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = h2Utils.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN_PASSWORD);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int userId = resultSet.getInt("id");
                String userLogin = resultSet.getString("login");
                user = new User(userId, userLogin); 
            }
        } catch (SQLException exc) {
            System.err.println("SQL exception (request or table failed): " + exc);
        } finally {
            h2Utils.close(statement);
            h2Utils.close(connection);
        }  
        return user;
    }
}