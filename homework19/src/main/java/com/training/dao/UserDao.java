package com.training.dao;

import com.training.entity.User;

import java.util.List;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link User} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface UserDao {

    /**
     * @return Returns a list of all users contained in the database.
     */
    List<User> getUserList();

    /**
     * Get User object by it's ID.
     *
     * @param id
     *            id of the user.
     * @return object of the {@link User} class.
     */
    User getUserById(final int id);

    /**
     * Get User object by it's login and password.
     *
     * @param login
     *            login of the user.
     * @param password
     *            password of the user.
     * @return object of the {@link User} class.
     */
    User getUserByLoginPwd(final String login, final String password);
}
