package com.training.dao;

import com.training.entity.OrderGoodInfo;

import java.util.List;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link OrderGoodInfo} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface OrderGoodInfoDao {

    /**
     * @return Returns a list of all {@link OrderGoodInfo} objects contained in the
     *         database.
     */
    List<OrderGoodInfo> getOrderGoodInfoList();

    /**
     * Get OrderGoodInfo object by it's ID.
     *
     * @param id
     *            id of the OrderGoodInfo entry.
     * @return object of the {@link OrderGoodInfo} class.
     */
    List<OrderGoodInfo> getInfoByOrderId(final int id);
    
    /**
     * Insert into database information from instance of the {@link OrderGoodInfo}
     * class.
     *
     * @param orderGoodInfo
     *            instance of the {@link OrderGoodInfo} class.
     * @return boolean result of the operation.
     */
    boolean insertOrderGoodInfo(final OrderGoodInfo orderGoodInfo);
}