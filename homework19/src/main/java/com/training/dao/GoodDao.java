package com.training.dao;

import com.training.entity.Good;

import java.util.List;

/**
 * Data Access Object interface. Provides 'database' operations with
 * {@link Good} objects.
 * 
 * @author Alexandr_Terehov
 */
public interface GoodDao {

    /**
     * @return Returns a list of all goods contained in the database.
     */
    List<Good> getGoodsList();

    /**
     * Get Good object by it's ID.
     *
     * @param id
     *            id of the good.
     * @return object of the {@link Good} class.
     */
    Good getGoodById(final int id);
}
