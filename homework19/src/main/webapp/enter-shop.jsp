<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<!--     JSTL Tag Library -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<style>
.layer1 {
	margin-left: 60%;
}
</style>
</head>
<body>
	<div class='layer1'>
		<form action='online-shop' method='get'>
			<input type='submit' value='Logout'>
		</form>
	</div>
	<div align=center>
		<h1>Hello ${user.getName()}!</h1>
		<c:choose>
			<c:when test="${chosenGoodsList.isEmpty()}">
				<h2>Make your order</h2>
			</c:when>
			<c:otherwise>
				<h4>You have already chosen:</h4>
				<%
					int itemNumber = 1;
				%>
				<c:forEach items="${chosenGoodsList}" var="item">
					<h4><%=itemNumber%>) ${item.getTitle()} ${item.getPrice()}$
					</h4>
					<%
						itemNumber++;
					%>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		<form action='enter' method='post'>
			<select name='shopGoodId'>
				<c:forEach items="${shopGoodList}" var="good">
					<option value='${good.getId()}'>${good.getTitle()}
						${good.getPrice()}$
				</c:forEach>
			</select> <input type='submit' value='Add Item'>
		</form>
		<form action='order' method='post'>
			<input type='submit' value='Submit'>
		</form>
		<c:choose>
			<c:when test="${usersOrderList.isEmpty()}">
				<br>
				<h2>${user.getName()} you have no orders yet.</h2>
			</c:when>
			<c:otherwise>
				<br>
				<h4>${user.getName()} your previous orders:</h4>
				<%
					int orderNumber = 1;
				%>
				<c:forEach items="${usersOrderList}" var="order">
					<h4>
						<a href='order-info?orderId=${order.getId()}'><%=orderNumber%>)
							Order ID: ${order.getId()} Total Price: ${order.getTotalPrice()}$</a>
					</h4>
					<%
						orderNumber++;
					%>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>