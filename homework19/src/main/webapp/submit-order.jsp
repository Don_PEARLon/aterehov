<%@ page contentType="text/html;charset=UTF-8" language="java"
import="com.training.service.ChosenGoodsService"%>

<!--     JSTL Tag Library -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>
	<div align=center>
		<h1>Dear ${user.getName()}, your order:</h1>
    	<% int itemNumber = 1; %>
		<c:forEach items="${chosenGoodsList}" var="item">
			<h4><%=itemNumber %>) ${item.getTitle()} ${item.getPrice()}$</h4>
			<% itemNumber++; %>
		</c:forEach>
		<br>
		<h4>Total: ${totalPrice}$</h4> <br>
		<% final ChosenGoodsService chosenGoodsService = (ChosenGoodsService) session.getAttribute("chosenGoodsService");
		   chosenGoodsService.clearGoodsList();   
		   session.setAttribute("chosenGoodsService", chosenGoodsService);
		%>
		<form action='enter' method='post'>
			<input type='submit' value='Back'>
		</form>
	</div>
</body>
</html>