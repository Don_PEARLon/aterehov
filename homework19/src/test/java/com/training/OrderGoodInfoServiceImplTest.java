package com.training;

import com.training.converter.OrderGoodInfoConverter;
import com.training.dao.OrderGoodInfoDao;
import com.training.dto.OrderGoodInfoDto;
import com.training.entity.OrderGoodInfo;
import com.training.service.impl.OrderGoodInfoServiceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the OrderGoodInfoServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderGoodInfoServiceImplTest {
	@Mock
	private OrderGoodInfoDao orderGoodInfoDao;
	
	OrderGoodInfoConverter orderGoodInfoConverter;
	
	@InjectMocks
	private OrderGoodInfoServiceImpl orderGoodInfoServiceImpl;
	
	private List<OrderGoodInfo> orderGoodInfoList;
	private OrderGoodInfo orderGoodInfo1;
	private OrderGoodInfo orderGoodInfo2;
	private OrderGoodInfo orderGoodInfo3;
	
	private List<OrderGoodInfoDto> orderGoodInfoListDto;
	private OrderGoodInfoDto orderGoodInfoDto1;
	private OrderGoodInfoDto orderGoodInfoDto2;
	private OrderGoodInfoDto orderGoodInfoDto3;
	
	@Before
	public void initObjects() {
		orderGoodInfoConverter = new OrderGoodInfoConverter();
		orderGoodInfoServiceImpl.setOrderGoodInfoConverter(orderGoodInfoConverter);
		
		orderGoodInfoList = new ArrayList<>();
		orderGoodInfoListDto = new ArrayList<>();
		
		orderGoodInfo1 = new OrderGoodInfo(1, 1, 1);
		orderGoodInfo2 = new OrderGoodInfo(2, 1, 2);
		orderGoodInfo3 = new OrderGoodInfo(3, 1, 3);
				
		orderGoodInfoList.add(orderGoodInfo1);
		orderGoodInfoList.add(orderGoodInfo2);
		orderGoodInfoList.add(orderGoodInfo3);
		
		orderGoodInfoDto1 = new OrderGoodInfoDto(1, 1, 1);
		orderGoodInfoDto2 = new OrderGoodInfoDto(2, 1, 2);
		orderGoodInfoDto3 = new OrderGoodInfoDto(3, 1, 3);
				
		orderGoodInfoListDto.add(orderGoodInfoDto1);
		orderGoodInfoListDto.add(orderGoodInfoDto2);
		orderGoodInfoListDto.add(orderGoodInfoDto3);
		
	}
	
	@Test
	public void testGetOrderGoodInfoList() {
		
		//given:
		Mockito.when(orderGoodInfoDao.getOrderGoodInfoList()).thenReturn(orderGoodInfoList);
		
		//when:
		List<OrderGoodInfoDto> result= orderGoodInfoServiceImpl.getOrderGoodInfoList();
		
		//then:
		assertEquals(result, orderGoodInfoListDto);
		Mockito.verify(orderGoodInfoDao).getOrderGoodInfoList();
	}
	
	@Test
	public void testGenerateNewOrderId() {
		
		//given:
		Mockito.when(orderGoodInfoDao.getOrderGoodInfoList()).thenReturn(orderGoodInfoList);
		
		//when:
		int newOrderId = orderGoodInfoServiceImpl.generateNewOrderGoodId();
		
		//then:
		assertEquals(4, newOrderId, 0);
		Mockito.verify(orderGoodInfoDao).getOrderGoodInfoList();
	}
	
	@Test
	public void testGenerateNewOrderFirstId() {
		
		//given:
		List<OrderGoodInfo> newOrderGoodInfoList = new ArrayList<>();
		Mockito.when(orderGoodInfoDao.getOrderGoodInfoList()).thenReturn(newOrderGoodInfoList);
		
		//when:
		int newOrderId = orderGoodInfoServiceImpl.generateNewOrderGoodId();
		
		//then:
		assertEquals(1, newOrderId, 0);
		Mockito.verify(orderGoodInfoDao).getOrderGoodInfoList();
	}
}
