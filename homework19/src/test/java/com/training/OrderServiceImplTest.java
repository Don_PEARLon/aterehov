package com.training;

import com.training.converter.OrderConverter;
import com.training.dao.OrderDao;
import com.training.dto.OrderDto;
import com.training.entity.Order;
import com.training.exception.OrderNotFoundExc;
import com.training.service.impl.OrderServiceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the OrderServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
	@Mock
	private OrderDao orderDao;
	
	@InjectMocks
	private OrderServiceImpl orderServiceImpl;
	
	private OrderConverter orderConverter; 
	
	private List<Order> orderList;
	private Order order1;
	private Order order2;
	private Order order3;
	
	private List<OrderDto> orderListDto;
	private OrderDto orderDto1;
	private OrderDto orderDto2;
	private OrderDto orderDto3;
	
	@Before
	public void initObjects() {
		orderConverter = new OrderConverter();
		orderServiceImpl.setOrderConverter(orderConverter);
		
		orderList = new ArrayList<>();
		orderListDto = new ArrayList<>();
		
		order1 = new Order(1, 1, 100);
		order2 = new Order(2, 1, 200);
		order3 = new Order(3, 2 ,300);
		
		orderDto1 = new OrderDto(1, 1, 100);
		orderDto2 = new OrderDto(2, 1, 200);
		orderDto3 = new OrderDto(3, 2 ,300);
		
		orderList.add(order1);
		orderList.add(order2);
		orderList.add(order3);
		
		orderListDto.add(orderDto1);
		orderListDto.add(orderDto2);
		orderListDto.add(orderDto3);
		
	}
	
	@Test
	public void testGetOrderList() {
		//given:
        Mockito.when(orderDao.getOrderList()).thenReturn(orderList);
       
        //when
        List<OrderDto> resultList = orderServiceImpl.getOrderList();
        
        //then
        assertEquals(resultList, orderListDto);
        Mockito.verify(orderDao).getOrderList();
	}
	
	@Test
	public void testGetOrderById() {
		//given:
        Mockito.when(orderDao.getOrderById(1)).thenReturn(order1);
        
        //when
        OrderDto result = orderServiceImpl.getOrderById(1);
        
        //then
        assertEquals(result, orderDto1);
        Mockito.verify(orderDao).getOrderById(1);
	}
	
	@Test(expected = OrderNotFoundExc.class)
	public void testGetOrderByIdNegative() {
		//given:
        Mockito.when(orderDao.getOrderById(4)).thenReturn(null);
        
        //when
        orderServiceImpl.getOrderById(4);
        
        //then
        Mockito.verify(orderDao).getOrderById(4);
	}
	
	@Test
	public void testGenerateNewOrderId() {
		
		//given:
		Mockito.when(orderDao.getOrderList()).thenReturn(orderList);
		
		//when:
		int newOrderId = orderServiceImpl.generateNewOrderId();
		
		//then:
		assertEquals(4, newOrderId, 0);
		Mockito.verify(orderDao).getOrderList();
	}
	
	@Test
	public void testGenerateNewOrderFirstId() {
		
		//given:
		List<Order> newOrderList = new ArrayList<>();
		Mockito.when(orderDao.getOrderList()).thenReturn(newOrderList);
		
		//when
		int newOrderId = orderServiceImpl.generateNewOrderId();
		
		//then
		assertEquals(1, newOrderId, 0);
		Mockito.verify(orderDao).getOrderList();
	}
}
