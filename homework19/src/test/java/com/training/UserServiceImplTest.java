package com.training;

import com.training.converter.UserConverter;
import com.training.dao.UserDao;
import com.training.dto.UserDto;
import com.training.entity.User;
import com.training.exception.UserNotFoundExc;
import com.training.service.impl.UserServiceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the UserServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    @Mock
    private UserDao userDao;
    
    @InjectMocks
	private UserServiceImpl userServiceImpl;
    
    private UserConverter userConverter;
    
    private List<User> userList;
    private List<UserDto> userListDto;
    
    private User user1;
    private User user2;
    private User user3;
    
    private UserDto userDto1;
    private UserDto userDto2;
    private UserDto userDto3;
    
    @Before
	public void initObjects() {
    	userConverter = new UserConverter(); 
    	
    	userServiceImpl.setUserConverter(userConverter);
    	
    	userList = new ArrayList<>();
    	userListDto = new ArrayList<>();
    	
    	user1 = new User(1, "user1");
    	user2 = new User(2, "user2");
    	user3 = new User(3, "user3");
    	
    	userDto1 = new UserDto(1, "user1");
    	userDto2 = new UserDto(2, "user2");
    	userDto3 = new UserDto(3, "user3");
    	
    	userList.add(user1);
    	userList.add(user2);
    	userList.add(user3);
    	
    	userListDto.add(userDto1);
    	userListDto.add(userDto2);
    	userListDto.add(userDto3);
    	
    }
	
	@Test
	public void testGetUserList() {
		
		//given:
        Mockito.when(userDao.getUserList()).thenReturn(userList);
        
        //when
        List<UserDto> resultList = userServiceImpl.getUserList();
        
        //then
        assertEquals(resultList, userListDto);
        Mockito.verify(userDao).getUserList();
    }
	
	@Test
	public void testGetUserById() {
		
		//given:
        Mockito.when(userDao.getUserById(1)).thenReturn(user1);
        
        //when
        UserDto result = userServiceImpl.getUserById(1);
        
        //then
        assertEquals(result, userDto1);
        Mockito.verify(userDao).getUserById(1);
	}
	
	@Test(expected = UserNotFoundExc.class)
	public void testGetUserByIdNegative() {
		//given:
        Mockito.when(userDao.getUserById(4)).thenReturn(null);
        
        //when
        userServiceImpl.getUserById(4);
        
        //then
        Mockito.verify(userDao).getUserById(4);
	}
	
	@Test
	public void testGetUserByLoginPwd() {
		//given:
        Mockito.when(userDao.getUserByLoginPwd("user1", "1")).thenReturn(user1);
        
        //when
        UserDto result = userServiceImpl.getUserByLoginPwd("user1", "1");
        
        //then
        assertEquals(result, userDto1);
        Mockito.verify(userDao).getUserByLoginPwd("user1", "1");
	}
	
	@Test(expected = UserNotFoundExc.class)
	public void testGetUserByLoginPwdNegative() {
		//given:
        Mockito.when(userDao.getUserByLoginPwd("user4", "1")).thenReturn(null);
        
        //when
        userServiceImpl.getUserByLoginPwd("user4", "1");
        
        //then
        Mockito.verify(userDao).getUserByLoginPwd("user1", "1");
	}
}
