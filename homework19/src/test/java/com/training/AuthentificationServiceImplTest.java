package com.training;

import com.training.dao.UserDao;
import com.training.entity.User;
import com.training.service.impl.AuthentificationServiceImpl;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the AuthentificationServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthentificationServiceImplTest {
	@Mock
	private UserDao userDao;

	@InjectMocks
	private AuthentificationServiceImpl authentificationServiceImpl;

	private List<User> userList;
	private User user1;
	private User user2;
	private User user3;

	@Before
	public void initObjects() {
		user1 = new User(1, "user1");
		user2 = new User(2, "user2");
		user3 = new User(3, "user3");
		        
		userList = new ArrayList<>();
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
	}
    
	@Test
	public void testDoAuth() {
		
		//given:
		Mockito.when(userDao.getUserList()).thenReturn(userList);
		
		//when:
		boolean condition = authentificationServiceImpl.doAuth("user1");
		
		//then
		assertTrue(condition);
		Mockito.verify(userDao).getUserList();
	}
	
	@Test
	public void testDoAuthNegative() {
		
		//given:
		Mockito.when(userDao.getUserList()).thenReturn(userList);
		
		//when:
		boolean condition = authentificationServiceImpl.doAuth("user4");
		
		//then:
		assertFalse(condition);
		Mockito.verify(userDao).getUserList();
	}
}
