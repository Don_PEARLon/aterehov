package com.training;

import com.training.converter.GoodConverter;
import com.training.dao.GoodDao;
import com.training.dto.GoodDto;
import com.training.entity.Good;
import com.training.exception.GoodNotFoundExc;
import com.training.service.impl.GoodServiceImpl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test suite for the GoodServiceImpl class.
 *
 * @author Alexandr_Terehov
 */
@RunWith(MockitoJUnitRunner.class)
public class GoodServiceImplTest {
	@Mock
	private GoodDao goodDao;
	
	@InjectMocks
	private GoodServiceImpl goodServiceImpl;
	
	private GoodConverter goodConverter; 
	
	private List<Good> goodList;
	private Good good1;
	private Good good2;
	private Good good3;
	
	private List<GoodDto> goodListDto;
	private GoodDto goodDto1;
	private GoodDto goodDto2;
	private GoodDto goodDto3;
	
	@Before
	public void initObjects() {
		goodConverter = new GoodConverter();
		goodServiceImpl.setGoodConverter(goodConverter);
		
		goodList = new ArrayList<>();
		goodListDto = new ArrayList<>();
		
		good1 = new Good(1, "good1", 100);
		good2 = new Good(2, "good2", 200);
		good3 = new Good(3, "good3" ,300);
		
		goodDto1 = new GoodDto(1, "good1", 100);
		goodDto2 = new GoodDto(2, "good2", 200);
		goodDto3 = new GoodDto(3, "good3" ,300);
		
		goodList.add(good1);
		goodList.add(good2);
		goodList.add(good3);
		
		goodListDto.add(goodDto1);
		goodListDto.add(goodDto2);
		goodListDto.add(goodDto3);
		
	}
	
	@Test
	public void testGetGoodsList() {
		//given:
        Mockito.when(goodDao.getGoodsList()).thenReturn(goodList);
       
        //when
        List<GoodDto> resultList = goodServiceImpl.getGoodsList();
        
        //then
        assertEquals(resultList, goodListDto);
        Mockito.verify(goodDao).getGoodsList();
	}
	
	@Test
	public void testGetGoodById() {
		//given:
        Mockito.when(goodDao.getGoodById(1)).thenReturn(good1);
        
        //when
        GoodDto result = goodServiceImpl.getGoodById(1);
        
        //then
        assertEquals(result, goodDto1);
        Mockito.verify(goodDao).getGoodById(1);
	}
	
	@Test(expected = GoodNotFoundExc.class)
	public void testGetGoodByIdNegative() {
		//given:
        Mockito.when(goodDao.getGoodById(4)).thenReturn(null);
        
        //when
        goodServiceImpl.getGoodById(4);
        
        //then
        Mockito.verify(goodDao).getGoodById(4);
	}
}
