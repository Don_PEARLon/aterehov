package com.training.homework8.task1;

import com.training.homework8.task1.exception.ListIsFullException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
                                    
/**
 * Resizable-array implementation of the 'List' interface.
 *
 * @author Alexandr_Terehov
 */
public class MyArrayList<E> extends ArrayList<E> {
    private static final long serialVersionUID = 8965452581122892189L;
          
    /**
     * Default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;
                 
    /**
     * Current capacity.
     */
    private int capacity;
              
    /**
     * The size of the MyArrayList (the number of elements it contains).
     */
    private int size;
          
    /**
     * The maximum size of the MyArrayList.
     */
    private int maxSize;
        
    /**
     * The array buffer into which the elements of the ArrayList are stored.
     */
    private Object[] elementData;
        
    /**
     * Constructs an empty list with an initial capacity of ten and max size of ten.
     */
    public MyArrayList() {
        elementData = new Object[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        maxSize = DEFAULT_CAPACITY;
    }
          
    /**
     * Constructs an empty list with its maximum size.
     *
     * @param maxSize
     *            The maximum size of the MyArrayList.
     * @throws IllegalArgumentException
     *             if the specified maximum size is negative.
     */
    public MyArrayList(int maxSize) {
        if (maxSize < 0) {
            throw new IllegalArgumentException("Illegal maximum size: " + maxSize);
        }
        elementData = new Object[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        this.maxSize = maxSize;
    }
                        
    /**
     * Returns the element at the specified position in this list.
     *
     * @param index
     *            index of the element to return.
     * @return the element at the specified position in this list.
     * @throws IndexOutOfBoundsException
     *             when index is out of array bounds.
     */
    @Override
    public E get(int index) {
        indexCheck(index);
        return (E) elementData[index];
    }
           
    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param index
     *            index of the element to replace.
     * @param element
     *            element to be stored at the specified position.
     * @return the element previously at the specified position.
     * @throws IndexOutOfBoundsException
     *             when index is out of array bounds.
     */
    @Override
    public E set(int index, E element) {
        indexCheck(index);
        E oldValue = this.get(index);
        elementData[index] = element;
        return oldValue;
    }
             
    /**
     * Inserts the specified element at the specified position in this list.
     *
     * @param index
     *            index at which the specified element is to be inserted.
     * @param element
     *            element to be inserted.
     */
    @Override
    public void add(int index, E element) {
        try {
            maxSizeCheck();
            indexCheck(index);
            if ((size + 1) > (capacity)) {
                elementDataArrayGrow();
            }
            System.arraycopy(elementData, index, elementData, index + 1, size - index);
            elementData[index] = element;
            size++;
        } catch (ListIsFullException exception) {
            System.out.println(exception.getMessage());
        }
    }
           
    /**
     * Appends the specified element to the end of this list.
     *
     * @param elem
     *            element to be appended to this list
     * @return 'true' if element has been successfully added.
     */
    @Override
    public boolean add(E elem) {
        boolean result = false;
        try {
            maxSizeCheck();
            if ((size + 1) > (capacity)) {
                elementDataArrayGrow();
            }
            elementData[size] = elem;
            size++;
            result = true;
        } catch (ListIsFullException exception) {
            System.out.println(exception.getMessage());
        }
        return result;
    }
         
    /**
     * Increases the capacity by 50% if MyArrayList is already full and its capacity
     * needs to be increased.
     */
    private void elementDataArrayGrow() {
        int oldCapacity = capacity;
        capacity = oldCapacity + (oldCapacity >> 1);
        elementData = Arrays.copyOf(elementData, capacity);
    }
                
    /**
     * Removes the element at the specified position in this list.
     *
     * @param index
     *            the index of the element to be removed.
     * @return the element that was removed from the list.
     * @throws IndexOutOfBoundsException
     *             when index is out of array bounds..
     */
    @Override
    public E remove(int index) {
        indexCheck(index);
        E oldValue = this.get(index);
        fastRemove(index);
        return oldValue;
    }
          
    /**
     * Removes the first occurrence of the specified element from this list.
     *
     * @param obj
     *            element to be removed from this list, if present.
     * @return 'true' if this list contained the specified element.
     */
    @Override
    public boolean remove(Object obj) {
        if (obj == null) {
            for (int index = 0; index < size; index++) {
                if (elementData[index] == null) {
                    fastRemove(index);
                    return true;
                }
            }    
        } else {
            for (int index = 0; index < size; index++) {
                if (obj.equals(elementData[index])) {
                    fastRemove(index);
                    return true;
                }
            }
        }
        return false;
    }
              
    /**
     * Private remove method that skips bounds checking and does not return the
     * value removed.
     */
    private void fastRemove(int index) {
        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);
        }
        elementData[--size] = null;
    }
             
    /**
     * Checks if the given index is in range. If not, throws an
     * IndexOutOfBoundsException.
     * 
     * @param index
     *            index to check.
     * @throws IndexOutOfBoundsException
     *             when index is out of array bounds.
     */
    private void indexCheck(int index) {
        if ((index < 0) || (index >= size)) {
            throw new IndexOutOfBoundsException("Index:" + index + " is out of array bounds");
        }
    }
            
    /**
     * Checks maximum size of this List, a possibility of adding a new element. If
     * this List is full, throws a ListIsFullException.
     * 
     * @throws ListIsFullException
     *              if this list is already full. 
     */
    private void maxSizeCheck() throws ListIsFullException {
        if ((size + 1) > maxSize) {
            throw new ListIsFullException("Can't add a new element. " 
                       + "This List is already full.");
        }
    }
            
    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }
           
    /**
     * Returns 'true' if this list contains no elements.
     *
     * @return 'true' if this list contains no elements
     */
    public boolean isEmpty() {
        return size == 0;
    }
            
    /**
     * Removes all of the elements from this list. The list will be empty after this
     * call returns.
     */
    public void clear() {
        for (int i = 0; i < size; i++) {
            elementData[i] = null;
        }
        size = 0;
    }
           
    /**
     * Returns 'true' if this list contains the specified element.
     *
     * @param obj
     *            element whose presence in this list is to be tested
     * @return 'true' if this list contains the specified element
     */
    @Override
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }
            
    /**
     * Returns the index of the first occurrence of the specified element.
     *
     * @param obj
     *            element whose index in this list needs to be found.
     * @return index of the element in the list.
     */
    @Override
    public int indexOf(Object obj) {
        if (obj == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (obj.equals(elementData[i])) {
                    return i;
                }
            }
        }
        return -1;
    }
                  
    /**
     * Method provides getting the String representation of this list.
     */
    @Override
    public String toString() {
        String arrayListStr = "[";
        for (int i = 0; i < size - 1; i++) {
            arrayListStr += elementData[i] + ", ";
        }
        arrayListStr += elementData[size - 1] + "]";
        return arrayListStr;
    }
                 
    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * @return an iterator over the elements in this list in proper sequence.
     */
    @Override
    public Iterator<E> iterator() {
        return new MyListIterator();
    }
              
    /**
     * An inner class, implementation of the 'Iterator' interface.
     */
    private class MyListIterator implements Iterator<E> {
        int cursor;
        
        public boolean hasNext() {
            return cursor != size;
        }
        
        public E next() {
            int i = cursor;
            if (i >= size) {
                throw new NoSuchElementException();
            }
            cursor = i + 1;
            return (E) elementData[i];
        }
    }
}
