package com.training.homework8.task1.exception;
                  
/**
 * Class used to represent a custom exception that can occur during the process
 * of adding element in a full List.
 *
 * @author Alexandr_Terehov
 */
public class ListIsFullException extends Exception {
    public ListIsFullException(String message) {
        super(message);
    }
    
    private static final long serialVersionUID = 1L;
}
