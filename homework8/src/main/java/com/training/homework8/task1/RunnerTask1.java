package com.training.homework8.task1;

import java.util.Iterator;
import java.util.List;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {

    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        List<Integer> myArrayList = new MyArrayList<Integer>(10);
        Integer testInt = new Integer(12);
        myArrayList.add(5);
        myArrayList.add(10);
        myArrayList.add(testInt);
        myArrayList.add(15);
        myArrayList.add(20);
        myArrayList.add(17);
        myArrayList.add(24);
        myArrayList.add(68);
        myArrayList.add(45);
        myArrayList.add(3);
        myArrayList.add(4, 111);
        System.out.println(myArrayList);
        System.out.println(myArrayList.size());
        myArrayList.remove(5);
        System.out.println(myArrayList.size());
        myArrayList.add(4, 111);
        Iterator<Integer> iterator = myArrayList.iterator();
        while (iterator.hasNext()) {
            Integer val = iterator.next();
            System.out.print(val + " ");
        }
    }
}
