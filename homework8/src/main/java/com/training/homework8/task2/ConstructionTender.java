package com.training.homework8.task2;

import com.training.homework8.task2.entity.WorkingCrew;
import com.training.homework8.task2.entity.WorkingCrewReport;
import com.training.homework8.task2.entity.WorkingProfessions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class represents the construction tender process.
 *
 * @author Alexandr_Terehov
 */
public class ConstructionTender {
        
    /**
     * Map contains tender requirements.
     */
    private Map<WorkingProfessions, Integer> requirements;
           
    /**
     * List of working crews participating in the construction tender.
     */
    List<WorkingCrew> workingCrews;
             
    /**
     * List of the reports contains information about each working crew
     * participating in the construction tender.
     */
    List<WorkingCrewReport> crewReports;
              
    /**
     * List of the reports contains information about each working crew satisfying
     * tender requirements.
     */
    List<WorkingCrewReport> passedCrewReports;
            
    /**
     * Constructor.
     *
     * @param requirements
     *            construction tender requirements.
     * @param workingCrews
     *            List of working crews participating in the construction tender.
     */
    public ConstructionTender(Map<WorkingProfessions, Integer> requirements, 
            List<WorkingCrew> workingCrews) {
        if ((requirements == null) || (workingCrews == null)) {
            throw new NullPointerException("Tender requirements "
                    + "or Working Crew List can't be null");
        }
        this.requirements = requirements;
        this.workingCrews = workingCrews;
        crewReports = new ArrayList<WorkingCrewReport>();
        passedCrewReports = new ArrayList<WorkingCrewReport>();
        buildCrewReports();
    }
              
    /**
     * Method used build list of reports with information about each working crew
     * participating in the construction tender.
     */
    private void buildCrewReports() {
        WorkCrewReportBuilder reportBuilder;
        WorkingCrewReport crewReport;
        for (WorkingCrew workingCrew : workingCrews) {
            reportBuilder = new WorkCrewReportBuilder(workingCrew);
            crewReport = reportBuilder.getWorkingCrewReport();
            crewReports.add(crewReport);
        }
        buildPassedCrewReports();
    }
            
    /**
     * Method used build list of reports with information about each working crew
     * satisfying tender requirements.
     */
    private void buildPassedCrewReports() {
        for (WorkingCrewReport crewReport : crewReports) {
            if (verifyCrewReport(crewReport)) {
                passedCrewReports.add(crewReport);
            }
        }
    }
                   
    /**
     * Method comparing information about working crews with requirements of
     * construction tender
     *
     * @param crewReport
     *            report contains information about working crew.
     */
    private boolean verifyCrewReport(WorkingCrewReport crewReport) {
        boolean result = true;
        Map<WorkingProfessions, Integer> reportProfessionsInfo = 
                crewReport.getCrewProfessionsInfo();
        for (WorkingProfessions profession : requirements.keySet()) {
            if ((reportProfessionsInfo.get(profession)) < requirements.get(profession)) {
                result = false;
            }
        }
        return result;
    }
                               
    /**
     * 
     * @return result of the construction tender.
     */
    public WorkingCrewReport getTenderResults() {
        WorkingCrewReport winner = null;
        if (passedCrewReports.size() != 0) {
            if (passedCrewReports.size() == 1) {
                winner = passedCrewReports.get(0);
            } else {
                winner = passedCrewReports.get(0);
                for (WorkingCrewReport crewReport : passedCrewReports) {
                    if (crewReport.getCrewSalary() < winner.getCrewSalary()) {
                        winner = crewReport;
                    }
                }
            }
        }
        return winner;
    }
}
