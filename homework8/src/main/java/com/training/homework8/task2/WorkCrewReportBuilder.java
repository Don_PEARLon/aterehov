package com.training.homework8.task2;

import com.training.homework8.task2.entity.Worker;
import com.training.homework8.task2.entity.WorkingCrew;
import com.training.homework8.task2.entity.WorkingCrewReport;
import com.training.homework8.task2.entity.WorkingProfessions;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class used to build object of the WorkingCrewReport class from data retrieved
 * from the object of the WorkingCrew class.
 *
 * @author Alexandr_Terehov
 */
public class WorkCrewReportBuilder {
    private WorkingCrewReport workingCrewReport;
    private WorkingCrew workingCrew;
    
    /**
     * List of workers related to the particular working crew.
     */
    private  List<Worker> workerList;
          
    /**
     * Constructor.
     *
     * @param workingCrew
     *            object of the WorkingCrew class.
     */
    public WorkCrewReportBuilder(WorkingCrew workingCrew) {
        if (workingCrew == null) {
            throw new NullPointerException("Instance of WorkingCrew class can't be null");
        }
        workingCrewReport = new WorkingCrewReport();
        workingCrewReport.setWorkingCrewId(workingCrew.getId());
        this.workingCrew = workingCrew;
        workerList = workingCrew.getWorkerList();
        buildReport();
    }
            
    /**
     * Method used to retrieve data from object of the WorkingCrew class and store
     * it into the object of the WorkingCrewReport class.
     */
    private void buildReport() {
        Set<WorkingProfessions> skillList;
        Map<WorkingProfessions, Integer> crewProfessionsInfo =
                workingCrewReport.getCrewProfessionsInfo();
        int professionQuantity;
        for (Worker worker : workerList) {
            skillList = worker.getSkillList();
            for (WorkingProfessions skill : skillList) {
                professionQuantity = crewProfessionsInfo.get(skill);
                workingCrewReport.setProfessionQuantity(skill, professionQuantity + 1);
            }
        }
        setReportCrewSalary();
    }
             
    /**
     * Method used to calculate overall salary of the working crew.
     */
    private void setReportCrewSalary() {
        int crewSalary = 0;
        for (Worker worker : workerList) {
            crewSalary += worker.getExpectedSalary();
        }
        workingCrewReport.setCrewSalary(crewSalary);
    }
          
    /**
     * @return object of the WorkingCrewReport class.
     */
    public WorkingCrewReport getWorkingCrewReport() {
        return workingCrewReport;
    }
}
