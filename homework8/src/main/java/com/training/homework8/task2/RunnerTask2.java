package com.training.homework8.task2;

import com.training.homework8.task2.entity.Worker;
import com.training.homework8.task2.entity.WorkingCrew;
import com.training.homework8.task2.entity.WorkingCrewReport;
import com.training.homework8.task2.entity.WorkingProfessions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask2 {
                      
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        Worker worker1Crew1 = new Worker("Worker1Crew1", 1500);
        worker1Crew1.addSkill(WorkingProfessions.BRICKLAYER);
        worker1Crew1.addSkill(WorkingProfessions.PLUMBER);
        worker1Crew1.addSkill(WorkingProfessions.BRICKLAYER);
        Worker worker2Crew1 = new Worker("Worker2Crew1", 1000);
        worker2Crew1.addSkill(WorkingProfessions.BRICKLAYER);
        Worker worker3Crew1 = new Worker("Worker3Crew1", 1000);
        worker3Crew1.addSkill(WorkingProfessions.ASSEMBLER);
        worker3Crew1.addSkill(WorkingProfessions.PAINTER);
        Worker worker4Crew1 = new Worker("Worker4Crew1", 1000);
        worker4Crew1.addSkill(WorkingProfessions.PAINTER);
        WorkingCrew workingCrew1 = new WorkingCrew(111);
        workingCrew1.addWorkerToList(worker1Crew1);
        workingCrew1.addWorkerToList(worker2Crew1);
        workingCrew1.addWorkerToList(worker3Crew1);
        workingCrew1.addWorkerToList(worker4Crew1);
                    
        Worker worker1Crew2 = new Worker("Worker1Crew2", 1500);
        worker1Crew2.addSkill(WorkingProfessions.BRICKLAYER);
        worker1Crew2.addSkill(WorkingProfessions.PLUMBER);
        worker1Crew2.addSkill(WorkingProfessions.BRICKLAYER);
        Worker worker2Crew2 = new Worker("Worker2Crew2", 1000);
        worker2Crew2.addSkill(WorkingProfessions.BRICKLAYER);
        Worker worker3Crew2 = new Worker("Worker3Crew2", 1000);
        worker3Crew2.addSkill(WorkingProfessions.ASSEMBLER);
        worker3Crew2.addSkill(WorkingProfessions.PAINTER);
        Worker worker4Crew2 = new Worker("Worker4Crew2", 1000);
        worker4Crew2.addSkill(WorkingProfessions.PAINTER);
        Worker worker5Crew2 = new Worker("Worker5Crew2", 1000);
        worker5Crew2.addSkill(WorkingProfessions.WELDER);
        Worker worker6Crew2 = new Worker("Worker6Crew2", 1000);
        worker6Crew2.addSkill(WorkingProfessions.SLINGER);
        Worker worker7Crew2 = new Worker("Worker7Crew2", 1000);
        worker7Crew2.addSkill(WorkingProfessions.CRANE_OPERATOR);
        Worker worker8Crew2 = new Worker("Worker8Crew2", 1000);
        worker8Crew2.addSkill(WorkingProfessions.ELECTRICIAN);
        Worker worker9Crew2 = new Worker("Worker7Crew2", 1000);
        worker9Crew2.addSkill(WorkingProfessions.PLASTERER);
        WorkingCrew workingCrew2 = new WorkingCrew(222);
        workingCrew2.addWorkerToList(worker1Crew2);
        workingCrew2.addWorkerToList(worker2Crew2);
        workingCrew2.addWorkerToList(worker3Crew2);
        workingCrew2.addWorkerToList(worker4Crew2);
        workingCrew2.addWorkerToList(worker5Crew2);
        workingCrew2.addWorkerToList(worker6Crew2);
        workingCrew2.addWorkerToList(worker7Crew2);
        workingCrew2.addWorkerToList(worker8Crew2);
        workingCrew2.addWorkerToList(worker9Crew2);
                               
        Worker worker1Crew3 = new Worker("Worker1Crew3", 1000);
        worker1Crew3.addSkill(WorkingProfessions.BRICKLAYER);
        worker1Crew3.addSkill(WorkingProfessions.PLUMBER);
        Worker worker2Crew3 = new Worker("Worker2Crew3", 1000);
        worker2Crew3.addSkill(WorkingProfessions.BRICKLAYER);
        Worker worker3Crew3 = new Worker("Worker3Crew3", 1000);
        worker3Crew3.addSkill(WorkingProfessions.ASSEMBLER);
        worker3Crew3.addSkill(WorkingProfessions.PAINTER);
        Worker worker4Crew3 = new Worker("Worker4Crew3", 1000);
        worker4Crew3.addSkill(WorkingProfessions.PAINTER);
        Worker worker5Crew3 = new Worker("Worker5Crew3", 1000);
        worker5Crew3.addSkill(WorkingProfessions.WELDER);
        Worker worker6Crew3 = new Worker("Worker6Crew3", 1000);
        worker6Crew3.addSkill(WorkingProfessions.SLINGER);
        Worker worker7Crew3 = new Worker("Worker7Crew3", 1000);
        worker7Crew3.addSkill(WorkingProfessions.CRANE_OPERATOR);
        Worker worker8Crew3 = new Worker("Worker8Crew3", 1000);
        worker8Crew3.addSkill(WorkingProfessions.ELECTRICIAN);
        Worker worker9Crew3 = new Worker("Worker7Crew3", 1000);
        worker9Crew3.addSkill(WorkingProfessions.PLASTERER);
        WorkingCrew workingCrew3 = new WorkingCrew(333);
        workingCrew3.addWorkerToList(worker1Crew3);
        workingCrew3.addWorkerToList(worker2Crew3);
        workingCrew3.addWorkerToList(worker3Crew3);
        workingCrew3.addWorkerToList(worker4Crew3);
        workingCrew3.addWorkerToList(worker5Crew3);
        workingCrew3.addWorkerToList(worker6Crew3);
        workingCrew3.addWorkerToList(worker7Crew3);
        workingCrew3.addWorkerToList(worker8Crew3);
        workingCrew3.addWorkerToList(worker9Crew3);
                
        Map<WorkingProfessions, Integer> tenderRequirements =
                new HashMap<WorkingProfessions, Integer>();
        tenderRequirements.put(WorkingProfessions.BRICKLAYER, 2);
        tenderRequirements.put(WorkingProfessions.PLASTERER, 1);
        tenderRequirements.put(WorkingProfessions.WELDER, 1);
        tenderRequirements.put(WorkingProfessions.ASSEMBLER, 1);
        tenderRequirements.put(WorkingProfessions.SLINGER, 1);
        tenderRequirements.put(WorkingProfessions.ELECTRICIAN, 1);
        tenderRequirements.put(WorkingProfessions.CRANE_OPERATOR, 1);
        tenderRequirements.put(WorkingProfessions.PAINTER, 1);
        tenderRequirements.put(WorkingProfessions.PLUMBER, 1);
               
        List<WorkingCrew> workingCrewList = new ArrayList<WorkingCrew>();
        workingCrewList.add(workingCrew1);
        workingCrewList.add(workingCrew2);
        workingCrewList.add(workingCrew3);
                              
        ConstructionTender constructionTender =
                new ConstructionTender(tenderRequirements, workingCrewList);
        outputTenderResults(constructionTender);
    }
                
    /**
     * Method used to output results of the construction tender to the console.
     *
     * @param constructionTender
     *            information about construction tender.
     */
    public static void outputTenderResults(ConstructionTender constructionTender) {
        WorkingCrewReport tenderWinner = constructionTender.getTenderResults();
        if (tenderWinner != null) {
            System.out.println("Congratulations! We have a winner!");
            System.out.println("Working Crew ID is: " + tenderWinner.getWorkingCrewId());
            System.out.println("Working Crew salary is: " + tenderWinner.getCrewSalary());
            System.out.println("Working Crew stuff info is:");
            System.out.println(tenderWinner.getCrewProfessionsInfo());
        } else {
            System.out.println("There is no tender winner. Construction has been closed");
        }
    }
}
