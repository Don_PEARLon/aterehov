package com.training.homework8.task2.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Class represents a Worker.
 *
 * @author Alexandr_Terehov
 */
public class Worker {
    private String name;
    private int expectedSalary;
          
    /**
     * List of workers skills.
     */
    private Set<WorkingProfessions> skillList;
           
    /**
     * Constructor.
     *
     * @param name
     *            name of the worker.
     * @param expectedSalary
     *            workers expected salary.
     * @throws IllegalArgumentException
     *             if expected salary less than 0 or equals 0.
     */
    public Worker(String name, int expectedSalary) {
        if (expectedSalary <= 0) {
            throw new IllegalArgumentException("Expected salary of worker: "
                    + name + " should be more than 0.");
        }
        this.name = name;
        this.expectedSalary = expectedSalary;
        skillList = new HashSet<WorkingProfessions>();
    }
        
    /**
     * @return name of the worker.
     */
    public String getName() {
        return name;
    }
              
    /**
     * 
     * @param name
     *            name of the worker to set.
     */
    public void setName(String name) {
        this.name = name;
    }
               
    /**
     * @return expected salary of the worker.
     */
    public int getExpectedSalary() {
        return expectedSalary;
    }
         
    /**
     * 
     * @param expectedSalary
     *            expected salary to set.
     */
    public void setExpectedSalary(int expectedSalary) {
        this.expectedSalary = expectedSalary;
    }
            
    /**
     * @return List of workers skill.
     */
    public Set<WorkingProfessions> getSkillList() {
        return skillList;
    }
                  
    /**
     * 
     * @param skillList
     *            list of workers skills to set.
     */
    public void setSkillList(Set<WorkingProfessions> skillList) {
        this.skillList = skillList;
    }
            
    /**
     * 
     * @param skill
     *            add a new skill to the workers skill list.
     */
    public void addSkill(WorkingProfessions skill) {
        skillList.add(skill);
    }
}