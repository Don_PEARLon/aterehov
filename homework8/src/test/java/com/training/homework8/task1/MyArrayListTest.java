package com.training.homework8.task1;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import com.training.homework8.task1.exception.ListIsFullException;

/**
 * Test suite for MyArrayList class.
 *
 * @author Alexandr_Terehov
 */
public class MyArrayListTest {
	private static List<Integer> myArrayList;
	private static Integer testInt;

	@Before
	public void initObjects() {
		myArrayList = new MyArrayList<Integer>(11);
		testInt = new Integer(12);
		myArrayList.add(5);
		myArrayList.add(10);
		myArrayList.add(testInt);
		myArrayList.add(15);
		myArrayList.add(20);
		myArrayList.add(17);
		myArrayList.add(24);
		myArrayList.add(68);
		myArrayList.add(45);
		myArrayList.add(3);
	}

	@Test
	public void testGet() {
		myArrayList.set(5, 22);
		Integer element = myArrayList.get(5);
		assertEquals(22, element, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetNegative() {
		Integer element = myArrayList.get(22);
	}

	@Test
	public void removeObject() {
		boolean condition = false;
		if ((myArrayList.remove(testInt)) && (myArrayList.size() == 9)) {
			condition = true;
		}
		assertTrue(condition);
	}

	@Test
	public void removeElementByIndex() {
		Integer element = myArrayList.remove(3);
		assertEquals(15, element, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveElementByIndexNegative() {
		Integer element = myArrayList.remove(20);
	}

	@Test
	public void removeClear() {
		myArrayList.clear();
		assertTrue(myArrayList.isEmpty());
	}

	@Test
	public void testAdd() {
		boolean condition1 = myArrayList.add(111);
		boolean condition2 = (myArrayList.size() == 11);
		boolean condition = false;
		if ((condition1 == true) && (condition2 == true)) {
			condition = true;
		}
		assertTrue(condition);
	}

	@Test
	public void testAddWithIndex() {
		myArrayList.add(3, 111);
		boolean condition = false;
		if ((myArrayList.get(3) == 111) && (myArrayList.size() == 11)) {
			condition = true;
		}
		assertTrue(condition);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testAddWithNegativeIndex() {
		myArrayList.add(20, 111);
	}

	@Test
	public void testAddNegative() {
		myArrayList.add(111);
		boolean condition1 = myArrayList.add(200);
		boolean condition2 = (myArrayList.size() == 11);
		boolean condition = false;
		if ((condition1 == false) && (condition2 == true)) {
			condition = true;
		}
		assertTrue(condition);
	}

	@Test
	public void testByIndexAddNegative() {
		myArrayList.add(3, 111);
		myArrayList.add(4, 200);
		boolean condition1 = (myArrayList.get(4) == 200);
		boolean condition2 = (myArrayList.size() == 11);
		boolean condition = false;
		if ((condition1 == false) && (condition2 == true)) {
			condition = true;
		}
		assertTrue(condition);
	}

	@Test
	public void testContains() {
		boolean condition = myArrayList.contains(testInt);
		assertTrue(condition);
	}

	@Test
	public void testIndexOf() {
		int index = myArrayList.indexOf(testInt);
		assertEquals(2, index, 0);
	}

	@Test
	public void testIterator() {
		int count = 0;
		Integer val = 0;
		Iterator<Integer> iterator = myArrayList.iterator();
		while (iterator.hasNext()) {
			count++;
			val = iterator.next();
		}
		boolean condition = false;
		if ((count == 10) && (val == 3)) {
			condition = true;
		}
		assertTrue(condition);
	}
}
