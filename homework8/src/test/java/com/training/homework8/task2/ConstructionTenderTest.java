package com.training.homework8.task2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.training.homework8.task2.entity.Worker;
import com.training.homework8.task2.entity.WorkingCrew;
import com.training.homework8.task2.entity.WorkingCrewReport;
import com.training.homework8.task2.entity.WorkingProfessions;

/**
 * Test suite for ConstructionTender class.
 *
 * @author Alexandr_Terehov
 */
public class ConstructionTenderTest {
	private static Worker worker1Crew1;
	private static Worker worker2Crew1;
	private static Worker worker3Crew1;
	private static Worker worker4Crew1;

	private static Worker worker1Crew2;
	private static Worker worker2Crew2;
	private static Worker worker3Crew2;
	private static Worker worker4Crew2;
	private static Worker worker5Crew2;
	private static Worker worker6Crew2;
	private static Worker worker7Crew2;
	private static Worker worker8Crew2;
	private static Worker worker9Crew2;

	private static Worker worker1Crew3;
	private static Worker worker2Crew3;
	private static Worker worker3Crew3;
	private static Worker worker4Crew3;
	private static Worker worker5Crew3;
	private static Worker worker6Crew3;
	private static Worker worker7Crew3;
	private static Worker worker8Crew3;
	private static Worker worker9Crew3;

	private static WorkingCrew workingCrew1;
	private static WorkingCrew workingCrew2;
	private static WorkingCrew workingCrew3;

	private static List<WorkingCrew> workingCrewList;

	private static Map<WorkingProfessions, Integer> tenderRequirements1;
	private static Map<WorkingProfessions, Integer> tenderRequirements2;

	@Before
	public void initObjects() {
		worker1Crew1 = new Worker("Worker1Crew1", 1500);
		worker1Crew1.addSkill(WorkingProfessions.BRICKLAYER);
		worker1Crew1.addSkill(WorkingProfessions.PLUMBER);
		worker1Crew1.addSkill(WorkingProfessions.BRICKLAYER);
		worker2Crew1 = new Worker("Worker2Crew1", 1000);
		worker2Crew1.addSkill(WorkingProfessions.BRICKLAYER);
		worker3Crew1 = new Worker("Worker3Crew1", 1000);
		worker3Crew1.addSkill(WorkingProfessions.ASSEMBLER);
		worker3Crew1.addSkill(WorkingProfessions.PAINTER);
		worker4Crew1 = new Worker("Worker4Crew1", 1000);
		worker4Crew1.addSkill(WorkingProfessions.PAINTER);

		workingCrew1 = new WorkingCrew(111);
		workingCrew1.addWorkerToList(worker1Crew1);
		workingCrew1.addWorkerToList(worker2Crew1);
		workingCrew1.addWorkerToList(worker3Crew1);
		workingCrew1.addWorkerToList(worker4Crew1);

		worker1Crew2 = new Worker("Worker1Crew2", 1500);
		worker1Crew2.addSkill(WorkingProfessions.BRICKLAYER);
		worker1Crew2.addSkill(WorkingProfessions.PLUMBER);
		worker1Crew2.addSkill(WorkingProfessions.BRICKLAYER);
		worker2Crew2 = new Worker("Worker2Crew2", 1000);
		worker2Crew2.addSkill(WorkingProfessions.BRICKLAYER);
		worker3Crew2 = new Worker("Worker3Crew2", 1000);
		worker3Crew2.addSkill(WorkingProfessions.ASSEMBLER);
		worker3Crew2.addSkill(WorkingProfessions.PAINTER);
		worker4Crew2 = new Worker("Worker4Crew2", 1000);
		worker4Crew2.addSkill(WorkingProfessions.PAINTER);
		worker5Crew2 = new Worker("Worker5Crew2", 1000);
		worker5Crew2.addSkill(WorkingProfessions.WELDER);
		worker6Crew2 = new Worker("Worker6Crew2", 1000);
		worker6Crew2.addSkill(WorkingProfessions.SLINGER);
		worker7Crew2 = new Worker("Worker7Crew2", 1000);
		worker7Crew2.addSkill(WorkingProfessions.CRANE_OPERATOR);
		worker8Crew2 = new Worker("Worker8Crew2", 1000);
		worker8Crew2.addSkill(WorkingProfessions.ELECTRICIAN);
		worker9Crew2 = new Worker("Worker7Crew2", 1000);
		worker9Crew2.addSkill(WorkingProfessions.PLASTERER);

		workingCrew2 = new WorkingCrew(222);
		workingCrew2.addWorkerToList(worker1Crew2);
		workingCrew2.addWorkerToList(worker2Crew2);
		workingCrew2.addWorkerToList(worker3Crew2);
		workingCrew2.addWorkerToList(worker4Crew2);
		workingCrew2.addWorkerToList(worker5Crew2);
		workingCrew2.addWorkerToList(worker6Crew2);
		workingCrew2.addWorkerToList(worker7Crew2);
		workingCrew2.addWorkerToList(worker8Crew2);
		workingCrew2.addWorkerToList(worker9Crew2);

		worker1Crew3 = new Worker("Worker1Crew3", 1000);
		worker1Crew3.addSkill(WorkingProfessions.BRICKLAYER);
		worker1Crew3.addSkill(WorkingProfessions.PLUMBER);
		worker2Crew3 = new Worker("Worker2Crew3", 1000);
		worker2Crew3.addSkill(WorkingProfessions.BRICKLAYER);
		worker3Crew3 = new Worker("Worker3Crew3", 1000);
		worker3Crew3.addSkill(WorkingProfessions.ASSEMBLER);
		worker3Crew3.addSkill(WorkingProfessions.PAINTER);
		worker4Crew3 = new Worker("Worker4Crew3", 1000);
		worker4Crew3.addSkill(WorkingProfessions.PAINTER);
		worker5Crew3 = new Worker("Worker5Crew3", 1000);
		worker5Crew3.addSkill(WorkingProfessions.WELDER);
		worker6Crew3 = new Worker("Worker6Crew3", 1000);
		worker6Crew3.addSkill(WorkingProfessions.SLINGER);
		worker7Crew3 = new Worker("Worker7Crew3", 1000);
		worker7Crew3.addSkill(WorkingProfessions.CRANE_OPERATOR);
		worker8Crew3 = new Worker("Worker8Crew3", 1000);
		worker8Crew3.addSkill(WorkingProfessions.ELECTRICIAN);
		worker9Crew3 = new Worker("Worker7Crew3", 1000);
		worker9Crew3.addSkill(WorkingProfessions.PLASTERER);

		workingCrew3 = new WorkingCrew(333);
		workingCrew3.addWorkerToList(worker1Crew3);
		workingCrew3.addWorkerToList(worker2Crew3);
		workingCrew3.addWorkerToList(worker3Crew3);
		workingCrew3.addWorkerToList(worker4Crew3);
		workingCrew3.addWorkerToList(worker5Crew3);
		workingCrew3.addWorkerToList(worker6Crew3);
		workingCrew3.addWorkerToList(worker7Crew3);
		workingCrew3.addWorkerToList(worker8Crew3);
		workingCrew3.addWorkerToList(worker9Crew3);

		workingCrewList = new ArrayList<WorkingCrew>();
		workingCrewList.add(workingCrew1);
		workingCrewList.add(workingCrew2);
		workingCrewList.add(workingCrew3);

		tenderRequirements1 = new HashMap<WorkingProfessions, Integer>();
		tenderRequirements1.put(WorkingProfessions.BRICKLAYER, 2);
		tenderRequirements1.put(WorkingProfessions.PLASTERER, 1);
		tenderRequirements1.put(WorkingProfessions.WELDER, 1);
		tenderRequirements1.put(WorkingProfessions.ASSEMBLER, 1);
		tenderRequirements1.put(WorkingProfessions.SLINGER, 1);
		tenderRequirements1.put(WorkingProfessions.ELECTRICIAN, 1);
		tenderRequirements1.put(WorkingProfessions.CRANE_OPERATOR, 1);
		tenderRequirements1.put(WorkingProfessions.PAINTER, 1);
		tenderRequirements1.put(WorkingProfessions.PLUMBER, 1);

		tenderRequirements2 = new HashMap<WorkingProfessions, Integer>();
		tenderRequirements2.put(WorkingProfessions.BRICKLAYER, 4);
		tenderRequirements2.put(WorkingProfessions.PLASTERER, 1);
		tenderRequirements2.put(WorkingProfessions.WELDER, 1);
		tenderRequirements2.put(WorkingProfessions.ASSEMBLER, 1);
		tenderRequirements2.put(WorkingProfessions.SLINGER, 1);
		tenderRequirements2.put(WorkingProfessions.ELECTRICIAN, 1);
		tenderRequirements2.put(WorkingProfessions.CRANE_OPERATOR, 1);
		tenderRequirements2.put(WorkingProfessions.PAINTER, 1);
		tenderRequirements2.put(WorkingProfessions.PLUMBER, 1);
	}

	@Test
	public void testGetTenderResults() {
		ConstructionTender constructionTender = new ConstructionTender(tenderRequirements1, workingCrewList);
		WorkingCrewReport crewReport = constructionTender.getTenderResults();
		int winnerCrewId = crewReport.getWorkingCrewId();
		assertEquals(333, winnerCrewId, 0);
	}

	@Test
	public void testGetTenderResultsNegative() {
		ConstructionTender constructionTender = new ConstructionTender(tenderRequirements2, workingCrewList);
		WorkingCrewReport crewReport = constructionTender.getTenderResults();
		boolean condition = (crewReport == null);
		assertTrue(condition);
	}
	
	@Test(expected = NullPointerException.class)
	public void createConstrTenderNegative() {
		ConstructionTender constructionTender = new ConstructionTender(tenderRequirements1, null);
	}
}