package com.training.homework8.task2;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.training.homework8.task2.entity.Worker;
import com.training.homework8.task2.entity.WorkingCrew;

/**
 * Test suite for WorkerCrew class.
 *
 * @author Alexandr_Terehov
 */
public class WorkingCrewTest {
	private static WorkingCrew workingCrew;
	private static Worker worker;

	@Before
	public void initObjects() {
		workingCrew = new WorkingCrew(111);
		worker = new Worker("worker", 100);
	}

	@Test
	public void testAddWorkerToList() {
		workingCrew.addWorkerToList(worker);
		boolean condition = workingCrew.getWorkerList().contains(worker);
		assertTrue(condition);
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void createWorkerNegative() {
		WorkingCrew workingCrew1 = new WorkingCrew(-111);
	}
}