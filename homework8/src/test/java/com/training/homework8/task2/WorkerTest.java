package com.training.homework8.task2;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.training.homework8.task2.entity.Worker;
import com.training.homework8.task2.entity.WorkingProfessions;

/**
 * Test suite for Worker class.
 *
 * @author Alexandr_Terehov
 */
public class WorkerTest {
	private static Worker worker;

	@Before
	public void initObjects() {
		worker = new Worker("worker", 100);
	}

	@Test
	public void testAddSkill() {
		WorkingProfessions skill = WorkingProfessions.BRICKLAYER;
		worker.addSkill(skill);
		boolean condition = worker.getSkillList().contains(skill);
		assertTrue(condition);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void createWorkerNegative() {
		Worker worker1 = new Worker("worker", -100);
	}
}