package com.training.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Filter used during process of authorization into online-shop, checks
 * the correctness of the user name input.
 */
public class UserNameFilter implements Filter {
    private String userName;
    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }
    
    /**
     * Handles {@link HttpServlet} doFilter Method.
     *
     * @param request
     *            the {@link ServletRequest}
     * @param response
     *            the {@link ServletResponse}
     * @param chain
     *            the {@link FilterChain}
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest requestHttp = (HttpServletRequest) request;
        HttpSession session = requestHttp.getSession();
        userName = request.getParameter("userName");
        if (session.getAttribute("user") == null && !checkName(userName)) {
            final String body = "<html>"
                              + "<head>"
                              + "<style>"
                              + ".layer1 {"
                              + "margin-left: 10%;"
                              + "}"
                              + "</style>"
                              + "</head>"
                              + "<body>"
                              + "<div class='layer1'>"
                              + "<h1>Oops!</h1><br>"
                              + "<h4>Error occured while authentication process</h4>"
                              + "<h4>Check if user name is entered correctly</h4>"
                              + "<br>"
                              + "<h4><a href='/online-shop'> Start page</a></h4>"
                              + "</div>"
                              + "</body>"   
                              + "</html>";
            PrintWriter out = response.getWriter();
            out.println(body);
        } else {
            chain.doFilter(request, response);
        }
    }
    
    /**
     * Method used to check the correctness of the user name.
     *
     * @param name
     *            name to check.
     * @return boolean result of the checking.
     */
    private boolean checkName(String name) {
        Pattern pattern = Pattern.compile("^(?!\\s*$).+");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }
      
    @Override
    public void destroy() {
       
    }
}