package com.training.servlet;

import com.training.entity.ShopItem;
import com.training.entity.User;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet used for confirmation of the order, provides calculation of the total
 * order price.
 */
public class SubmitOrderServlet extends HttpServlet {
    private static final long serialVersionUID = -1882926836615346135L;
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession();
        final User user = (User) session.getAttribute("user");
        final String orderStr = getOrderStr(user);
        final String totalPriceStr = getTotalPriceStr(user);
        PrintWriter out = response.getWriter();
        final String body = "<html>" + "<body>" + "<h1 align=center>Dear "
                + user.getName() + ", your order!</h1>"
                + orderStr + "<br>" + totalPriceStr + "</body>" + "</html>";
        out.println(body);
    }
      
    /**
     * Method provides creation of string in HTML format from user's list of selected
     * shop items.
     *
     * @param user
     *            user of the online shop.
     */
    private String getOrderStr(User user) {
        String orderStr = "";
        int itemNumber = 1;
        for (ShopItem item : user.getItemList()) {
            orderStr += "<h4 align=center>" + itemNumber + ") " + item.getName()
                + "   " + item.getPrice() + "$" + "</h4>";
            itemNumber++;
        }
        return orderStr;
    }
     
    /**
     * Method provides creation of string in HTML format contains information about
     * total sum of user's orders from the online shop.
     *
     * @param user
     *            user of the online shop.
     */
    private String getTotalPriceStr(User user) {
        double totalPrice = 0;
        for (ShopItem item : user.getItemList()) {
            totalPrice += item.getPrice();
        }
        String totalPriceStr = "<h4 align=center>Total: "
                + String.valueOf(totalPrice) + "$" + "</h4>";
        return totalPriceStr;
    }
}
