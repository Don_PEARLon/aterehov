package com.training.servlet;

import com.training.entity.ShopItem;
import com.training.entity.User;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet used during process of shop item selection for the order. 
 */
public class EnterShopServlet extends HttpServlet {
    private static final long serialVersionUID = -6881786289314956801L;
    
    /**
     * Map that contains information about items of the online-shop. Name of the
     * item is a key and price is a value.
     */
    private Map<String, Double> shopItemMap;
    private String shopItemOptions;
    private String orderStr;
     
    @Override
    public void init() {
        shopItemMap = new HashMap<>();
        shopItemMap.put("Mobile Phone", 10.0);
        shopItemMap.put("Book", 5.5);
        shopItemMap.put("Laptop", 310.15);
        shopItemMap.put("TV", 250.0);
        for (Map.Entry<String, Double> entry : shopItemMap.entrySet()) {
            shopItemOptions += "<option value='" + entry.getKey()
                    + "'>" + entry.getKey() + " " + entry.getValue() + "$";
        }
    }
    
    /**
     * Handles {@link HttpServlet} POST Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession();
        final String userName;
        final User user;
        if (session.getAttribute("user") == null) {
            userName = request.getParameter("userName");
            user = new User(userName);
            session.setAttribute("user", user);
            orderStr = "<h2 align=center>Make your order</h2>";
        } else {
            user = (User) session.getAttribute("user");
            userName = user.getName();
            String shopItemName = request.getParameter("shopItemName");
            if (shopItemName != null) {
                ShopItem shopItem = getShopItem(shopItemName);
                user.addItemToList(shopItem);
            }
            if (user.getItemList().isEmpty()) {
                orderStr = "<h2 align=center>Make your order</h2>";
            } else {
                orderStr = "<h4 align=center>You have already chosen:</h4>";
                int itemNumber = 1;
                for (ShopItem item: user.getItemList()) {
                    orderStr += "<h4 align=center>" + itemNumber + ") " + item.getName()
                        + "   " + item.getPrice() + "$" + "</h4>";
                    itemNumber++;
                }
            }
        }
        PrintWriter out = response.getWriter();
        final String body = "<html>"
                          + "<body>"
                          + "<h1 align=center>Hello "
                          + userName + "!</h1>"
                          + orderStr
                          + "<form align=center action='enter' method='post'>"
                          + "<select input type='text' name='shopItemName'>"
                          + shopItemOptions
                          + "</select>"
                          + "<input type='submit' value='Add Item'>"
                          + "</form>"
                          + "<form align=center action='order' method='post'>"
                          + "<input type='submit' value='Submit'>"
                          + "</form>"
                          + "</body>"   
                          + "</html>";
        out.println(body);
    }
 
    /**
     * Creates instance of the ShopItem class from the input String.
     *
     * @param shopItemName
     *            string representation of the shop item.
     * @return instance of the ShopItem class.
     */
    private ShopItem getShopItem(String shopItemName) {
        ShopItem shopItem = null;
        for (Map.Entry<String, Double> entry : shopItemMap.entrySet()) {
            if (entry.getKey().equals(shopItemName)) {
                shopItem = new ShopItem(entry.getKey(), entry.getValue());
            }
        }
        return shopItem;
    }
}