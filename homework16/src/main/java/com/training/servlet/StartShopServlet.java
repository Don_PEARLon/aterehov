package com.training.servlet;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet starts an applications, provides logging in to online-shop.
 */
public class StartShopServlet extends HttpServlet {
    private static final long serialVersionUID = -4281588832138964034L;
    
    /**
     * Handles {@link HttpServlet} GET Method.
     *
     * @param request
     *            the {@link HttpServletRequest}
     * @param response
     *            the {@link HttpServletResponse}
     * @throws IOException
     *             the IO exception
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession();
        session.invalidate();
        PrintWriter out = response.getWriter();
        final String body = "<html>"
                          + "<body>"
                          + "<h2 align=center>Welcome to Online Shop</h2>"
                          + "<form align=center action='enter' method='post'>"
                          + "<input align=center type='text' name='userName'"
                          + " placeholder='Enter your name'><br>"
                          + "<br><input align=center type='submit' value='Enter'><br>"
                          + "<br><input align=center type='checkbox'"
                          + " name='termsAgreement' value='agree'/>"
                          + "I agree with terms of service"
                          + "</form>"
                          + "</body>"     
                          + "</html>";
        out.println(body);
    }
}
