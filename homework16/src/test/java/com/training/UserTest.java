package com.training;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.training.entity.ShopItem;
import com.training.entity.User;

/**
 * Test suite for User class.
 *
 * @author Alexandr_Terehov
 */
public class UserTest {
	private static User user;
	private static ShopItem item;

	@Before
	public void initObjects() {
		user = new User("User1");
		item = new ShopItem("Item1", 100);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUserNegative() {
		User user1 = new User("  ");
	}

	@Test
	public void testGetName() {
		String expectedName = "User1";
		assertTrue(expectedName.equals(user.getName()));
	}

	@Test
	public void testSetName() {
		String expectedName = "User2";
		user.setName("User2");
		assertTrue(expectedName.equals(user.getName()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNameNegative() {
		user.setName("  ");
	}

	@Test
	public void testAddItemToList() {
		user.addItemToList(item);
		List<ShopItem> itemList = user.getItemList();
		assertTrue(itemList.contains(item));
	}

	@Test(expected = NullPointerException.class)
	public void testAddItemToListNegative() {
		item = null;
		user.addItemToList(item);
	}
}
