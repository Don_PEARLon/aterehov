package com.training.homework12.task1;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.training.homework12.task1.entity.Worker;
import com.training.homework12.task1.entity.WorkingCrew;
import com.training.homework12.task1.entity.WorkingCrewReport;
import com.training.homework12.task1.entity.WorkingProfessions;

/**
 * Test suite for WorkingCrewReport class.
 *
 * @author Alexandr_Terehov
 */
public class WorkingCrewReportTest {
	private static Worker worker1;
	private static Worker worker2;
	private static Worker worker3;
	private static Worker worker4;
	private static WorkingCrew workingCrew;
	private static WorkCrewReportBuilder crewReportBuilder;
	private static WorkingCrewReport workingCrewReport;

	@Before
	public void initObjects() {
		worker1 = new Worker("Worker1", 1500);
		worker1.addSkill(WorkingProfessions.BRICKLAYER);
		worker1.addSkill(WorkingProfessions.PLUMBER);
		worker1.addSkill(WorkingProfessions.BRICKLAYER);
		worker2 = new Worker("Worker2", 1000);
		worker2.addSkill(WorkingProfessions.BRICKLAYER);
		worker3 = new Worker("Worker3", 1000);
		worker3.addSkill(WorkingProfessions.ASSEMBLER);
		worker3.addSkill(WorkingProfessions.PAINTER);
		worker4 = new Worker("Worker", 1000);
		worker4.addSkill(WorkingProfessions.PAINTER);
		workingCrew = new WorkingCrew(111);
		workingCrew.addWorkerToList(worker1);
		workingCrew.addWorkerToList(worker2);
		workingCrew.addWorkerToList(worker3);
		workingCrew.addWorkerToList(worker4);
		crewReportBuilder = new WorkCrewReportBuilder(workingCrew);
		workingCrewReport = crewReportBuilder.getWorkingCrewReport();
	}

	@Test
	public void testCrewSalary() {
		int crewSalary = workingCrewReport.getCrewSalary();
		assertEquals(4500, crewSalary, 0);
	}

	@Test
	public void testCrewProfessionsInfo() {
		Map<WorkingProfessions, Integer> crewProfessionsInfo;
		crewProfessionsInfo = workingCrewReport.getCrewProfessionsInfo();
		int bricklayerQuantity = crewProfessionsInfo.get(WorkingProfessions.BRICKLAYER);
		assertEquals(2, bricklayerQuantity, 0);
	}
}
