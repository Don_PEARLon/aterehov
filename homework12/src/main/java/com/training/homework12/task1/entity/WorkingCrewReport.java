package com.training.homework12.task1.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Class represents a report related to the working crew. This report contains
 * data about workers and their professional skills. Also this report contains
 * overall salary of the working crew.
 *
 * @author Alexandr_Terehov
 */
public class WorkingCrewReport {
    private int workingCrewId;
    private int crewSalary;
    Map<WorkingProfessions, Integer> crewProfessionsInfo;
    
    /**
     * Constructor. Initializes crewProfessionsInfo map with information about
     * working professions related to working crew members.
     */
    public WorkingCrewReport() {
        crewProfessionsInfo = new HashMap<WorkingProfessions, Integer>();
        for (WorkingProfessions profession : WorkingProfessions.values()) {
            crewProfessionsInfo.put(profession, 0);
        }
    }
                  
    /**
     * @return ID of the working crew.
     */
    public int getWorkingCrewId() {
        return workingCrewId;
    }
                 
    /**
     * 
     * @param workingCrewId
     *            ID of the working crew to set.
     */
    public void setWorkingCrewId(int workingCrewId) {
        if (workingCrewId <= 0) {
            throw new IllegalArgumentException("Id of the working crew must be more than 0");
        }
        this.workingCrewId = workingCrewId;
    }
                     
    /**
     * @return overall salary of the working crew.
     */
    public int getCrewSalary() {
        return crewSalary;
    }
                          
    /**
     * 
     * @param crewSalary
     *            overall salary of the working crew to set.
     */
    public void setCrewSalary(int crewSalary) {
        if (crewSalary <= 0) {
            throw new IllegalArgumentException("Crew salary should be more than 0.");
        }
        this.crewSalary = crewSalary;
    }
                         
    /**
     * @return Map contains information about working professions and quantity of
     *         workers related to this professions.
     */
    public Map<WorkingProfessions, Integer> getCrewProfessionsInfo() {
        return crewProfessionsInfo;
    }
            
    /**
     * 
     * @param crewProfessionsInfo
     *            Map contains information about working professions and quantity of
     *            workers related to this professions.
     */
    public void setCrewProfessionsInfo(Map<WorkingProfessions, Integer> crewProfessionsInfo) {
        this.crewProfessionsInfo = crewProfessionsInfo;
    }
                
    /**
     * This method modifies information about quantity of workers related to
     * particular a profession.
     *
     * @param profession
     *            working profession.
     * @param quantity
     *            quantity of workers.
     */
    public void setProfessionQuantity(WorkingProfessions profession, int quantity) {
        crewProfessionsInfo.put(profession, quantity);
    }
}
