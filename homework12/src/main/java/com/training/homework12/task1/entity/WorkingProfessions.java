package com.training.homework12.task1.entity;

/**
 * Enum used to represent different working professions.
 *
 * @author Alexandr_Terehov
 */
public enum WorkingProfessions {
    /**
     * Bricklayer working profession.
     */
    BRICKLAYER,
    /**
     * Plasterer working profession.
     */
    PLASTERER,
    /**
     * Welder working profession.
     */
    WELDER,
    /**
     * Assembler working profession.
     */
    ASSEMBLER,
    /**
     * Slinger working profession.
     */
    SLINGER,
    /**
     * Electrician working profession.
     */
    ELECTRICIAN,
    /**
     * Crane operator working profession.
     */
    CRANE_OPERATOR,
    /**
     * Painter working profession.
     */
    PAINTER,
    /**
     * Plumber working profession.
     */
    PLUMBER;
}
