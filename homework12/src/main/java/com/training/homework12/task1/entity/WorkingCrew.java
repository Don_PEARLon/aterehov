package com.training.homework12.task1.entity;

import java.util.ArrayList;
import java.util.List;

public class WorkingCrew {
    private int id;
                               
    /**
     * List of workers related to this working crew.
     */
    private List<Worker> workerList;
            
    /**
     * Constructor.
     *
     * @param id
     *            id of the working crew.
     */
    public WorkingCrew(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("Id of the working crew must be more than 0");
        }
        this.id = id;
        workerList = new ArrayList<Worker>();
    }
                                
    /**
     * @return id of the working crew.
     */
    public int getId() {
        return id;
    }
              
    /**
     * 
     * @param id
     *            id of the working crew to set.
     */
    public void setId(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("Id of the working crew must be more than 0");
        }
        this.id = id;
    }
               
    /**
     * @return list of workers related to this working crew.
     */
    public List<Worker> getWorkerList() {
        return workerList;
    }
               
    /**
     * 
     * @param workerList
     *            list of workers to set.
     */
    public void setWorkerList(List<Worker> workerList) {
        this.workerList = workerList;
    }
              
    /**
     * 
     * @param worker
     *            add a new worker to the crew workers list.
     */
    public void addWorkerToList(Worker worker) {
        workerList.add(worker);
    }
}
