package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test suite for FactorialFor class.
 *
 * @author Alexandr_Terehov
 */
public class FactorialForTest {

	@Test
	public void testFactorialOfZero() {
		Factorial factorial = new FactorialFor(0);
		assertEquals(1, factorial.getFactorial(), 0);
	}

	@Test
	public void testFactorial() {
		Factorial factorial = new FactorialFor(6);
		assertEquals(720, factorial.getFactorial(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialNegative() {
		Factorial factorial = new FactorialFor(-1);
		assertEquals(720, factorial.getFactorial(), 0);
	}
}
