package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for FibonacciFactory class.
 *
 * @author Alexandr_Terehov
 */
public class FibonacciFactoryTest {
	private static FibonacciFactory fibonacciFactory;

	@Before
	public void initFactory() {
		fibonacciFactory = new FibonacciFactory();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialFactoryExc() {
		fibonacciFactory.getFibonacciInstance(4, 4);
	}

	@Test
	public void testFactorialFactoryInst() {
		boolean condition = fibonacciFactory.getFibonacciInstance(3, 4) instanceof FibonacciFor;
		assertTrue(condition);
	}
}
