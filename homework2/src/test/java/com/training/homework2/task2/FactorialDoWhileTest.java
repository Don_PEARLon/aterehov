package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test suite for FactorialDoWhile class.
 *
 * @author Alexandr_Terehov
 */
public class FactorialDoWhileTest {

	@Test
	public void testFactorialOfZero() {
		Factorial factorial = new FactorialDoWhile(0);
		assertEquals(1, factorial.getFactorial(), 0);
	}

	@Test
	public void testFactorial() {
		Factorial factorial = new FactorialDoWhile(9);
		assertEquals(362880, factorial.getFactorial(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialNegative() {
		Factorial factorial = new FactorialDoWhile(-1);
		assertEquals(362880, factorial.getFactorial(), 0);
	}
}
