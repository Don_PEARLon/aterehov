package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test suite for FibonacciFor class.
 *
 * @author Alexandr_Terehov
 */
public class FibonacciForTest {

	@Test
	public void testFibonacci() {
		Fibonacci fibonacci = new FibonacciFor(16);
		assertEquals(610, fibonacci.getFibonacciArray()[15], 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFibonacciNegative() {
		Fibonacci fibonacci = new FibonacciFor(-1);
		assertEquals(89, fibonacci.getFibonacciArray()[11], 0);
	}

	@Test
	public void testFibonacciThird() {
		Fibonacci fibonacci = new FibonacciFor(3);
		assertEquals(1, fibonacci.getFibonacciArray()[2], 0);
	}
}
