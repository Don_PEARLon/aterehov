package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test suite for FibonacciWhile class.
 *
 * @author Alexandr_Terehov
 */
public class FibonacciWhileTest {

	@Test
	public void testFibonacci() {
		Fibonacci fibonacci = new FibonacciWhile(12);
		assertEquals(89, fibonacci.getFibonacciArray()[11], 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFibonacciNegative() {
		Fibonacci fibonacci = new FibonacciWhile(-1);
		assertEquals(89, fibonacci.getFibonacciArray()[11], 0);
	}

	@Test
	public void testFibonacciThird() {
		Fibonacci fibonacci = new FibonacciWhile(3);
		assertEquals(1, fibonacci.getFibonacciArray()[2], 0);
	}
}
