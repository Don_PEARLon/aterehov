package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test suite for FactorialWhile class.
 *
 * @author Alexandr_Terehov
 */
public class FactorialWhileTest {

	@Test
	public void testFactorialOfZero() {
		Factorial factorial = new FactorialWhile(0);
		assertEquals(1, factorial.getFactorial(), 0);
	}

	@Test
	public void testFactorial() {
		Factorial factorial = new FactorialWhile(8);
		assertEquals(40320, factorial.getFactorial(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialNegative() {
		Factorial factorial = new FactorialWhile(-1);
		assertEquals(40320, factorial.getFactorial(), 0);
	}
}
