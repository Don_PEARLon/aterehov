package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for FactorialFactory class.
 *
 * @author Alexandr_Terehov
 */
public class FactorialFactoryTest {
	private static FactorialFactory factorialFactory;

	@Before
	public void initFactory() {
		factorialFactory = new FactorialFactory();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialFactoryExc() {
		factorialFactory.getFactorialInstance(4, 4);
	}

	@Test
	public void testFactorialFactoryInst() {
		boolean condition = factorialFactory.getFactorialInstance(1, 4) instanceof FactorialWhile;
		assertTrue(condition);
	}
}
