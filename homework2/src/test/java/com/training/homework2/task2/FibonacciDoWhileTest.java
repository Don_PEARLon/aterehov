package com.training.homework2.task2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test suite for FibonacciDoWhile class.
 *
 * @author Alexandr_Terehov
 */
public class FibonacciDoWhileTest {

	@Test
	public void testFibonacci() {
		Fibonacci fibonacci = new FibonacciDoWhile(9);
		assertEquals(21, fibonacci.getFibonacciArray()[8], 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFibonacciNegative() {
		Fibonacci fibonacci = new FibonacciDoWhile(-1);
		assertEquals(21, fibonacci.getFibonacciArray()[8], 0);
	}

	@Test
	public void testFibonacciThird() {
		Fibonacci fibonacci = new FibonacciDoWhile(3);
		assertEquals(1, fibonacci.getFibonacciArray()[2], 0);
	}
}
