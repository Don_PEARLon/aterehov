package com.training.homework2.task1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
/**
 * Test for Task1 class.
 *
 * @author Alexandr_Terehov
 */
public class Task1Test {

    @Test
    public void testCalculation() {
        assertEquals(140.3677, Task1.taskCalculation(4, 2, 1.3, 3.2), 0.00001);
    }
}
