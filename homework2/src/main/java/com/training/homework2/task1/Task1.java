package com.training.homework2.task1;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

import java.util.Scanner;

/**
 * Main application class.
 *
 * @author Alexandr_Terehov
 */
public class Task1 {
    /**
    * Application start point.
    *
    * @param args
    *            Command line arguments.
    */
    public static void main(String[] args) {
        // int a=Integer.valueOf(args[0]);
        // int p=Integer.valueOf(args[1]);
        // double m1=Double.valueOf(args[2]);
        // double m2=Double.valueOf(args[3]);
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter 4 params for calcualtion");
        int a = Integer.valueOf(sc.nextLine());
        int p = Integer.valueOf(sc.nextLine());
        double m1 = Double.valueOf(sc.nextLine());
        double m2 = Double.valueOf(sc.nextLine());
        double g = taskCalculation(a, p, m1, m2);
        System.out.println("Result = " + g);
    }

    /**
     * Method used to calculate the required formula
     *
     * @param a
     *            first number.
     * @param p
     *            second number.
     * @param m1
     *            third number.
     * @param m2
     *            fourth number.
     * @return result of formula calculation.
     */
    public static double taskCalculation(int a, int p, double m1, double m2) {
        return 4 * pow((PI), 2) * ((pow(a, 3)) / ((pow(p, 2)) * (m1 + m2)));
    }
}
