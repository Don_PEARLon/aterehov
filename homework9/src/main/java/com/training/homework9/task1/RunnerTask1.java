package com.training.homework9.task1;

import java.util.Arrays;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask1 {
       
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        Integer[] array = { 12, 104, 22, 63, 26, 1003, 56, 95, 201 };
        System.out.println("Before sorting: " + Arrays.toString(array));
        Arrays.sort(array, new SumOfDigitsComparator());
        System.out.println("After sorting: " + Arrays.toString(array));
    }
}
