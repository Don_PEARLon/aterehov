package com.training.homework9.task1;

import java.util.Comparator;

/**
 * Class represents a comparator for integer values. The comparison is made by
 * the sum of digits of the number.
 *
 * @author Alexandr_Terehov
 */
public class SumOfDigitsComparator implements Comparator<Integer> {
          
    /**
     * Method used for comparison of two numbers by sum of their digits.
     * 
     * @param number1
     *            first number to compare.
     * @param number2
     *            second number to compare.
     */
    @Override
    public int compare(Integer number1, Integer number2) {
        return sumOfDigits(number1) - sumOfDigits(number2);
    }
         
    /**
     * Method calculates the sum of digits of a number.
     * 
     * @param number
     *            number for calculation.
     * @return result of the calculation.
     */
    public Integer sumOfDigits(Integer number) {
        int calcNumber = number;
        int sum = 0;
        while (calcNumber != 0) {
            sum += calcNumber % 10;
            calcNumber /= 10;
        }
        return sum;
    }
}
