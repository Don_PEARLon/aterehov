package com.training.homework9.task2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Application runner class.
 *
 * @author Alexandr_Terehov
 */
public class RunnerTask2 {
              
    /**
     * Application start point.
     *
     * @param args
     *            Command line arguments.
     */
    public static void main(String[] args) {
        MyHashSet<String> mySet = new MyHashSet<>();
        mySet.add("A");
        mySet.add("B");
        mySet.add("C");
        mySet.add("E");
        System.out.println(mySet);
        Set<String> set1 = new HashSet<>();
        set1.add("C");
        set1.add("B");
        set1.add("D");
        set1.add("F");
        System.out.println(set1);
        System.out.println("Union is:");
        System.out.println(mySet.union(set1));
        System.out.println("Intersection is:");
        System.out.println(mySet.intersection(set1));
        System.out.println("Minus is:");
        System.out.println(mySet.minus(set1));
        System.out.println("Difference is:");
        System.out.println(mySet.difference(set1));
    }
}
