package com.training.homework9.task2;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Custom implementation of the 'Set' interface, backed by a hash table.
 *
 * @author Alexandr_Terehov
 */
public class MyHashSet<E> extends HashSet<E> {
    static final long serialVersionUID = -5024744406713321676L;
    private transient HashMap<E, Object> map;

    // Dummy value to associate with an Object in the backing Map
    private static final Object PRESENT = new Object();

    /**
     * Constructs a new, empty set.
     */
    public MyHashSet() {
        map = new HashMap<>();
    }
    
    /**
     * Constructs a new set containing the elements in the specified
     * collection.  
     *
     * @param collection the collection whose elements are to be placed into this set
     * @throws NullPointerException if the specified collection is null
     */
    public MyHashSet(Collection<? extends E> collection) {
        map = new HashMap<>(Math.max((int) (collection.size() / .75f) + 1, 16));
        addAll(collection);
    }
    
    /**
     * Returns an iterator over the elements in this set.  The elements
     * are returned in no particular order.
     *
     * @return an Iterator over the elements in this set
     */
    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }

    /**
     * Returns the number of elements in this set.
     *
     * @return the number of elements in this set.
     */
    public int size() {
        return map.size();
    }

    /**
     * Returns 'true' if this set contains no elements.
     *
     * @return 'true' if this set contains no elements
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * Returns 'true' if this set contains the specified element.
     *
     * @param object element whose presence in this set is to be tested
     * @return 'true' if this set contains the specified element
     */
    public boolean contains(Object object) {
        return map.containsKey(object);
    }

    /**
     * Adds the specified element to this set if it is not already present.
     *
     * @param element element to be added to this set
     * @return 'true' if this set did not contain the specified element.
     */
    public boolean add(E element) {
        return map.put(element, PRESENT) == null;
    }

    /**
     * Removes the specified element from this set if it is present.
     *
     * @param obj object to be removed from this set, if present
     * @return 'true' if the set contained the specified element
     */
    public boolean remove(Object obj) {
        return map.remove(obj) == PRESENT;
    }

    /**
     * Removes all of the elements from this set.
     * The set will be empty after this call returns.
     */
    public void clear() {
        map.clear();
    }
    
    /**
     * Method used to create union of two sets.
     *
     * @param set1
     *            set to union with.
     * @return result of the union of two sets.
     * @throws NullPointerException if the set1 is null
     */
    public Set<E> union(Set<E> set1) {
        Set<E> resultSet = new MyHashSet<>();
        for (E item : this) {
            resultSet.add(item);
        }
        for (E item : set1) {
            resultSet.add(item);
        }
        return resultSet;
    }
    
    /**
     * Method used to create an intersection of two sets.
     *
     * @param set1
     *            set to create intersection with.
     * @return result of the intersection of two sets.
     * @throws NullPointerException if the set1 is null
     */
    public Set<E> intersection(Set<E> set1) {
        Map<E, Integer> resultMap = getUnionMap(set1);
        Set<E> resultSet = new HashSet<>();
        for (Map.Entry<E, Integer> entry : resultMap.entrySet()) {
            // Putting into result set an element which is present in both sets.
            if (entry.getValue() == 2) {
                resultSet.add(entry.getKey());
            }
        }
        return resultSet;
    }
    
    /**
     * Method used to create a subtraction of two sets.
     *
     * @param set1
     *            set to subtract.
     * @return result of the subtraction of two sets.
     * @throws NullPointerException if set1 is null
     */
    public Set<E> minus(Set<E> set1) {
        Map<E, Integer> resultMap = getUnionMap(set1);
        Set<E> resultSet = new HashSet<>();
        for (E item : this) {
            for (Map.Entry<E, Integer> entry : resultMap.entrySet()) {
                // Putting into result set an element which is present only in first set.
                if (item.equals(entry.getKey()) && entry.getValue() == 1) {
                    resultSet.add(item);
                }
            }
        }
        return resultSet;
    }
    
    /**
     * Method used to create a 'difference' set of two sets.
     *
     * @param set1
     *            to create difference with.
     * @return result 'difference' set.
     * @throws NullPointerException if the set1 is null
     */
    public Set<E> difference(Set<E> set1) {
        Map<E, Integer> resultMap = getUnionMap(set1);
        Set<E> resultSet = new HashSet<>();
        for (E item : this) {
            for (Map.Entry<E, Integer> entry : resultMap.entrySet()) {
                // Putting into result set an element which is present only in first set.
                if (item.equals(entry.getKey()) && entry.getValue() == 1) {
                    resultSet.add(item);
                }
            }
        }
        for (E item : set1) {
            for (Map.Entry<E, Integer> entry : resultMap.entrySet()) {
                // Putting into result set an element which is present only in second set.
                if (item.equals(entry.getKey()) && entry.getValue() == 1) {
                    resultSet.add(item);
                }
            }
        }
        return resultSet;
    }
    
    /**
     * Method used to create a map containing every unique element of two sets as a
     * key. Value for a key is 1 or 2. 1 - if element is present only in one set. 2
     * - if element is present in both sets.
     *
     * @param set1
     *            to create union map with.
     * @return result union map of two sets.
     */
    private Map<E, Integer> getUnionMap(Set<E> set1) {
        Map<E, Integer> resultMap = new HashMap<>();
        Integer value;
        for (E item : this.union(set1)) {
            resultMap.put(item, 0);
        }
        for (E item : this) {
            for (Map.Entry<E, Integer> entry : resultMap.entrySet()) {
                if (item.equals(entry.getKey())) {
                    value = entry.getValue() + 1;
                    resultMap.put(item, value);
                }
            }
        }
        for (E item : set1) {
            for (Map.Entry<E, Integer> entry : resultMap.entrySet()) {
                if (item.equals(entry.getKey())) {
                    value = entry.getValue() + 1;
                    resultMap.put(item, value);
                }
            }
        }
        return resultMap;
    }
}
