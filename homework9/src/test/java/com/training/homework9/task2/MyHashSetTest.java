package com.training.homework9.task2;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for MyHashSet class.
 *
 * @author Alexandr_Terehov
 */
public class MyHashSetTest {
	private static MyHashSet<String> mySet;
	private static Set<String> set1;

	@Before
	public void initObjects() {
		mySet = new MyHashSet<>();
		mySet.add("A");
        mySet.add("B");
        mySet.add("C");
        mySet.add("E");
		set1 = new HashSet<>();
		set1.add("C");
		set1.add("B");
		set1.add("D");
		set1.add("F");
	}

	@Test
	public void testMinus() {
		Set<String> expectedResultSet = new HashSet<>();
		expectedResultSet.add("A");
		expectedResultSet.add("E");
		Set<String> resultSet = mySet.minus(set1);
		boolean condition = testEquality(resultSet, expectedResultSet);
		assertTrue(condition);
	}

	@Test
	public void testUnion() {
		Set<String> expectedResultSet = new HashSet<>();
		expectedResultSet.add("A");
		expectedResultSet.add("B");
		expectedResultSet.add("C");
		expectedResultSet.add("D");
		expectedResultSet.add("E");
		expectedResultSet.add("F");
		Set<String> resultSet = mySet.union(set1);
		boolean condition = testEquality(resultSet, expectedResultSet);
		assertTrue(condition);
	}

	@Test
	public void testIntersection() {
		Set<String> expectedResultSet = new HashSet<>();
		expectedResultSet.add("B");
		expectedResultSet.add("C");
		Set<String> resultSet = mySet.intersection(set1);
		boolean condition = testEquality(resultSet, expectedResultSet);
		assertTrue(condition);
	}

	@Test
	public void testDifference() {
		Set<String> expectedResultSet = new HashSet<>();
		expectedResultSet.add("A");
		expectedResultSet.add("D");
		expectedResultSet.add("E");
		expectedResultSet.add("F");
		Set<String> resultSet = mySet.difference(set1);
		boolean condition = testEquality(resultSet, expectedResultSet);
		assertTrue(condition);
	}

	private static boolean testEquality(Set<String> set1, Set<String> set2) {
		boolean condition = false;
		if (set1.containsAll(set2) && (set1.size() == set2.size())) {
			condition = true;
		}
		return condition;
	}
}
