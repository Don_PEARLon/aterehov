package com.training.homework9.task1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for SumOfDigitsComparator class.
 *
 * @author Alexandr_Terehov
 */
public class SumOfDigitsComparatorTest {
	private static SumOfDigitsComparator comparator;

	@Before
	public void initObjects() {
		comparator = new SumOfDigitsComparator();
	}

	@Test
	public void testCompare() {
		Integer number1 = 1004;
		Integer number2 = 97;
		int result = comparator.compare(number1, number2);
		assertTrue(result < 0);
	}

	@Test
	public void testSumOfDigits() {
		Integer number = 20034;
		Integer sumOfDigits = comparator.sumOfDigits(number);
		assertEquals(9, sumOfDigits, 0);
	}
}
